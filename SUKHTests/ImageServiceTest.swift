//
//  ImageServiceTest.swift
//  SUKHTests
//
//  Created by Sönke Sobott on 16.07.21.
//  Copyright © 2021 SUKH. All rights reserved.
//

import XCTest
@testable import SUKH

class ImageServiceTest: XCTestCase {
    
    var imageService: ImageService!
    
    override func setUpWithError() throws {
        imageService = ImageService()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUploadImage() throws {
        let bundle = Bundle.init(for: ImageServiceTest.self)
        let image = UIImage(named: "1k", in: bundle, compatibleWith: nil)
        XCTAssertNotNil(image)
        
        let imageURL: String = imageService.uploadImage(image: image!)
        XCTAssertEqual(imageURL, "something_wrong")
    }

    

}

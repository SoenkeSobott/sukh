//
//  ImageService.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.07.21.
//  Copyright © 2021 SUKH. All rights reserved.
//

import Foundation
import AZSClient
import Keys


class ImageService {
    
    let keys = SUKHKeys()
    
    func getBlobContainer() -> AZSCloudBlobContainer {
        var account: AZSCloudStorageAccount? = nil
        do {
            account = try AZSCloudStorageAccount(fromConnectionString: keys.azureStorageConnectionString)
        } catch {
            NSLog("Error: Could not create AZSCloudStorageAccount from connection string")
        }
        let blobClient: AZSCloudBlobClient = account!.getBlobClient()
        return blobClient.containerReference(fromName: "user-images")
    }
    
    func uploadImage(image: UIImage) -> URL {
        let blobContainer: AZSCloudBlobContainer = getBlobContainer()
        
        
        blobContainer.createContainerIfNotExists(with: AZSContainerPublicAccessType.container, requestOptions: nil, operationContext: nil) { (NSError, Bool) -> Void in
            if ((NSError) != nil){
                NSLog("Error in creating container.")
            }
        }
        
        let blob: AZSCloudBlockBlob = blobContainer.blockBlobReference(fromName: "testImage.png" as String)
        let imageData = image.pngData()
        blob.upload(from: imageData!, completionHandler: {(NSError) -> Void in
                print("NSSSNNNNSNS")
                NSLog("Ok, uploaded!")
            })
    
        return blob.storageUri.primaryUri
    }
}

//
//  Classics.swift
//  SUKH
//
//  Created by Sönke Sobott on 27.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

// THIS score gets reset every month or so -> ever month intention to do as much as possible
// GOD is with animation

// LORD is schlagendes HEART
enum CreditScore:String {
    case NOOB1
    case NOOB2
    case NOOB3
    case NOOB4
    case NOOB5
    case STARTER1
    case STARTER2
    case STARTER3
    case STARTER4
    case STARTER5
    case PRO1
    case PRO2
    case PRO3
    case PRO4
    case PRO5
    case GOD1
    case GOD2
    case GOD3
    case GOD4
    case GOD5
    case LORD
}

class Classics {
    
    let locationService = LocationService()
    
    func calculateDistanceInMeters(latitude: Double, longitude: Double) -> Double {
        if (latitude != 0.0 && longitude != 0.0) {
            let eventLocation = CLLocation(latitude: latitude, longitude: longitude)
            let currentLocation = locationService.getUserLocation()
            let distance = (currentLocation.distance(from: eventLocation))
             
            return distance
         } else {
            return 0
         }
    }
    
    func transformDistanceToStringDistance(distance: Double) -> String {
        let distanceMeters = Measurement(value: distance, unit: UnitLength.meters)

        if (ceil(log10(distance)) < 4) {
            return String(Int(distance)) + " m"
        } else if (ceil(log10(distance)) > 3) {
            let distanceKm = distanceMeters.converted(to: UnitLength.kilometers)
            return String(format:"%.2f", distanceKm.value) + " km"
        } else {
            return "NA"
        }
    }
    
    func getDistanceStringFromCoordinates(latitude: Double, longitude: Double) -> String {
        return transformDistanceToStringDistance(distance: calculateDistanceInMeters(latitude: latitude, longitude: longitude))
    }
    
    func getGoingUsersString(goingUsers: [String]?) -> String {
        return String(goingUsers?.count ?? 0) + " going"
    }
    
    func calculateRank(votes: [Vote]) -> Int64 {
        var result: Int64 = 0
        for vote in votes {
            if (vote.positive!) {
                result = result + vote.value!
            } else {
                result = result - vote.value!
            }
        }
        return result
    }
    
    func calculateUpVotes(votes: [Vote]) -> Int64 {
        var upVotes: Int64 = 0
        for vote in votes {
            if (vote.positive!) {
                upVotes = upVotes + vote.value!
            }
        }
        return upVotes
    }
    
    func calculateDownVotes(votes: [Vote]) -> Int64 {
        var downVotes: Int64 = 0
        for vote in votes {
            if (!vote.positive!) {
                downVotes = downVotes + vote.value!
            }
        }
        return downVotes
    }
    
    func timeToStart(ISO8601DateStringStart: String, ISO8601DateStringEnd: String, size: CGFloat) -> NSAttributedString {
        let ISO8601formatter = ISO8601DateFormatter()
        ISO8601formatter.timeZone = .current
        let startDate = ISO8601formatter.date(from: ISO8601DateStringStart)
        let endDate = ISO8601formatter.date(from: ISO8601DateStringEnd)
        
        let secondsInAnHour = 3600
        let hoursInDay = 24
        let now = Date()
        let timeIntervalToStart = startDate!.timeIntervalSince(now)
        let timeIntervalToEnd = endDate!.timeIntervalSince(now)
        
        let timeIntervalToStartInHours = Int(timeIntervalToStart) / secondsInAnHour
        
        if (timeIntervalToEnd < 0) {
            return makePartOfStringBold(string: "Event is over", boldString: "over", size: size)
        } else if (timeIntervalToStart < 3600 && timeIntervalToStart > 0){
            return makePartOfStringBold(string: "Starting soon", boldString: "soon", size: size)
        } else if (timeIntervalToStart < 0 && timeIntervalToEnd > 0) {
            return makePartOfStringBold(string: "Active", boldString: "Active", size: size)
        } else {
            if (timeIntervalToStartInHours > 23) {
                let days = timeIntervalToStartInHours / hoursInDay
                if (timeIntervalToStartInHours < 48) {
                    return makePartOfStringBold(string: "Starts in \(days) day" as NSString, boldString: "\(days) days", size: size)
                } else {
                    return makePartOfStringBold(string: "Starts in \(days) days" as NSString, boldString: "\(days) days", size: size)
                }
            } else {
                return makePartOfStringBold(string: "Starts in \(timeIntervalToStartInHours) hours" as NSString, boldString: "\(timeIntervalToStartInHours) hours", size: size)
            }
        }
    }
    
    func makePartOfStringBold(string: NSString, boldString: String, size: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string as String)

        let boldRange = string.range(of: boldString)
        let font = UIFont.boldSystemFont(ofSize: size)

        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: boldRange)
        
        return attributedString
    }
    
    func stringFromISO8601Date(_ ISO8601DateString: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yy HH:mm"
        
        let ISO8601formatter = ISO8601DateFormatter()
        ISO8601formatter.timeZone = .current
        let date = ISO8601formatter.date(from: ISO8601DateString)
        
        return formatter.string(from: date!)

    }
    
    func dateFromISO8601Date(_ ISO8601DateString: String) -> Date {
        
        let ISO8601formatter = ISO8601DateFormatter()
        ISO8601formatter.timeZone = .current
        let date = ISO8601formatter.date(from: ISO8601DateString)!
        
        return date
    }
    
    func setSystemImageOrDefault(systemName: String) -> UIImage {
        if #available(iOS 13.0, *) {
            return UIImage(systemName: systemName)!
        } else {
            return UIImage(named: "NatureOne")!
        }
    }
    
    func isOver(end: String) -> Bool {
        let endDate = self.dateFromISO8601Date(end)
        if (endDate > Date()) {
            return false
        }
        return true
    }
    
    func timeToNowInHours(from: Date) -> TimeInterval {
        let timeIntervalToNow = from.timeIntervalSince(Date())
        return (timeIntervalToNow / 3600)
    }
    
    func timeToNowString(ISO8601DateString: String) -> String{
        let ISO8601formatter = ISO8601DateFormatter()
        ISO8601formatter.timeZone = .current
        let date = ISO8601formatter.date(from: ISO8601DateString)
        
        let secondsInAnHour = 3600
        let hoursInDay = 24
        let now = Date()
        var timeInterval = date!.timeIntervalSince(now)
        
        if (timeInterval < 0) {
            timeInterval = (timeInterval * -1)
            if (timeInterval > 3600) {
                let timeIntervalToEndInHours = Int(timeInterval) / secondsInAnHour
                if (timeIntervalToEndInHours > 24 && timeIntervalToEndInHours < 49) {
                    return "1 day ago"
                } else if (timeIntervalToEndInHours > 48) {
                    return "\(timeIntervalToEndInHours / hoursInDay) days ago"
                }
                return "\(timeIntervalToEndInHours) h ago"
            } else {
                let min: Int = Int(timeInterval / 60)
                if min < 1 {
                    return "just now"
                }
                return " \(min) min. ago"
            }
        } else {
            // error
            return "Error"
        }
    }
    
    func timeToNowString(ISO8601DateStringStart: String, ISO8601DateStringEnd: String) -> String {
        let ISO8601formatter = ISO8601DateFormatter()
        ISO8601formatter.timeZone = .current
        let startDate = ISO8601formatter.date(from: ISO8601DateStringStart)
        let endDate = ISO8601formatter.date(from: ISO8601DateStringEnd)
        
        let secondsInAnHour = 3600
        let hoursInDay = 24
        let now = Date()
        let timeIntervalToStart = startDate!.timeIntervalSince(now)
        var timeIntervalToEnd = endDate!.timeIntervalSince(now)
        
        
        if (timeIntervalToEnd < 0) {
            timeIntervalToEnd = (timeIntervalToEnd * -1)
            if (timeIntervalToEnd > 3600) {
                let timeIntervalToEndInHours = Int(timeIntervalToEnd) / secondsInAnHour
                if (timeIntervalToEndInHours > 24 && timeIntervalToEndInHours < 49) {
                    return "1 day ago"
                } else if (timeIntervalToEndInHours > 48) {
                    return "\(timeIntervalToEndInHours / hoursInDay) days ago"
                }
                return "\(timeIntervalToEndInHours) h ago"
            }
            return "Just ended"
        } else {
            if (timeIntervalToStart < 0 && timeIntervalToEnd > 0) {
                return "Active"
            } else {
                if (timeIntervalToStart > 3600) {
                    let timeIntervalToStartInHours = Int(timeIntervalToStart) / secondsInAnHour
                    if (timeIntervalToStartInHours > 24 && timeIntervalToStartInHours < 49) {
                        return "starts in 1 day"
                    } else if (timeIntervalToStartInHours > 48) {
                        return "starts in \(timeIntervalToStartInHours / hoursInDay) days"
                    }
                    return "starts in \(timeIntervalToStartInHours) h"
                }
                return "Starts soon"
            }
        }
    }
    
    func calculateCreditOfEvent(username: String, feedEntries: [EventFeedEntry]) -> (Int, CreditScore) {
        var credit = 4 // This is the return value. Depending on this your score will be shown
        
        for feedEntry in feedEntries {
            if (feedEntry.createdBy == username) {
                credit += 5
            }
        }
        
        if (credit > 0) {
            if (credit > 0 && credit < 5) {
                return (credit, CreditScore.NOOB1)
            } else if (credit > 4 && credit < 10){
                return (credit, CreditScore.NOOB2)
            } else if (credit > 9 && credit < 15){
                return (credit, CreditScore.NOOB3)
            } else if (credit > 14 && credit < 20){
                return (credit, CreditScore.NOOB4)
            } else if (credit > 19 && credit < 25){
                return (credit, CreditScore.NOOB5)
            } else if (credit > 24 && credit < 30){
                return (credit, CreditScore.STARTER1)
            } else if (credit > 29 && credit < 35){
                return (credit, CreditScore.STARTER2)
            } else if (credit > 34 && credit < 40){
                return (credit, CreditScore.STARTER3)
            } else if (credit > 39 && credit < 45){
                return (credit, CreditScore.STARTER4)
            } else if (credit > 44 && credit < 50){
                return (credit, CreditScore.STARTER5)
            } else if (credit > 49 && credit < 55){
                return (credit, CreditScore.PRO1)
            } else if (credit > 54 && credit < 60){
                return (credit, CreditScore.PRO2)
            } else if (credit > 59 && credit < 65){
                return (credit, CreditScore.PRO3)
            } else if (credit > 64 && credit < 70){
                return (credit, CreditScore.PRO4)
            } else if (credit > 69 && credit < 75){
                return (credit, CreditScore.PRO5)
            } else if (credit > 74 && credit < 80){
                return (credit, CreditScore.GOD1)
            } else if (credit > 79 && credit < 85){
                return (credit, CreditScore.GOD2)
            } else if (credit > 84 && credit < 90){
                return (credit, CreditScore.GOD3)
            } else if (credit > 89 && credit < 95){
                return (credit, CreditScore.GOD4)
            } else  if (credit > 94 && credit < 100){
                return (credit, CreditScore.GOD5)
            } else {
                return (credit, CreditScore.LORD)
            }
        } else {
            // Is smaller zero
            return (credit, CreditScore.NOOB1)
        }
        
    }
    
    // 1000 Credit per month
    func calculateCredit(userCredit: Int) -> (Int, CreditScore) {
        var returnCredit: (Int, CreditScore)
        let credit = userCredit // Manipulate/Scale if neccessary
        
        if (credit > 0) {
            if (credit > 0 && credit < 50) {
                returnCredit = (credit, CreditScore.NOOB1)
            } else if (credit > 49 && credit < 100){
                returnCredit = (credit, CreditScore.NOOB2)
            } else if (credit > 99 && credit < 150){
                returnCredit = (credit, CreditScore.NOOB3)
            } else if (credit > 149 && credit < 200){
                returnCredit = (credit, CreditScore.NOOB4)
            } else if (credit > 199 && credit < 250){
                returnCredit = (credit, CreditScore.NOOB5)
            } else if (credit > 249 && credit < 300){
                returnCredit = (credit, CreditScore.STARTER1)
            } else if (credit > 299 && credit < 350){
                returnCredit = (credit, CreditScore.STARTER2)
            } else if (credit > 349 && credit < 400){
                returnCredit = (credit, CreditScore.STARTER3)
            } else if (credit > 399 && credit < 450){
                returnCredit = (credit, CreditScore.STARTER4)
            } else if (credit > 449 && credit < 500){
                returnCredit = (credit, CreditScore.STARTER5)
            } else if (credit > 499 && credit < 550){
                returnCredit = (credit, CreditScore.PRO1)
            } else if (credit > 549 && credit < 600){
                returnCredit = (credit, CreditScore.PRO2)
            } else if (credit > 599 && credit < 650){
                returnCredit = (credit, CreditScore.PRO3)
            } else if (credit > 649 && credit < 700){
                returnCredit = (credit, CreditScore.PRO4)
            } else if (credit > 699 && credit < 750){
                returnCredit = (credit, CreditScore.PRO5)
            } else if (credit > 749 && credit < 800){
                returnCredit = (credit, CreditScore.GOD1)
            } else if (credit > 799 && credit < 850){
                returnCredit = (credit, CreditScore.GOD2)
            } else if (credit > 849 && credit < 900){
                returnCredit = (credit, CreditScore.GOD3)
            } else if (credit > 899 && credit < 950){
                returnCredit = (credit, CreditScore.GOD4)
            } else  if (credit > 949 && credit < 1000){
                returnCredit = (credit, CreditScore.GOD5)
            } else {
                returnCredit = (1000, CreditScore.LORD)
            }
            return returnCredit
        } else {
            // Is smaller zero
            return (0, CreditScore.NOOB1)
        }
        
    }
}

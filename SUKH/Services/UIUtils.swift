//
//  UIUtils.swift
//  SUKH
//
//  Created by Sönke Sobott on 02.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import UIKit

class UIUtils {
    
    
    func setSukhShadow(forView: UIView) {
        forView.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        forView.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        forView.layer.shadowOpacity = 0.5
        forView.clipsToBounds       = true
        forView.layer.masksToBounds = false
    }
    
    func setSukhTimelineShadow(forView: UIView) {
        forView.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        forView.layer.shadowOffset  = CGSize(width: 5, height: 0.5)
        forView.layer.shadowOpacity = 0.5
        forView.clipsToBounds       = true
        forView.layer.masksToBounds = false
    }
    
    func setSukhShadow(forLabel: UILabel) {
        forLabel.layer.shadowColor   = UIColor.black.cgColor
        forLabel.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        forLabel.layer.shadowOpacity = 1
        forLabel.clipsToBounds       = true
        forLabel.layer.masksToBounds = false
    }
    
    func setSukhShadow(forButton: UIButton) {
        forButton.layer.shadowColor   = UIColor.black.cgColor
        forButton.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        forButton.layer.shadowOpacity = 1
        forButton.clipsToBounds       = true
        forButton.layer.masksToBounds = false
    }
    
    func setSukhNavigationShadow(forView: UIView) {
        forView.layer.shadowColor   = UIColor(named: "SukhShadowColor")?.cgColor
        forView.layer.shadowOffset  = CGSize(width: 0, height: 6)
        forView.layer.shadowOpacity = 0.5
        forView.clipsToBounds       = true
        forView.layer.masksToBounds = false
    }
    
    func setSukhStandardNavigationShadow(forNavigationController: UINavigationController) {
        forNavigationController.navigationBar.layer.shadowColor = UIColor(named: "SukhShadowColor")?.cgColor
        forNavigationController.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 4)
        forNavigationController.navigationBar.layer.shadowOpacity = 0.5
    }
    
}

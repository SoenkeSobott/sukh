//
//  LocationsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 09.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit
import CoreLocation
import Keys

class LocationService: UIViewController {
    
    let locationManager = CLLocationManager()
    
    func startUpdatingLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        // Request user location
        locationManager.startUpdatingLocation()

    }
    
    func stopUpatingLocation() {
        locationManager.stopUpdatingLocation()
        // StopUpdatingLocation() does not work, set delegate to nil does. Don't ask why.
        locationManager.delegate = nil
    }
    
    func getUserLocation() -> CLLocation {
        // TODO: safe user location in keychain (sensible data)
        let latitude = Double(UserDefaults.standard.string(forKey: "userLocationLatitude") ?? "0")
        let longitude = Double(UserDefaults.standard.string(forKey: "userLocationLongitude") ?? "0")

        return CLLocation(latitude: latitude!, longitude: longitude!)
    }
    
    func setCenterOfMap(location: CLLocation) {
        UserDefaults.standard.set(location.coordinate.latitude, forKey: "centerOfMapLatitude")
        UserDefaults.standard.set(location.coordinate.longitude, forKey: "centerOfMapLongitude")
    }
    
    func getCenterOfMap() -> CLLocation {
        let latitude = Double(UserDefaults.standard.string(forKey: "centerOfMapLatitude") ?? "0")
        let longitude = Double(UserDefaults.standard.string(forKey: "centerOfMapLongitude") ?? "0")

        return CLLocation(latitude: latitude!, longitude: longitude!)
    }
    
    func setMapFilter(eventFilter: EventFilter) {
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: eventFilter)
        UserDefaults.standard.set(encodedData, forKey: "eventFilter")
        UserDefaults.standard.synchronize()
    }
    
    func getMapFilter() -> EventFilter {
        // default
        let upcoming = EventFilter(id: "01",
                             name: "Upcoming",
                             from: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                             to: nil,
                             tags: [],
                             types: [])
        
        let decoded  = UserDefaults.standard.data(forKey: "eventFilter")
        if decoded == nil {
            // return default
            return upcoming
        } else {
            // return eventFilter
            return NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! EventFilter
        }
    }
    
    func setSearchRadius(radius: Int) {
        UserDefaults.standard.set(radius, forKey: "searchRadius")
    }
    
    func getSearchRadius() -> Int {
        let searchRadius = UserDefaults.standard.integer(forKey: "searchRadius")
        return searchRadius
    }
    
    func reverseGeocode(location: CLLocation, completion: @escaping (String) -> ()) {
        var address: String = ""
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil {
                NSLog("Error: reverse geocode address")
            } else {
                if let place = placemark?[0] {
                    // Adress Builder
                    if (place.country ?? "NA") != "NA" {
                        address = place.country!
                        if (place.subAdministrativeArea ?? "NA") != "NA" {
                            address = place.subAdministrativeArea! + ", " + address
                            if (place.subThoroughfare ?? "NA") != "NA" {
                                address = place.subThoroughfare! + ", " + address
                                if (place.thoroughfare ?? "NA") != "NA" {
                                    address = place.thoroughfare! + " " + address
                                    completion(address)
                                } else {
                                    completion(address)
                                }
                            } else {
                                completion(address)
                            }
                        } else {
                            completion(address)
                        }
                    } else {
                        completion(address)
                    }
                } else {
                    completion(address)
                }
            }
        }
    }
    
    func reverseGeocodeCity(location: CLLocation, completion: @escaping (String) -> ()) {
        var city: String = ""
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil {
                NSLog("Error: reverse geocode city")
            } else {
                if let place = placemark?[0] {
                    // Adress Builder
                    if (place.subAdministrativeArea ?? "NA") != "NA" {
                        city = place.subAdministrativeArea!
                        completion(city)
                    } else {
                        completion(city)
                    }
                } else {
                    completion(city)
                }
            }
        }
    }
    
    func reverseGeocodeCityCountry(location: CLLocation, completion: @escaping (String) -> ()) {
        var address: String = ""
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil {
                NSLog("Error: reverse geocode city and country")
            } else {
                if let place = placemark?[0] {
                    if (place.country ?? "NA") != "NA" {
                        address = place.country!
                        if (place.subAdministrativeArea ?? "NA") != "NA" {
                            address = place.subAdministrativeArea! + ", " + address
                            completion(address)
                        } else {
                            completion(address)
                        }
                    } else {
                        completion(address)
                    }
                }
            }
        }
    }
}

extension LocationService: CLLocationManagerDelegate{
    
    // When location is updated
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            UserDefaults.standard.set(location.coordinate.latitude, forKey: "userLocationLatitude")
            UserDefaults.standard.set(location.coordinate.longitude, forKey: "userLocationLongitude")
        } else {
            NSLog("No coordinates found")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NSLog(error.localizedDescription)
    }
}

//
//  ConnectionService.swift
//  SUKH
//
//  Created by Sönke Sobott on 11.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import Reachability

class ConnectionService {
    
    let reachability = try! Reachability()
    
    func setReachabilityNotifier(forViewController: UIViewController) {
        NotificationCenter.default.addObserver(forViewController.self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)

        do {
            try reachability.startNotifier()
        } catch {
            NSLog("Could not start reachability notifier")
        }
    }
    
    func setReachabilityWithoutNotifier() {
        do {
            try reachability.startNotifier()
        } catch {
            NSLog("Could not start reachability notifier")
        }
    }
    
    func getConnectionStatus() -> Reachability.Connection {
        reachability.connection
    }
    
    @objc func reachabilityChanged(note: Notification) {
        // Implemented in the ViewControllers
    }
    
}

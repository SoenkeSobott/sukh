//
//  File.swift
//  SUKH
//
//  Created by Sönke Sobott on 11.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift

class BasicAlerts {
    
    func setConnectionAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "No connection", style: .info)
        banner.show()
    }
    
    func setReportAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Reported user", style: .info)
        banner.show()
    }
    
    func setBlockAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Blocked user", style: .info)
        banner.show()
    }
    
    func setReportEventAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Reported event", style: .info)
        banner.show()
    }
    
    func setDeleteEventAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Deleted Event", style: .success)
        banner.show()
    }
    
    func setReportFeedEntryAlert(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Reported feed entry", style: .info)
        banner.show()
    }
    
    func setFeedbackBanner(forViewController: UIViewController) {
        let banner = StatusBarNotificationBanner(title: "Thanks for your feedback", style: .info)
        banner.show()
    }
    
    func setEventIsOverBanner(forViewController: UIViewController) {
        let banner = FloatingNotificationBanner(title: "Event is over", subtitle: "You can't change a event that is over", style: .info)
        banner.show()
    }
    
    func setFeedWaitForEventToStartBanner(forViewController: UIViewController) {
        let banner = FloatingNotificationBanner(title: "Event not started", subtitle: "Adding content to the live feed is allowed one hour before the official launch", style: .info)
        banner.show()
    }
    
    func setVoteWaitForEventToStartBanner(forViewController: UIViewController) {
        let banner = FloatingNotificationBanner(title: "Voting not possible", subtitle: "Voting is only allowed from one hour before the start to 10 hours after the official end", style: .info)
        banner.show()
    }
    
    func setVoteOnlyWhenAtEventLocationBanner(forViewController: UIViewController) {
        let banner = FloatingNotificationBanner(title: "Voting not possible", subtitle: "Voting is only allowed when you are at least 500 meters near the event", style: .info)
        banner.show()
    }
    
    func setCreateEventFillInAllFieldsAlert(forViewController: UIViewController, missingInput: String) {
        let alert = UIAlertController(title: "Can't create Event", message: missingInput + " missing.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "invalidAlert"
        forViewController.present(alert, animated: true)
    }
    
    func setUpdateEventFillInAllFieldsAlert(forViewController: UIViewController, missingInput: String) {
        let alert = UIAlertController(title: "Can't update Event", message: missingInput + " missing.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "invalidAlert"
        forViewController.present(alert, animated: true)
    }
    
    func setFeatureComingSoonAlert(forViewController: UIViewController){
        let userNotExistingAlert = UIAlertController(title: "Coming soon", message: "This feature is coming soon - stay tuned!", preferredStyle: .alert)
            
        userNotExistingAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        forViewController.present(userNotExistingAlert, animated: true)
    }
}

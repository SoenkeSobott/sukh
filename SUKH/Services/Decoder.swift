//
//  Decoder.swift
//  SUKH
//
//  Created by Sönke Sobott on 17.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import CoreLocation

class Decoder {
    
    func decodeEvents(eventData: Data) -> [Event] {
        
        var events = [Event]()
        
        guard let json = try? JSONSerialization.jsonObject(with: eventData, options: JSONSerialization.ReadingOptions()) as? [Any] else { return events }
        
        for event in json {
            
            let eventString = event as? [String:Any]

            
            let votesString = eventString?["votes"] as! [Any]
            var votes: [Vote] = []
            for vote in votesString {
                let voteString = vote as! [String:Any]
                let newVote = Vote(username: voteString["username"] as? String,
                                value: voteString["value"] as? Int64,
                                positive: voteString["positive"] as? Bool)
                votes.append(newVote)
            }
            
            let eventFeedEntryString = eventString?["eventFeedEntries"] as! [Any]
            var eventFeedEntries: [EventFeedEntry] = []
            for eventFeedEntry in eventFeedEntryString {
                let eventFeedEntryString = eventFeedEntry as! [String:Any]
                let newEventFeedEntry = EventFeedEntry(feedEntryId: eventFeedEntryString["feedEntryId"] as! String?,
                                                       imageURL: eventFeedEntryString["imageURL"] as! String?,
                                                       image: nil,
                                                       createdBy: eventFeedEntryString["createdBy"] as! String?,
                                                       createdAt: eventFeedEntryString["createdAt"] as! String?)
                eventFeedEntries.append(newEventFeedEntry)
            }
            
            let newEvent = Event(eventID: eventString?["eventID"] as! String?,
                                  eventImage: nil,
                                  eventImageURL: eventString?["eventImageURL"] as! String?,
                                  name: eventString?["name"] as! String?,
                                  type: eventString?["type"] as! String?,
                                  startTime: eventString?["startTime"] as? String,
                                  endTime: eventString?["endTime"] as? String,
                                  invitedUsers: eventString?["invitedUsers"] as! [String]?,
                                  goingUsers: eventString?["goingUsers"] as! [String]?,
                                  isPublic: Bool((eventString?["isPublic"] as! Bool?)!),
                                  latitude: eventString?["latitude"] as? Double,
                                  longitude: eventString?["longitude"] as? Double,
                                  votes: votes,
                                  eventFeedEntries: eventFeedEntries,
                                  createdAt: eventString?["createdAt"] as? String,
                                  createdBy: eventString?["createdBy"] as! String?,
                                  tags: eventString?["tags"] as? [String]
                                  )
            
            events.append(newEvent)
        }
        return events
    }
    
    func decodeSingleEvent(eventData: Data) -> Event {
        
        let emptyEvent = Event(eventID: nil,
            eventImage: nil,
            eventImageURL: nil,
            name: nil,
            type: nil,
            startTime: nil,
            endTime: nil,
            invitedUsers: nil,
            goingUsers: nil,
            isPublic: nil,
            latitude: nil,
            longitude: nil,
            votes: nil,
            createdAt: nil,
            createdBy: nil,
            tags: nil)
        
        
        guard let json = try? JSONSerialization.jsonObject(with: eventData, options: JSONSerialization.ReadingOptions()) as? [String:Any] else { return emptyEvent }
        
        let votesString = json["votes"] as! [Any]
        var votes: [Vote] = []
        for vote in votesString {
            let voteString = vote as! [String:Any]
            let newVote = Vote(username: voteString["username"] as? String,
                            value: voteString["value"] as? Int64,
                            positive: voteString["positive"] as? Bool)
            votes.append(newVote)
        }
        
        let eventFeedEntryString = json["eventFeedEntries"] as! [Any]
        var eventFeedEntries: [EventFeedEntry] = []
        for eventFeedEntry in eventFeedEntryString {
            let eventFeedEntryString = eventFeedEntry as! [String:Any]
            let newEventFeedEntry = EventFeedEntry(feedEntryId: eventFeedEntryString["feedEntryId"] as! String?,
                                                   imageURL: eventFeedEntryString["imageURL"] as! String?,
                                                   image: nil,
                                                   createdBy: eventFeedEntryString["createdBy"] as! String?,
                                                   createdAt: eventFeedEntryString["createdAt"] as! String?)
            eventFeedEntries.append(newEventFeedEntry)
        }
        
        let newEvent = Event(eventID: json["eventID"] as! String?,
                              eventImage: nil,
                              eventImageURL: json["eventImageURL"] as! String?,
                              name: json["name"] as! String?,
                              type: json["type"] as! String?,
                              startTime: json["startTime"] as? String,
                              endTime: json["endTime"] as? String,
                              invitedUsers: json["invitedUsers"] as! [String]?,
                              goingUsers: json["goingUsers"] as! [String]?,
                              isPublic: Bool((json["isPublic"] as! Bool?)!),
                              latitude: json["latitude"] as? Double,
                              longitude: json["longitude"] as? Double,
                              votes: votes,
                              eventFeedEntries: eventFeedEntries,
                              createdAt: json["createdAt"] as? String,
                              createdBy: json["createdBy"] as! String?,
                              tags: json["tags"] as? [String]
                              )
        
        return newEvent
    }
    
    func decodeUser(data: Data) -> User {
        
        let emptyUser = User(username: nil, name: nil, mail: nil, password: nil, createdAt: nil, role: nil, profileImage: nil, profileImageURL: nil, followers: nil, following: nil)
    
        guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String:Any] else { return emptyUser }
       
        let user = User(username: json["username"] as? String,
                        name: json["name"] as? String,
                        mail: json["mail"] as? String,
                        password: nil,
                        createdAt: json["createdAt"] as? String,
                        role: json["role"] as! String?,
                        profileImage: nil,
                        profileImageURL: json["profileImageURL"] as? String,
                        followers: json["followers"] as? [String],
                        following: json["following"] as? [String],
                        monthlyCredit: json["monthlyCredit"] as? Int,
                        totalCredit: json["totalCredit"] as? Int)
    
        return user
    }
    
    func decodeUsers(data: Data) -> [User] {
        
        var users = [User]()
    
        guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [Any] else { return users }
                
        for user in json {
            let userString = user as? [String:Any]

            let user = User(username: userString?["username"] as? String,
                                name: userString?["name"] as? String,
                                mail: userString?["mail"] as? String,
                                password: nil,
                                createdAt: userString?["createdAt"] as? String,
                                role: userString?["role"] as! String?,
                                profileImage: nil,
                                profileImageURL: userString?["profileImageURL"] as? String,
                                followers: userString?["followers"] as? [String],
                                following: userString?["following"] as? [String],
                                monthlyCredit: userString?["monthlyCredit"] as? Int,
                                totalCredit: userString?["totalCredit"] as? Int)
                    
            users.append(user)
        }
        
        return users
        
    }
    
    func decodeSukhNotification(data: Data) -> SukhNotification {
        
        let emptySukhNotification = SukhNotification(newFollowers: nil, eventInvitations: nil)
        
        guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String:Any] else { return emptySukhNotification }
           
        let sukhNotification = SukhNotification(newFollowers: json["newFollowers"] as? Int,
                                                eventInvitations: json["eventInvitations"] as? Int)
        
        return sukhNotification
    }
    
    func decodeBlockedUsers(data: Data) -> [String] {
        
        var blockedUsers = [String]()
                
        guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [Any] else { return blockedUsers }
                   
        for username in json {
            blockedUsers.append(username as! String)
        }
        
        return blockedUsers
    }
}

//
//  AuthController.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import SDWebImage

enum UserRole: String {
    case Guest
    case UnverifiedUser
    case VerifiedUser
}

class AuthService {
    
    func saveUserAndPasswordAndRole(username: String, password: String, role: UserRole) {
        // Set username in UserDefaults
        UserDefaults.standard.set(username, forKey: "username")
        
        let savePasswordSuccessful: Bool = KeychainWrapper.standard.set(password, forKey: "userPassword")
        NSLog("Saving password " + (savePasswordSuccessful ? "succeded" : "failed"))
        
        let saveRoleSuccessful: Bool = KeychainWrapper.standard.set(role.rawValue, forKey: "userRole")
        NSLog("Saving role " + (saveRoleSuccessful ? "succeded" : "failed"))
    }
    
    func removeUserAndPasswordAndRole() {
        UserDefaults.standard.removeObject(forKey: "username")
        
        let removePasswordSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "userPassword")
        NSLog("Removing password " + (removePasswordSuccessful ? "succeded" : "failed"))
        
        let removeRoleSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "userRole")
        NSLog("Removing role " + (removeRoleSuccessful ? "succeded" : "failed"))
    }    
    
    func setGuestUser(role: UserRole) {
        let saveRoleSuccessful: Bool = KeychainWrapper.standard.set(role.rawValue, forKey: "userRole")
        NSLog("Saving role " + (saveRoleSuccessful ? "succeded" : "failed"))
    }
    
    func getCurrentUser() -> String {
        return UserDefaults.standard.string(forKey: "username") ?? ""
    }
    
    func getRoleOfCurrentUser() -> UserRole {
        let userRole: String? = KeychainWrapper.standard.string(forKey: "userRole")
        if (userRole != nil) {
            return UserRole(rawValue: userRole!)!
        } else {
            return .Guest // as a guest you can't do anything unsafe
        }
    }
    
    func isSignedIn() -> Bool {
        // Check if a user exists
        guard self.getCurrentUser() != nil else {
            return false
        }
        // Check if password and role exists
        let password: String? = KeychainWrapper.standard.string(forKey: "userPassword")
        let role: String? = KeychainWrapper.standard.string(forKey: "userRole")

        if (password != nil && role != nil) {
            return true
        } else {
            return false
        }
    }
    
    func isAdmin() -> Bool {
        // TODO: implement isAdmin check
        return false
    }
    
    func resetUserDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func resetSDWebImageCache() {
        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
    }
    
    func getPassword() -> String {
        let password: String? = KeychainWrapper.standard.string(forKey: "userPassword")
        if password != nil {
            return password!
        } else {
            NSLog("Error retriewing password of user - password was nil")
            return password!
        }
    }
}

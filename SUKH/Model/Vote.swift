//
//  Vote.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class Vote: NSObject, Codable {
    
    internal init(username: String? = nil, value: Int64? = nil, positive: Bool? = nil) {
        self.username = username
        self.value = value
        self.positive = positive
    }
    
   
    var username: String?
    var value: Int64?
    var positive: Bool?
    
}

extension Vote {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
}

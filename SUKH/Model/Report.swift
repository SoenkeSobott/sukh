//
//  Report.swift
//  SUKH
//
//  Created by Sönke Sobott on 05.12.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

enum ReportType: String {
    case Event
    case Image
    case FeedEntry
    case User
}


class Report: NSObject, Codable {
    
    internal init(reportId: String? = nil, type: String? = nil, subject: String? = nil, createdBy: String? = nil, createdAt: String? = nil) {
        self.reportId = reportId
        self.type = type
        self.subject = subject
        self.createdBy = createdBy
        self.createdAt = createdAt
    }
    
    var reportId: String?
    var type: String?
    var subject: String?
    var createdBy: String?
    var createdAt: String?
}

extension Report {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
}


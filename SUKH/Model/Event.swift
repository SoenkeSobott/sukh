//
//  Event.swift
//  SUKH
//
//  Created by Sönke Sobott on 28.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class Event: NSObject, Codable {
    
    internal init(eventID: String? = nil, eventImage: Data? = nil, eventImageURL: String? = nil, name: String? = nil, type: String? = nil, startTime: String? = nil, endTime: String? = nil, invitedUsers: [String]? = nil, goingUsers: [String]? = nil, isPublic: Bool? = nil, latitude: Double? = nil, longitude: Double? = nil, votes: [Vote]? = nil, eventFeedEntries: [EventFeedEntry]? = nil, createdAt: String? = nil, createdBy: String? = nil, tags: [String]? = nil) {
        self.eventID = eventID
        self.eventImage = eventImage
        self.eventImageURL = eventImageURL
        self.name = name
        self.type = type
        self.startTime = startTime
        self.endTime = endTime
        self.invitedUsers = invitedUsers
        self.goingUsers = goingUsers
        self.isPublic = isPublic
        self.latitude = latitude
        self.longitude = longitude
        self.votes = votes
        self.eventFeedEntries = eventFeedEntries
        self.createdAt = createdAt
        self.createdBy = createdBy
        self.tags = tags
    }
    
    var eventID: String?
    var eventImage: Data?
    var eventImageURL: String?
    var name: String?
    var type: String?
    
    var startTime: String?
    var endTime: String?
    
    var invitedUsers: [String]?
    var goingUsers: [String]?
    
    var isPublic: Bool?
    
    var latitude: Double?
    var longitude: Double?
    
    var votes: [Vote]?
    var eventFeedEntries: [EventFeedEntry]?
    var createdAt: String?
    var createdBy: String?

    var tags: [String]?
    
}

extension Event {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
    // TODO decode

}

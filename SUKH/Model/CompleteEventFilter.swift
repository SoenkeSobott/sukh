//
//  CompleteEventFilter.swift
//  SUKH
//
//  Created by Sönke Sobott on 24.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class CompleteEventFilter: NSObject, Codable {
    
    internal init(id: String? = nil, name: String? = nil, latitude: Double? = nil, longitude: Double? = nil, radius: Int? = nil, from: String? = nil, to: String? = nil, tags: [String]? = nil, types: [String]? = nil) {
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
        self.from = from
        self.to = to
        self.tags = tags
        self.types = types
    }
    
    var name: String?
    var id: String?
    // Geo filter
    var latitude: Double?
    var longitude: Double?
    var radius: Int?
    
    // Time filter
    var from: String?
    var to: String?
    
    // Tag filter
    var tags: [String]?

    // Type filter
    var types: [String]?
    
}

extension CompleteEventFilter {
    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
}

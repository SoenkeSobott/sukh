//
//  EventFilter.swift
//  SUKH
//
//  Created by Sönke Sobott on 24.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class EventFilter: NSObject, Codable, NSCoding {
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(from, forKey: "from")
        coder.encode(to, forKey: "to")
        coder.encode(tags, forKey: "tags")
        coder.encode(types, forKey: "types")

    }
    
    required convenience init?(coder: NSCoder) {
        let id = coder.decodeObject(forKey: "id") as? String
        let name = coder.decodeObject(forKey: "name") as? String
        let from = coder.decodeObject(forKey: "from") as? String
        let to = coder.decodeObject(forKey: "to") as? String
        let tags = coder.decodeObject(forKey: "tags") as? [String]
        let types = coder.decodeObject(forKey: "types") as? [String]

        self.init(id: id, name: name, from: from, to: to, tags: tags, types: types)
    }
    
    
    internal init(id: String? = nil, name: String? = nil, from: String? = nil, to: String? = nil, tags: [String]? = nil, types: [String]? = nil) {
        self.id = id
        self.name = name
        self.from = from
        self.to = to
        self.tags = tags
        self.types = types
    }
    
    var id: String?
    var name: String?
    
    // Time filter
    var from: String?
    var to: String?
    
    // Tag filter
    var tags: [String]?

    // Type filter
    var types: [String]?
    
}

extension EventFilter {
    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
}

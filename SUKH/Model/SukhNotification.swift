//
//  Notification.swift
//  SUKH
//
//  Created by Sönke Sobott on 18.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation


class SukhNotification: NSObject, Codable {
    
    internal init(newFollowers: Int? = nil, eventInvitations: Int? = nil) {
        self.newFollowers = newFollowers
        self.eventInvitations = eventInvitations
    }
    
    var newFollowers: Int?
    var eventInvitations: Int?
    
}

extension SukhNotification {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
}

//
//  User.swift
//  SUKH
//
//  Created by Sönke Sobott on 29.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class User: NSObject, Codable {
    
    internal init(username: String? = nil, name: String? = nil, mail: String? = nil, password: String? = nil, createdAt: String? = nil, role: String? = nil, profileImage: Data? = nil, profileImageURL: String? = nil, followers: [String]? = nil, following: [String]? = nil, monthlyCredit: Int? = nil, totalCredit: Int? = nil) {
        self.username = username
        self.name = name
        self.mail = mail
        self.password = password
        self.createdAt = createdAt
        self.role = role
        self.profileImage = profileImage
        self.profileImageURL = profileImageURL
        self.followers = followers
        self.following = following
        self.monthlyCredit = monthlyCredit
        self.totalCredit = totalCredit
    }
    
    
    var username: String?
    var name: String?
    var mail: String?
    var password: String?
    var createdAt: String?
    var role: String?
    var profileImage: Data?
    var profileImageURL: String?
    var followers: [String]?
    var following: [String]?
    var monthlyCredit: Int?
    var totalCredit: Int?
    
}

extension User {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
}


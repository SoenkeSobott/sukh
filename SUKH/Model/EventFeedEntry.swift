//
//  EventFeedEntry.swift
//  SUKH
//
//  Created by Sönke Sobott on 20.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation

class EventFeedEntry: NSObject, Codable {
    
    internal init(feedEntryId: String? = nil, imageURL: String? = nil, image: Data? = nil, createdBy: String? = nil, createdAt: String? = nil) {
        self.feedEntryId = feedEntryId
        self.imageURL = imageURL
        self.image = image
        self.createdBy = createdBy
        self.createdAt = createdAt
    }
   
    var feedEntryId: String?
    var imageURL: String?
    var image: Data?
    var createdBy: String?
    var createdAt: String?
    
}

extension EventFeedEntry {

    func encoded(using jsonEncoder: JSONEncoder = JSONEncoder()) -> Data? {
        return try? jsonEncoder.encode(self)
    }
    
}

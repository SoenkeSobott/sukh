//
//  EventsRankingViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 21.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import AVFoundation
import Reachability
import NotificationBannerSwift


struct EventRankingCell {
    let eventID: String
    let name: String
    let imageURL: String
    var imageData: Data
    let distance: Double
    let rank: Int64
    let rankIndicator: RankIndicator
    let goingUsers: Int
    let tags: [String]
    let startTime: Date
    let endTime: Date
}

enum RankIndicator {
    case up
    case neutral
    case down
}

enum DragDirection {
    case up
    case down
}

class EventsRankingViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var customNavigationView: UIView!
    @IBOutlet weak var eventsRankingTableView: UITableView!

    // Services
    let locationService = LocationService()
    let authService = AuthService()
    let mongoQueryService = MongoQueryService()
    let classics = Classics()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    
    // Variables
    private let refreshControl = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    var rankLongPressGestureRecognizer = UILongPressGestureRecognizer()
    var tappedEventID = ""
    var dataSource: [EventRankingCell] = []
    var rankUpArrowButton = UIButton()
    var rankDownArrowButton = UIButton()
    var tapLocation = CGPoint()
    var longPressGestureChangedCounter = 0
    var dragDirection = DragDirection.up
    var dragSize = 0
    var noEventsBanner = DecentStyleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        // Register to listen for a notification center
        NotificationCenter.default.addObserver(self, selector: #selector(setFilters(notification:)), name: NSNotification.Name.init("setFiltersRanking"), object: nil)
        
        connectionService.setReachabilityNotifier(forViewController: self)
        self.activityIndicator = Spinner().getActivityIndicator(view: view.self)
        setUpTableView()
        setUpRefreshControl()
        setUpNavigationBar()
        setUpRankArrows()
        setUpLongPressGestureRecognizer()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshEventRankingTableView(self)
                
    }
    
    func setUpTableView() {
        eventsRankingTableView.dataSource = self
        eventsRankingTableView.delegate = self
    }
    
    func setUpNavigationBar() {
        UIUtils().setSukhNavigationShadow(forView: customNavigationView.self)
        customNavigationView.layer.zPosition  = 1000
    }
    
    func setUpRefreshControl() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            eventsRankingTableView.refreshControl = refreshControl
        } else {
            eventsRankingTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshEventRankingTableView(_:)), for: .valueChanged)
    }
    
    @IBAction func popViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func setUpLongPressGestureRecognizer() {
        rankLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(rankEvent(recognizer:)))
        rankLongPressGestureRecognizer.minimumPressDuration = 0.3
        rankLongPressGestureRecognizer.delegate = self
        eventsRankingTableView.addGestureRecognizer(rankLongPressGestureRecognizer)
    }
    
    func setUpView() {
        self.eventsRankingTableView.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func setUpRankArrows() {
        rankUpArrowButton = UIButton(frame: CGRect(origin: CGPoint(x: 10, y: 100), size: CGSize(width: 100, height: 50)))
        rankUpArrowButton.tintColor = UIColor.gray
        rankUpArrowButton.setBackgroundImage(classics.setSystemImageOrDefault(systemName: "chevron.up"), for: .normal)
        
        rankDownArrowButton = UIButton(frame: CGRect(origin: CGPoint(x: 10, y: self.view.frame.height - 100), size: CGSize(width: 100, height: 50)))
        rankDownArrowButton.tintColor = UIColor.gray
        rankDownArrowButton.setBackgroundImage(classics.setSystemImageOrDefault(systemName: "chevron.down"), for: .normal)
        
        rankUpArrowButton.isHidden = true
        rankDownArrowButton.isHidden = true
        
        self.view.addSubview(rankUpArrowButton)
        self.view.addSubview(rankDownArrowButton)

    }
    
    @IBAction func filterEvents(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:"EventsFilterViewController") as? EventsFilterViewController {
            vc.modalPresentationStyle = .popover
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func setFilters(notification: NSNotification) {
        refreshEventRankingTableView(self)
    }
    
    @objc func rankEvent(recognizer: UILongPressGestureRecognizer) {
        eventsRankingTableView.isScrollEnabled = false
        rankUpArrowButton.isHidden = false
        rankDownArrowButton.isHidden = false
        
        var changedLocation = CGPoint()
        if recognizer.state == UIGestureRecognizer.State.began {
            dragSize = 0
            AudioServicesPlaySystemSound(1519)
            tapLocation = recognizer.location(in: self.eventsRankingTableView)
            if let tapIndexPath = self.eventsRankingTableView.indexPathForRow(at: tapLocation) {
                if let tappedCell = self.eventsRankingTableView.cellForRow(at: tapIndexPath) as? EventsRankingPlaceOneTableViewCell
                {
                    tappedEventID = tappedCell.eventID
                }
                if let tappedCell = self.eventsRankingTableView.cellForRow(at: tapIndexPath) as? EventsRankingPlaceTwoTableViewCell
                {
                    tappedEventID = tappedCell.eventID
                }
                if let tappedCell = self.eventsRankingTableView.cellForRow(at: tapIndexPath) as? EventsRankingPlaceThreeTableViewCell
                {
                    tappedEventID = tappedCell.eventID
                }
                if let tappedCell = self.eventsRankingTableView.cellForRow(at: tapIndexPath) as? EventsRankingTableViewCell
                {
                    tappedEventID = tappedCell.eventID
                }
            }
        }
        
        if recognizer.state == UIGestureRecognizer.State.changed {
            dragSize = 0
            changedLocation = recognizer.location(in: self.eventsRankingTableView)
            if (changedLocation.y < tapLocation.y) {
                dragSize = Int(tapLocation.y - changedLocation.y)
                dragDirection = DragDirection.up
                if (dragSize > 100) {
                    rankUpArrowButton.tintColor = UIColor.green
                } else {
                    rankUpArrowButton.tintColor = UIColor.gray
                }
                
            }
            
            if (changedLocation.y > tapLocation.y) {
                dragSize = Int(changedLocation.y - tapLocation.y)
                dragDirection = DragDirection.down
                if (dragSize > 100) {
                    rankDownArrowButton.tintColor = UIColor.red
                } else {
                    rankDownArrowButton.tintColor = UIColor.gray

                }
            }
        }
        
        if recognizer.state == UIGestureRecognizer.State.ended {
            eventsRankingTableView.isScrollEnabled = true
            rankUpArrowButton.isHidden = true
            rankUpArrowButton.tintColor = UIColor.gray
            rankDownArrowButton.isHidden = true
            rankDownArrowButton.tintColor = UIColor.gray

            
            if ( dragSize > 100) {
                NSLog("Tapped on Cell: \(tappedEventID) with \(dragDirection) vote")
                var startDate = Date()
                var endDate = Date()
                var distance: Double = 0
                for event in dataSource {
                    if (event.eventID == tappedEventID) {
                        startDate = event.startTime
                        endDate = event.endTime
                        distance = event.distance
                    }
                }
                if (classics.timeToNowInHours(from: startDate) < 1 && classics.timeToNowInHours(from: endDate) > -10) {
                    if (distance < 500) {
                        updateRankOfEvent(eventID: tappedEventID, dragDirection: dragDirection)
                    } else {
                        basicAlerts.setVoteOnlyWhenAtEventLocationBanner(forViewController: self)
                    }
                } else {
                    basicAlerts.setVoteWaitForEventToStartBanner(forViewController: self)
                }
            }
        }
    }
    
    
    func updateRankOfEvent(eventID: String, dragDirection: DragDirection) {
        if (authService.getRoleOfCurrentUser() != .Guest) {
    
            var positive = false
            if (dragDirection == DragDirection.up) {
                positive = true
            } else {
                positive = false
            }
            let vote = Vote(username: authService.getCurrentUser(), value: Int64(1), positive: positive)

            mongoQueryService.addVoteToEvent(eventID: eventID, vote: vote) { (response) in
                let statusCode = response.statusCode
                if (statusCode == 200) {
                    self.refreshEventRankingTableView(self)
                } else if (statusCode == 409) {
                    let banner = FloatingNotificationBanner(title: "Already voted", subtitle: "You already voted for this event and therefore can't vote again", style: .info)
                    banner.show()
                }else {
                    NSLog("Error updating rank: \(statusCode)")
                    self.refreshEventRankingTableView(self)
                }
            }
        } else {
            setSignUpAlert()
        }
    }
    
    @objc func refreshEventRankingTableView(_ sender: Any){
        if (connectionService.getConnectionStatus() != .unavailable) {
            setLoading(true)
            self.dataSource = []
            
            mongoQueryService.getEventsFiltered(filter: locationService.getMapFilter()) { (response) in
                let statusCode: Int = response.response!.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    
                    let events = Decoder().decodeEvents(eventData: data)
                
                    self.fillDataSource(withEvents: events)
                    self.sortTable()
                    self.eventsRankingTableView.reloadData()
                    self.refreshControl.endRefreshing()
                    self.setLoading(false)
                } else {
                    NSLog("Error requesting data: \(statusCode)")
                    self.refreshControl.endRefreshing()
                    self.setLoading(false)
                }
            }
        } else {
            setLoading(false)
        }
    }
}

extension EventsRankingViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
}

// Tablew view
extension EventsRankingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row ==  0) {
            return 140
        } else if (indexPath.row == 1) {
            return 120
        } else if (indexPath.row == 2){
            return 100
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(dataSource.count == 0) {
            return UITableViewCell()
        }
    
        if (indexPath.row == 0) {
            guard let eventRankingOneCell = tableView.dequeueReusableCell(withIdentifier: "eventRankingOneCell", for: indexPath) as? EventsRankingPlaceOneTableViewCell else {
                return UITableViewCell()
            }
            
            let event = dataSource[indexPath.row]
                                    
            eventRankingOneCell.eventImage.image = nil
            eventRankingOneCell.rankIndicator.image = nil

            eventRankingOneCell.eventName.text = event.name
            eventRankingOneCell.eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            eventRankingOneCell.eventImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
            setRankIndicatorImage(imageView: eventRankingOneCell.rankIndicator, rankIndicator: event.rankIndicator)
            eventRankingOneCell.eventDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
            eventRankingOneCell.rank.text = String(event.rank)
            eventRankingOneCell.goingUsers.text = "\(event.goingUsers) going"
            eventRankingOneCell.eventID = event.eventID
            
            eventRankingOneCell.eventTagsView.subviews.forEach({ $0.removeFromSuperview() })
            add(tags: event.tags, toView: eventRankingOneCell.eventTagsView)
            
            return eventRankingOneCell
        } else if (indexPath.row == 1) {
            guard let eventRankingTwoCell = tableView.dequeueReusableCell(withIdentifier: "eventRankingTwoCell", for: indexPath) as? EventsRankingPlaceTwoTableViewCell else {
                return UITableViewCell()
            }
            
            let event = dataSource[indexPath.row]

            eventRankingTwoCell.eventImage.image = nil
            eventRankingTwoCell.rankIndicator.image = nil
            
            // Setup eventCell
            eventRankingTwoCell.eventName.text = event.name
            eventRankingTwoCell.eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            eventRankingTwoCell.eventImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
            setRankIndicatorImage(imageView: eventRankingTwoCell.rankIndicator, rankIndicator: event.rankIndicator)
            eventRankingTwoCell.eventDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
            eventRankingTwoCell.rank.text = String(event.rank)
            eventRankingTwoCell.goingUsers.text = "\(event.goingUsers) going"
            eventRankingTwoCell.eventID = event.eventID
        
            return eventRankingTwoCell
            
        } else if (indexPath.row == 2) {
            guard let eventRankingThreeCell = tableView.dequeueReusableCell(withIdentifier: "eventRankingThreeCell", for: indexPath) as? EventsRankingPlaceThreeTableViewCell else {
                return UITableViewCell()
            }
           
            let event = dataSource[indexPath.row]

            eventRankingThreeCell.eventImage.image = nil
            eventRankingThreeCell.rankIndicator.image = nil

            
            // Setup eventCell
            eventRankingThreeCell.eventName.text = event.name
            eventRankingThreeCell.eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            eventRankingThreeCell.eventImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
            setRankIndicatorImage(imageView: eventRankingThreeCell.rankIndicator, rankIndicator: event.rankIndicator)
            eventRankingThreeCell.eventDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
            
            eventRankingThreeCell.rank.text = String(event.rank)
            eventRankingThreeCell.eventID = event.eventID
            
            return eventRankingThreeCell
            
        } else {
            guard let eventRankingCell = tableView.dequeueReusableCell(withIdentifier: "eventRankingCell", for: indexPath) as? EventsRankingTableViewCell else {
                return UITableViewCell()
            }
            
            let event = dataSource[indexPath.row]

            eventRankingCell.eventImage.image = nil
            eventRankingCell.rankIndicator.image = nil

            // Setup eventCell
            eventRankingCell.eventName.text = event.name
            eventRankingCell.eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            eventRankingCell.eventImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
            setRankIndicatorImage(imageView: eventRankingCell.rankIndicator, rankIndicator: event.rankIndicator)
            eventRankingCell.eventDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
            eventRankingCell.rank.text = String(event.rank)
            eventRankingCell.eventID = event.eventID

            return eventRankingCell
        }
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = UIColor.gray
        }
        
        if (authService.getRoleOfCurrentUser() != .Guest) {
            performSegue(withIdentifier: "eventDetailsSegue", sender: dataSource[indexPath.row].eventID)
        } else {
            setSignUpAlert()
        }

        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// Helper functions
extension EventsRankingViewController {
    
    func fillDataSource(withEvents: [Event]) {
        
        for event in withEvents {
            let customizableEventCell = EventRankingCell(eventID: event.eventID!,
                                                         name: event.name!,
                                                         imageURL: event.eventImageURL!,
                                                         imageData: Data(),
                                                         distance: classics.calculateDistanceInMeters(latitude: event.latitude!, longitude: event.longitude!),
                                                         rank: classics.calculateRank(votes: event.votes!),
                                                         rankIndicator: setRankIndicator(event: event),
                                                         goingUsers: event.goingUsers!.count,
                                                         tags: event.tags!,
                                                         startTime: classics.dateFromISO8601Date(event.startTime!),
                                                         endTime: classics.dateFromISO8601Date(event.endTime!))
            
            // Check if already in dataSource
            let results = self.dataSource.filter { $0.eventID == customizableEventCell.eventID }
            if (results.isEmpty) {
                self.dataSource.append(customizableEventCell)
            }
        }
        
        // Set no events banner if necessary
        if (dataSource.count == 0) {
            setUpNoEventsBanner(isHidden: false)
        } else {
            setUpNoEventsBanner(isHidden: true)
        }
    }
    
    func sortTable() {
        dataSource.sort(by: {$0.rank > $1.rank})
        eventsRankingTableView.reloadData()
    }
    
    func setSignUpAlert() {
        let signUpAlert = UIAlertController(title: "You want more?", message: "Sign in to get the full experience", preferredStyle: .alert)
            
        signUpAlert.addAction(UIAlertAction(title: "Sign in", style: .default, handler: { (alert: UIAlertAction!) in
            // Go to login
            self.authService.removeUserAndPasswordAndRole()
            self.authService.resetUserDefaults()
            self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
        }))
        
        signUpAlert.addAction(UIAlertAction(title: "Later", style: .cancel, handler: nil))

        self.present(signUpAlert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailsSegue" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        eventsRankingTableView.isUserInteractionEnabled = !loading
    }
    
    func setUpNoEventsBanner(isHidden: Bool) {
        self.noEventsBanner.removeFromSuperview()
        if(isHidden) {
            eventsRankingTableView.isHidden = !isHidden
            noEventsBanner.isHidden = isHidden
        } else {
            noEventsBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: self.customNavigationView.frame.maxY + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
            noEventsBanner.setTitle("No nearby events", for: .normal)
            
            noEventsBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
            noEventsBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
            noEventsBanner.layer.shadowOpacity = 0.5
            noEventsBanner.clipsToBounds       = true
            noEventsBanner.layer.masksToBounds = false
                   
            self.view.addSubview(noEventsBanner)
            noEventsBanner.isHidden = isHidden
            eventsRankingTableView.isHidden = !isHidden
        }
        
    }
    
    func setRankIndicator(event: Event) -> RankIndicator {
        let votes = event.votes!
        for vote in votes {
            if (vote.username == authService.getCurrentUser()) {
                if (vote.positive!) {
                    return RankIndicator.up
                } else {
                    return RankIndicator.down
                }
            }
        }
        return RankIndicator.neutral
    }
    
    func setRankIndicatorImage(imageView: UIImageView, rankIndicator: RankIndicator) {
        
        switch rankIndicator {
        case .up:
            imageView.image = classics.setSystemImageOrDefault(systemName: "triangle.fill")
        case .down:
            if #available(iOS 13.0, *) {
                var image = UIImage(systemName: "triangle.fill")
                image = image!.sd_flippedImage(withHorizontal: false, vertical: true)
                imageView.image = image!.withTintColor(UIColor(named: "SukhTextColor")!)
            } else {
                // Fallback on earlier versions
            }
        case .neutral:
            imageView.image = classics.setSystemImageOrDefault(systemName: "circle")
        }
    }
    
    func add(tags: [String], toView: UIView) {
        let viewHeight = toView.frame.height
        let viewWidth = toView.frame.width
        var currentWidthOfTags: CGFloat = 0
        var x: CGFloat = 0
        
        for tag in tags {
            // Calculate width of tag
            let titleLength: CGFloat = CGFloat(tag.count)
            // Min button width of 80
            let widthOfEventTag: CGFloat = titleLength*12 > 80 ? titleLength*12 : 80
            
            currentWidthOfTags = currentWidthOfTags + widthOfEventTag
            
            if (currentWidthOfTags < viewWidth) {
                let tagButton = EventTagUIButton(frame: CGRect (x: x, y: (viewHeight / 2) - 15, width: widthOfEventTag, height: 30))
                tagButton.setTitle(tag, for: .normal)
                toView.addSubview(tagButton)
                
                x = x + widthOfEventTag + 10
            } else {
                break
            }
        }
    }
    
}

// Reachability
extension EventsRankingViewController {

    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            refreshEventRankingTableView(self)
        case .cellular:
            refreshEventRankingTableView(self)
        case .none:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not reachable")
        case .unavailable:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not available")
        }
    }
    
}


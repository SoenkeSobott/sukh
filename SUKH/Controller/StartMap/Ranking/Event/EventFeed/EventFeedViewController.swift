//
//  EventFeedViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class EventFeedViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    // Outlets
    @IBOutlet weak var customNavigationBar: UIView!
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addButton: UIButton!
    
    // Service
    let authService = AuthService()
    let basicAlerts = BasicAlerts()
    let mongoQueryService = MongoQueryService()
    let classics = Classics()
    let decoder = Decoder()
    
    // Variables
    var refreshControl = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    let image = UIImagePickerController()
    var eventID = ""
    var eventFeedEntries: [EventFeedEntry] = []
    var eventStart = Date()
    var event: Event = Event()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
        setUpNavigationBar()
        setUpRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getEventFeedEntries()
    }
    
    func setUpNavigationBar() {
        UIUtils().setSukhNavigationShadow(forView: customNavigationBar.self)
        customNavigationBar.layer.zPosition = 100
    }
    
    func setUpRefreshControl () {
       scrollView.refreshControl = refreshControl
       scrollView.refreshControl?.addTarget(self, action: #selector(getEventFeedEntries), for: .valueChanged)
    }
    
    @objc func getEventFeedEntries() {
        setLoading(true)
        mongoQueryService.getEvent(eventID: eventID) { (response) in
            let statusCode = response.response!.statusCode
            if (statusCode == 200) {
                let data = response.data
                self.event = self.decoder.decodeSingleEvent(eventData: data!)
                self.eventFeedEntries = self.event.eventFeedEntries!
                self.eventStart = self.classics.dateFromISO8601Date(self.event.startTime!)
                self.updateFeed(withFeedEntries: self.eventFeedEntries)
            } else {
                NSLog("Error retrieving event: \(statusCode)")
                self.setLoading(false)
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    @IBAction func addMedia(_ sender: Any) {
        if (classics.timeToNowInHours(from: eventStart) < 1) {
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            self.present(alert, animated: true, completion: nil)
        } else {
            basicAlerts.setFeedWaitForEventToStartBanner(forViewController: self)
        }
    }
}

// Media picker
extension EventFeedViewController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            uploadFeedEntry(image: image)
        } else {
            NSLog("Error uploading image")
            self.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera()
    {
        setLoading(true)
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            image.sourceType = UIImagePickerController.SourceType.camera
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
            setLoading(false)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            setLoading(false)
        }
    }

    func openGallary()
    {
        setLoading(true)
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        image.delegate = self
        self.present(image, animated: true, completion: nil)
        setLoading(false)
    }
    
    func uploadFeedEntry(image: UIImage) {
        setLoading(true)
        let ISO8601DateString = ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime])
        let imageData: Data = image.jpegData(compressionQuality: 0.5)! as Data

        let newEventFeedEntry = EventFeedEntry(feedEntryId: UUID().uuidString,
                                               imageURL: nil,
                                               image: imageData,
                                               createdBy: authService.getCurrentUser(),
                                               createdAt: ISO8601DateString)
        
        mongoQueryService.addFeedEntryToEvent(eventID: eventID, eventFeedEntry: newEventFeedEntry) { (response) in
            let statusCode = response.statusCode
            if (statusCode == 200) {
                self.setLoading(false)
                self.getEventFeedEntries()
            } else {
                NSLog("Error uploading feed entry: \(statusCode)")
                self.setLoading(false)
            }
        }
    }
    
    func updateFeed(withFeedEntries: [EventFeedEntry]) {
        var row: CGFloat = 0
        var placeInRow: CGFloat = 0
        
        self.removeAllSubviews()
        for entry in withFeedEntries {
            addFeedEntryToView(feedEntry: entry, row: row, placeInRow: placeInRow)
            
            if (placeInRow == 0) {
                placeInRow += 1
            } else {
                placeInRow = 0
                row += 1
            }
        }
        self.refreshControl.endRefreshing()
        self.setLoading(false)
    }
    
    func addFeedEntryToView(feedEntry: EventFeedEntry, row: CGFloat, placeInRow: CGFloat) {
        let scrollViewheight = scrollView.frame.height
        let width = feedView.frame.width/2
        let height: CGFloat = 300
        let feedEntryView = UIView()
        feedEntryView.frame = CGRect(x: (width * placeInRow), y: (height * row), width: width, height: height)
        feedEntryView.contentMode = .scaleToFill

        // Tap on feedEntryView
        let tap = TapGestureWithFeedEntryId(target: self,action: #selector(handleTaponFeedEntryView(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        tap.feedEntryId = feedEntry.feedEntryId!
        tap.createdBy = feedEntry.createdBy!
        feedEntryView.isUserInteractionEnabled = true
        feedEntryView.addGestureRecognizer(tap)

        let imageView = UIImageView()
        
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: URL(string: feedEntry.imageURL!), placeholderImage: UIImage(named: "natureOne"))
        imageView.frame = CGRect(x: 0, y: 0, width: width, height: feedEntryView.frame.height)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 2.5
        imageView.layer.borderColor = UIColor(named: "SukhBackgroundColor")?.cgColor
        imageView.layer.cornerRadius   = 8
        imageView.clipsToBounds = true
        feedEntryView.addSubview(imageView)

        let label = FeedEntryLabel()
        let labelHeight: CGFloat = 40
        label.text = feedEntry.createdBy == authService.getCurrentUser() ? "You" : feedEntry.createdBy
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.black
        //label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.2
        label.numberOfLines = 0 // or 1
        label.frame = CGRect(x: imageView.frame.minX, y: imageView.frame.maxY - labelHeight, width: imageView.frame.width, height: labelHeight)
        imageView.addSubview(label)
        
        // make special image view
        feedView.addSubview(feedEntryView)
        if let constraint = (feedView.constraints.filter{$0.identifier == "scrollViewHeight"}.first) {
            constraint.constant = ((row + 1) * height) < scrollViewheight ? (scrollViewheight + 100) : ((row + 1) * height)
            feedView.layoutIfNeeded()
        }
    }
    
}

// Helper functions
extension EventFeedViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMedia" {
            if let destination = segue.destination as? MediaViewController,
                let pair = sender as? (Event, String) {
                destination.event = pair.0
                destination.tappedMediaId = pair.1
            }
        }
    }
    
    @objc func handleTaponFeedEntryView(_ sender: TapGestureWithFeedEntryId) {
        performSegue(withIdentifier: "showMedia", sender: (event, sender.feedEntryId))
    }
    
    func removeAllSubviews() {
        feedView.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
    }
}

class TapGestureWithFeedEntryId: UITapGestureRecognizer {
    var feedEntryId = String()
    var createdBy = String()
}


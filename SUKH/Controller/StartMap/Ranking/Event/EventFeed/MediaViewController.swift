//
//  MediaViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class MediaViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    @IBOutlet weak var mediaCount: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mediaActionButton: UIButton!
    @IBOutlet weak var deleteImageButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let reportsMongoService = ReportsMongoService()
    let authService = AuthService()
    let uiUtils = UIUtils()
    let classics = Classics()
    let basicAlerts = BasicAlerts()
    
    // Variables
    var event: Event = Event()
    var media: [EventFeedEntry] = []
    var tappedMediaId = ""
    var onlyOnce = false
    var currentPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        media = event.eventFeedEntries!
        mediaCollectionView.contentInset.top = -UIApplication.shared.statusBarFrame.height
        setUpView()
    }
    
    func setUpView() {
        uiUtils.setSukhShadow(forLabel: mediaCount)
        uiUtils.setSukhShadow(forLabel: infoLabel)
        uiUtils.setSukhShadow(forButton: backButton)
        uiUtils.setSukhShadow(forButton: deleteImageButton)
        uiUtils.setSukhShadow(forButton: mediaActionButton)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mediaAction(_ sender: Any) {
        let alert = UIAlertController(title: "Media", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Report", style: .destructive, handler: { _ in
            self.setReportFeedEntryAlert()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func deleteImage(_ sender: Any) {
        let alert = UIAlertController(title: "Delete Image", message: "Do you wan't to delete this Image?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (alert: UIAlertAction!) in
            let feedEntryId = self.media[self.currentPage].feedEntryId
            
            self.mongoQueryService.removeFeedEntryFromEvent(eventID: self.event.eventID!, feedEntryId: feedEntryId!) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully deleted eventFeedEntry with status code: \(statusCode)")
                    // TODO load new stuff in function not in here
                    self.refreshMedia()
                } else {
                    NSLog("Failed deleting eventFeedEntry with status code: \(statusCode)")
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func refreshMedia() {
        media.remove(at: self.currentPage)
        mediaCount.text = "\(currentPage + 1) / \(media.count)"
        mediaCollectionView.reloadData()
    }
    
    func setReportFeedEntryAlert() {
        let reportUserAlert = UIAlertController(title: "Report entry", message: "Do you want to report this entry?", preferredStyle: .alert)
            
        reportUserAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            // Report feed entry
            let ISO8601DateString = ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime])
            
            let feedEntryId = self.media[self.currentPage].feedEntryId

            let newReport = Report(reportId: UUID().uuidString,
                                   type: ReportType.FeedEntry.rawValue,
                                   subject: feedEntryId,
                                   createdBy: self.authService.getCurrentUser(),
                                   createdAt: ISO8601DateString)
            
            self.reportsMongoService.postReport(report: newReport) { (statusCode) in
                if (statusCode == 201) {
                    self.basicAlerts.setReportFeedEntryAlert(forViewController: self)
                    NSLog("Successfully reported feed entry '" + feedEntryId! + "' with status code: \(statusCode)")
                } else {
                    NSLog("Error reporting feed entry '" + feedEntryId! + "' with status code: \(statusCode)")
                }
            }
        }))
        reportUserAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(reportUserAlert, animated: true)
    }
}

extension MediaViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return media.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        mediaCount.text = "\(currentPage + 1) / \(media.count)"
        
        let feedEntry = media[currentPage]
        // delete media
        if (feedEntry.createdBy == authService.getCurrentUser()) {
            deleteImageButton.isHidden = false
        } else {
            deleteImageButton.isHidden = true
        }
        
        // Info text
        var user = feedEntry.createdBy!
        if (user.elementsEqual(authService.getCurrentUser())) {
            user = "yourself"
        }
        let time = classics.timeToNowString(ISO8601DateString: feedEntry.createdAt!)
        infoLabel.text = "Added by \(user) \(time)"
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let size = cell.contentView.frame.size
        
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        imageview.contentMode = .scaleAspectFill
        
        let feedEntry = media[indexPath.row]
        imageview.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageview.sd_setImage(with: URL(string: feedEntry.imageURL!), placeholderImage: UIImage(named: "natureOne"))
        cell.contentView.addSubview(imageview)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !onlyOnce {
            let index = media.firstIndex { (feedEntry) -> Bool in
                feedEntry.feedEntryId == tappedMediaId
                } ?? 0
            mediaCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: false)
            onlyOnce = true
        }
    }
}

extension MediaViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = mediaCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

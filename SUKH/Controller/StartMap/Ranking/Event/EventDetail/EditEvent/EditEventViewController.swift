//
//  EditEventViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 13.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import NotificationBannerSwift

var eventLocation = CLLocation()
var updatedGoingUsers: [String] = []

class EditEventViewController: UIViewController, UIGestureRecognizerDelegate {

    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var customNavigationBar: UIView!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var imagePickerButton: UIButton!
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var eventLocationMap: MKMapView!
    @IBOutlet weak var eventLocationName: UIButton!
    @IBOutlet weak var privateLabel: UILabel!
    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var eventTypeTextField: InputTextField!
    @IBOutlet weak var eventTypePickerView: UIPickerView!
    
    @IBOutlet weak var startDatePicker: UIDatePicker!
    
    @IBOutlet weak var endDatePicker: UIDatePicker!
    
    @IBOutlet weak var goingUsers: UILabel!
    @IBOutlet weak var invitedUsers: UILabel!
    
    @IBOutlet weak var eventTagsView: UIView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let locationService = LocationService()
    let connectionService = ConnectionService()
    let classics = Classics()
    let basicAlerts = BasicAlerts()
    let authService = AuthService()
    
    // Variables
    var eventID = ""
    var rowCounter = 0
    var activityIndicator = UIActivityIndicatorView()
    var locationName = ""
    var eventTags = [String]()
    var eventTypePickerVisible = false
    var startDatePickerVisible = false
    var endDatePickerVisible = false
    let eventTypes = ["Party", "Hangout", "Productive"]
    var eventTypePickerSelectedRow = 1
    let hidePickersDuration = 0.3
    let eventAnnotation = MKPointAnnotation()
    var event = Event(eventID: nil, eventImage: nil, eventImageURL: nil, name: nil, type: nil, startTime: nil, endTime: nil, invitedUsers: nil, goingUsers: nil, isPublic: nil, latitude: nil, longitude: nil, votes: nil, createdAt: nil, createdBy: nil, tags: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
        NotificationCenter.default.addObserver(self, selector: #selector(setInvitedUsers(notification:)), name: NSNotification.Name.init("setInvitedUsers"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLocation(notification:)), name: NSNotification.Name.init("setUpLocationAndMap"), object: nil)
        getEvent()
        setUpScrollView()
        setUpNavigationBar()
        setUpEventTypePicker()
        setUpDatePickers()
    }
    
    func getEvent() {
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getEvent(eventID: eventID, completion: { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    
                    let event = Decoder().decodeSingleEvent(eventData: data)
                                
                    self.event = event
                    
                    // Setup
                    self.setUpView()
                    self.setUpLocationAndMap()
                    self.setLoading(false)
                } else {
                    NSLog("Error loading Event: \(statusCode!)")
                    self.setLoading(false)
                }
            })
        }
    }
    
    func setUpScrollView() {
        scrollView.keyboardDismissMode = .onDrag
        scrollView.delegate = self
    }
    
    func setUpNavigationBar() {
        UIUtils().setSukhNavigationShadow(forView: customNavigationBar.self)
        customNavigationBar.layer.zPosition = 100
        self.navigationItem.title = "Edit"
        return
    }
    
    func setUpEventTypePicker() {
        // TextField
        let tap = UITapGestureRecognizer(target: self,action: #selector(handleTaponTextField(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        eventTypeTextField.isUserInteractionEnabled = true
        eventTypeTextField.addGestureRecognizer(tap)
        eventTypeTextField.placeholder = "Event type"
        
        // Picker
        eventTypePickerView.isHidden = true
        eventTypePickerView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setUpDatePickers() {
        // Start Date Picker
        startDatePicker.isHidden = false
        startDatePicker?.datePickerMode = .dateAndTime
        startDatePicker?.minimumDate = Date(timeIntervalSinceNow: 0)
        startDatePicker?.addTarget(self, action: #selector(self.startDateChanged(datePicker:)), for: .valueChanged)
        
        // End Date Picker
        endDatePicker.isEnabled = true
        endDatePicker.isHidden = false
        endDatePicker?.datePickerMode = .dateAndTime
        endDatePicker?.minimumDate = Date(timeIntervalSinceNow: 0)
    }
    
    func setUpView() {
        // Set eventLocation
        eventLocation = CLLocation(latitude: event.latitude!, longitude: event.longitude!)
        
        // Set eventImage
        eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventImage.sd_setImage(with: URL(string: event.eventImageURL!), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        // Set name, private, type
        eventName.text = event.name
        privateSwitch.isOn = event.isPublic!
        eventTypeTextField.text = event.type
        
        // Set dates
        startDatePicker.date = classics.dateFromISO8601Date(event.startTime!)
        endDatePicker.date = classics.dateFromISO8601Date(event.endTime!)
        
        // Set goingUsers
        goingUsers.text = "\(event.goingUsers!.count) going"
        let tapOnGoingPeople = UITapGestureRecognizer(target: self,action: #selector(handleTapOnGoingPeopleTextField(_:)))
        tapOnGoingPeople.numberOfTapsRequired = 1
        goingUsers.isUserInteractionEnabled = true
        goingUsers.addGestureRecognizer(tapOnGoingPeople)
        updatedGoingUsers = event.goingUsers!
        
        // Set invitedUsers
        invitedUsers.text = "\(event.invitedUsers!.count) invited"
        let tapOnInvitedPeople = UITapGestureRecognizer(target: self,action: #selector(handleTapOnInvitedPeopleTextField(_:)))
        tapOnInvitedPeople.numberOfTapsRequired = 1
        invitedUsers.isUserInteractionEnabled = true
        invitedUsers.addGestureRecognizer(tapOnInvitedPeople)
        invitations = event.invitedUsers!
        
        // Set eventTags
        eventTags = event.tags!
        setUpEventTags()
    }
    
    func setUpLocationAndMap() {
        eventLocation = CLLocation(latitude: eventLocation.coordinate.latitude, longitude: eventLocation.coordinate.longitude)
        
        locationService.reverseGeocode(location: eventLocation) { (locationString) in
            self.locationName = locationString
            self.eventLocationName.setTitle(locationString, for: .normal)
        }
        
        let tapOnLocation = UITapGestureRecognizer(target: self,action: #selector(handleTapOnLocation(_:)))
        tapOnLocation.numberOfTapsRequired = 1
        tapOnLocation.delegate = self
        eventLocationName.isUserInteractionEnabled = true
        eventLocationName.addGestureRecognizer(tapOnLocation)
        
        eventLocationMap.isUserInteractionEnabled = false
        eventLocationMap.mapType = .mutedStandard
        let region = MKCoordinateRegion( center: eventLocation.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
        eventLocationMap.setRegion(eventLocationMap.regionThatFits(region), animated: false)
        
        eventAnnotation.coordinate = eventLocation.coordinate
        eventLocationMap.addAnnotation(eventAnnotation)
    }
    
    @objc func updateLocation(notification: NSNotification) {
        self.locationService.reverseGeocode(location: eventLocation) { (locationString) in
            self.locationName = locationString
            self.eventLocationName.setTitle(locationString, for: .normal)
        }
        
        let region = MKCoordinateRegion( center: eventLocation.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
        eventLocationMap.setRegion(eventLocationMap.regionThatFits(region), animated: false)
        eventAnnotation.coordinate = eventLocation.coordinate
    }
    
    @objc func handleTapOnGoingPeopleTextField(_ sender: UITapGestureRecognizer) {
        if let vc =  UIStoryboard.init(name: "EventDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditGoingUsersViewController") as? EditGoingUsersViewController {
            vc.modalPresentationStyle = .popover
            vc.eventID = eventID
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func handleTapOnInvitedPeopleTextField(_ sender: UITapGestureRecognizer) {
        if let vc =  UIStoryboard.init(name: "AddEvent", bundle: Bundle.main).instantiateViewController(withIdentifier: "InviteFollowersViewController") as? InviteFollowersViewController {
            vc.modalPresentationStyle = .popover
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func handleTapOnLocation(_ sender: UITapGestureRecognizer) {
        if let vc =  UIStoryboard.init(name: "EventDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangeLocationViewController") as? ChangeLocationViewController {
            vc.modalPresentationStyle = .popover
            performSegue(withIdentifier: "editLocationSegue", sender: eventLocation)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editLocationSegue" {
            if let destination = segue.destination as? ChangeLocationViewController,
                let location = sender as? CLLocation {
                destination.addEventLocation = location
            }
        }
    }
    
    @IBAction func popViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveChangedEvent(_ sender: Any) {
        setSaveUpdatedEventAlert()
    }    
}

// Change image
extension EditEventViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBAction func changeEventImage(_ sender: Any) {
        setLoading(true)
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        
        self.present(image, animated: true) {
            // After it is complete
            self.setLoading(false)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Encoding profile image
            setLoading(true)
            self.eventImage.image = image
            self.setLoading(false)
            
        } else {
            NSLog("Error uploading event image")
            self.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

// EventType picker
extension EditEventViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return eventTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return eventTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eventTypePickerSelectedRow = row
        eventTypeTextField.text = eventTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    @objc func handleTaponTextField(_ sender: UITapGestureRecognizer) {
        if (eventTypePickerVisible) {
            hidePickerView()
        } else {
            showPickerView()
        }
    }
    
    func showPickerView() {
        eventTypePickerVisible = true
        eventTypePickerView.selectRow(eventTypePickerSelectedRow, inComponent: 0, animated: false)
        eventTypeTextField.text = eventTypes[eventTypePickerSelectedRow]
                
        if let constraint = (eventTypePickerView.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 90.0
            eventTypePickerView.layoutIfNeeded()
            eventTypePickerView.isHidden = false
        }
    }
    
    func hidePickerView() {
        eventTypePickerVisible = false
        
        if let constraint = (eventTypePickerView.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 0.0
            UIView.animate(withDuration: hidePickersDuration, animations: {
                 self.view.layoutIfNeeded()
            })
            eventTypePickerView.isHidden = true
        }
    }
}

// Date pickers
extension EditEventViewController {
    
    @objc func startDateChanged(datePicker: UIDatePicker) {
        endDatePicker?.minimumDate = startDatePicker?.date
    }
}

// Event Tags View
extension EditEventViewController {
    
    func setUpAddEventTagButton(x: Int, y: Int) {
        let addEventTagButton = EventTagUIButton(frame: CGRect (x: x, y: y, width: 70, height: 30))
        let image = classics.setSystemImageOrDefault(systemName: "plus")
        addEventTagButton.setImage(image, for: .normal)
        addEventTagButton.tintColor = UIColor.white

        addEventTagButton.addTarget(self, action: #selector(addEventTag), for: .touchUpInside)
        addEventTagButton.tag = 666
        self.eventTagsView.addSubview(addEventTagButton)
    }
    
    @IBAction func addEventTag(sender: UIButton!) {
        addEventAlert()
        
    }
    
    func addEventAlert() {
        let addEventAlert = UIAlertController(title: "Add Tag", message: "Add some tags to describe your event", preferredStyle: .alert)

        addEventAlert.addTextField { (textField) in
            let exampleTags = ["HipHop", "Techno", "Houseparty", "Friends", "RnB", "Escalation"]
            let randomInt = Int.random(in: 0..<exampleTags.count)
            textField.placeholder = exampleTags[randomInt]
        }

        addEventAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak addEventAlert] (_) in
            let textField = addEventAlert?.textFields![0]
            if (textField?.text != "") {
                // Add to eventTags list
                self.eventTags.append("#" + (textField?.text)!)
                self.setUpEventTags()
            }
        }))

        self.present(addEventAlert, animated: true, completion: nil)
    }
    
    
    func setUpEventTags() {
        let screenWidth  = Int (UIScreen.main.fixedCoordinateSpace.bounds.width)
        rowCounter = 0
                
        // TODO: Max 8 tags
        var xValueOfButton: Int = 10
        var yValueOfButton: Int = 10
        
        var lengthOfCurrentRow: Int = 0
        var widthOfPreviousEventTag: Int = 0
        for (index, eventTag) in eventTags.enumerated(){
            let titleLength = eventTag.count
            // Min button width of 80
            let widthOfCurrentEventTag = titleLength*12 > 80 ? titleLength*12 : 80
            
            lengthOfCurrentRow = lengthOfCurrentRow + widthOfCurrentEventTag
            if (index != 0) {
                if (lengthOfCurrentRow > screenWidth-20) {
                    xValueOfButton = 10
                    yValueOfButton = yValueOfButton + 40
                    lengthOfCurrentRow = widthOfCurrentEventTag + 5
                    rowCounter += 1
                } else {
                    lengthOfCurrentRow = lengthOfCurrentRow + 5
                    xValueOfButton = xValueOfButton + widthOfPreviousEventTag + 5
                }
            }
            widthOfPreviousEventTag = widthOfCurrentEventTag
            
           // Add FilterButton with inital values
            let filterButton = EventTagUIButton(frame: CGRect (x: xValueOfButton, y: yValueOfButton, width: widthOfCurrentEventTag, height: 30))
            filterButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            filterButton.setTitle(eventTag, for: .normal)

            // Add filterButton to buttonsInOneRow[]
            self.eventTagsView.addSubview(filterButton)
            
        }
        
        removeAddEventButton()
        if (eventTags.count < 10) {
            if (lengthOfCurrentRow < screenWidth - 80 ) {
                setUpAddEventTagButton(x: xValueOfButton + widthOfPreviousEventTag + 5, y: yValueOfButton)
            } else {
                setUpAddEventTagButton(x: 10 , y: yValueOfButton +  40)
            }
        }
    }
    
    @IBAction func buttonAction(sender: UIButton!) {
        let pressedButton = sender as? EventTagUIButton
        
        let alert = UIAlertController(title: "Delete Filter", message: "Do you wan't to delete this Filter Tag?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (alert: UIAlertAction!) in
            let index = self.eventTags.firstIndex(of: (pressedButton?.titleLabel!.text!)!)
            self.eventTags.remove(at: index!)
            self.deleteOldView()
            self.setUpEventTags()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}

extension EditEventViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hidePickerView()
    }
}

// Helper functions
extension EditEventViewController {
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
    }
    
    func removeAddEventButton(){
        if let viewWithTag = self.view.viewWithTag(666) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func deleteOldView() {
        guard let sublayers = self.eventTagsView.layer.sublayers else { return }
        for layer in sublayers {
            layer.removeFromSuperlayer()
        }
    }
    
    @objc func setInvitedUsers(notification: NSNotification) {
        invitedUsers.text = "\(invitations.count) invited"
    }
    
    func setSaveUpdatedEventAlert() {
        let userNotExistingAlert = UIAlertController(title: "Save changes", message: "Do you want to update this Event?", preferredStyle: .alert)
            
        userNotExistingAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        userNotExistingAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            let inputValid = self.checkIfInputIsPresentAndValid().0
            let missingInput = self.checkIfInputIsPresentAndValid().1
            if (inputValid) {
                self.setLoading(true)
                let eventImageData: Data = self.eventImage.image!.jpegData(compressionQuality: 0.5)! as Data
                let updatedEvent = Event(eventID: self.eventID,
                                        eventImage: eventImageData,
                                        eventImageURL: nil,
                                        name: self.eventName.text,
                                        type: self.eventTypeTextField.text,
                                        startTime: ISO8601DateFormatter.string(from: self.startDatePicker.date, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        endTime: ISO8601DateFormatter.string(from: self.endDatePicker.date, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        invitedUsers: invitations,
                                        goingUsers: nil,
                                        isPublic: self.privateSwitch.isOn,
                                        latitude: eventLocation.coordinate.latitude,
                                        longitude: eventLocation.coordinate.longitude,
                                        votes: nil,
                                        createdAt: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        createdBy: self.authService.getCurrentUser(),
                                        tags: self.eventTags
                )
                self.mongoQueryService.updateEvent(eventID: self.eventID, event: updatedEvent) { (statusCode) in
                    if (statusCode == 200) {
                        let banner = FloatingNotificationBanner(title: "Updated Event", style: .success)
                        banner.show()
                        self.navigationController?.popViewController(animated: true)
                        self.setLoading(false)
                    } else {
                        let banner = FloatingNotificationBanner(title: "Failed Updating Event: \(statusCode)", style: .warning)
                        banner.show()
                        self.setLoading(false)
                    }
                }
            } else {
                self.basicAlerts.setUpdateEventFillInAllFieldsAlert(forViewController: self, missingInput: missingInput)
            }
        }))
        
        self.present(userNotExistingAlert, animated: true)
    }
    
    enum Input: String {
        case EventName = "Event name"
        case EventTags = "Event tags"
        case EventType = "Event type"
        case StartDate = "Start date"
        case EndDate = "End date"
        case None = "None"
    }
    
    func checkIfInputIsPresentAndValid() -> (Bool, String) {
        // Till now only check if input is present TODO: validation
        if (self.eventName.text?.count == 0) {
            return (false, Input.EventName.rawValue)
        } else if (self.eventTypeTextField.text?.count == 0) {
            return (false, Input.EventType.rawValue)
        } else if (self.eventTags.count == 0) {
            return (false, Input.EventTags.rawValue)
        } else {
            return (true, Input.None.rawValue)
        }
    }
}

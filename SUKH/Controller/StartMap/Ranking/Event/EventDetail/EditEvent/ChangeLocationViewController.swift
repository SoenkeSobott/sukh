//
//  ChangeLocationViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 14.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import MapKit

class ChangeLocationViewController: UIViewController, UIGestureRecognizerDelegate {

    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var saveLocationButton: GoToLocationButton!
    
    // Services
    
    
    // Variables
    var tapForNewEventGestureRecognizer = UIGestureRecognizer()
    var addEventLocationAnnotation = MKPointAnnotation()
    var addEventLocation = CLLocation()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setCurrentLocation(eventLocation: addEventLocation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tapForNewEventGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapForNewEventGestureRecognizer.delegate = self
        mapView.addGestureRecognizer(tapForNewEventGestureRecognizer)
        
        saveLocationButton.isEnabled = false
        saveLocationButton.setTitle("Tap on the Map to select a Location", for: .normal)
    }
    
    func setCurrentLocation(eventLocation: CLLocation) {
        let region = MKCoordinateRegion( center: eventLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(mapView.regionThatFits(region), animated: false)
        
        addEventLocationAnnotation.coordinate = eventLocation.coordinate
        mapView.addAnnotation(addEventLocationAnnotation)
    }
    

    @objc func handleTap(_ sender: UITapGestureRecognizer) {

        // Remove previous eventLocationAnnotation from mapView
        mapView.removeAnnotation(addEventLocationAnnotation)
        
        // Get coordinate of point on mapView
        let location = sender.location(in: mapView)
        let eventCoordinates = mapView.convert(location,toCoordinateFrom: mapView)
        
        // set addEventLocation
        addEventLocation = CLLocation(latitude: eventCoordinates.latitude, longitude: eventCoordinates.longitude)
                
        // Center mapView to tapped point
        mapView.setCenter(eventCoordinates, animated: true)
        
        // Make saveLocationButton visible
        saveLocationButton.setTitle("Save location", for: .normal)
        saveLocationButton.isEnabled = true

        // Add annotation:
        addEventLocationAnnotation.coordinate = eventCoordinates
        mapView.addAnnotation(addEventLocationAnnotation)
        
    }
    
    @IBAction func saveEventLocation(_ sender: Any) {
        eventLocation = addEventLocation
        
        NotificationCenter.default.post(name: NSNotification.Name.init("setUpLocationAndMap"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

//
//  EditGoingUsersViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class EditGoingUsersViewController: UIViewController {

    // Outlets
    @IBOutlet weak var goingUsersTableView: UITableView!
    @IBOutlet weak var searchField: SearchTextField!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let basicAlerts = BasicAlerts()

    // Variables
    var dataSource: [GoingCell] = []
    var activityIndicator = UIActivityIndicatorView()
    var currentUser = ""
    var eventID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = Spinner().getActivityIndicator(view: goingUsersTableView.self)
        currentUser = AuthService().getCurrentUser()
        setUpSearchField()
        setUpFriendTableView()
        getGoingUsers(eventID: eventID)
    }

    func setUpSearchField() {
        searchField.addTarget(self, action: #selector(searchFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        searchField.textColor = UIColor(named: "SukhTextColor")
        searchField.placeholder = "Search"
    }
    
    func setUpFriendTableView() {
        goingUsersTableView.dataSource = self
        goingUsersTableView.delegate = self
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func searchFieldDidChange(_ textField: UITextField) {
        guard let searchTerm = textField.text, searchTerm.count > 0 else {
            goingUsersTableView.isHidden = true
            sortTable()
            return
        }
        
        getGoingUsersFiltered(searchTerm: searchTerm)
    }
    
    func getGoingUsersFiltered(searchTerm: String) {
        print("Filter users: ", searchTerm)
    }
    
    func getGoingUsers(eventID: String) {
        setLoading(true)
        goingUsersTableView.isHidden = false
        
        mongoQueryService.getGoingUsersOfEvent(eventID: eventID) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!

                let users = Decoder().decodeUsers(data: data)
                
                self.fillDataSource(withUsers: users)
                self.sortTable()
                self.setLoading(false)
            } else {
                print("Failed loading users", statusCode!)
                self.setLoading(false)
            }
        }
    }
    
    @IBAction func searchGoingUsers(_ sender: Any) {
        print("Search")
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func removeUser(sender: UIButton) {
        //let username = dataSource[sender.tag].username
        basicAlerts.setFeatureComingSoonAlert(forViewController: self)
    }
}

// Table view
extension EditGoingUsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let friendCell = tableView.dequeueReusableCell(withIdentifier: "EditGoingUserTableViewCell", for: indexPath) as? EditGoingUserTableViewCell else {
            return UITableViewCell()
        }
                
        // reset image
        friendCell.profileImage.image = nil
            
        let friend = dataSource[indexPath.row]
                
        // Setup eventCell
        friendCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        friendCell.profileImage.sd_setImage(with: URL(string: friend.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        friendCell.name.text = friend.username
        friendCell.removeUser.tag = indexPath.row
        friendCell.removeUser.addTarget(self, action: #selector(removeUser), for: .touchUpInside)

        return friendCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "eventDetailsSeque", sender: dataSource[indexPath.row].eventID)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Helper functions
extension EditGoingUsersViewController {
    
    func sortTable() {
        goingUsersTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            self.dataSource.append(GoingCell(username: user.username!, imageURL: user.profileImageURL!))
        }
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
}


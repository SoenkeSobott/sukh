//
//  GoingViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 06.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

struct GoingCell {
    let username: String
    let imageURL: String
}

class GoingViewController: UIViewController {

    // Outlets
    @IBOutlet weak var searchField: SearchTextField!
    @IBOutlet weak var goingUsersTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    
    // Variables
    var dataSource: [GoingCell] = []
    var activityIndicator = UIActivityIndicatorView()
    var currentUser = ""
    var infoBanner = DecentStyleButton()
    var eventID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = Spinner().getActivityIndicator(view: goingUsersTableView.self)
        currentUser = AuthService().getCurrentUser()
        setUpView()
        setUpSearchField()
        setUpFriendTableView()
        setUpInfoBanner()
        getGoingUsers(eventID: eventID)
    }
    
    func setUpView() {
        view.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func setUpSearchField() {
        searchField.addTarget(self, action: #selector(searchFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        searchField.textColor = UIColor(named: "SukhTextColor")
        searchField.placeholder = "Search"
    }
    
    func setUpFriendTableView() {
        goingUsersTableView.dataSource = self
        goingUsersTableView.delegate = self
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func searchFieldDidChange(_ textField: UITextField) {
        guard let searchTerm = textField.text, searchTerm.count > 0 else {
            infoBanner.isHidden = false
            goingUsersTableView.isHidden = true
            sortTable()
            return
        }
        
        getGoingUsersFiltered(searchTerm: searchTerm)
    }
    
    func getGoingUsersFiltered(searchTerm: String) {
        print("Filter users: ", searchTerm)
    }
    
    func getGoingUsers(eventID: String) {
        setLoading(true)
        infoBanner.isHidden = true
        goingUsersTableView.isHidden = false
        
        mongoQueryService.getGoingUsersOfEvent(eventID: eventID) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!

                let users = Decoder().decodeUsers(data: data)
                
                self.fillDataSource(withUsers: users)
                self.sortTable()
                self.setLoading(false)
            } else {
                NSLog("Error loading going users of event: \(statusCode!)")
                self.setLoading(false)
            }
        }
    }
}

// Table view
extension GoingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let friendCell = tableView.dequeueReusableCell(withIdentifier: "GoingPeopleTableViewCell", for: indexPath) as? GoingPeopleTableViewCell else {
            return UITableViewCell()
        }
                
        // reset image
        friendCell.profileImage.image = nil
            
        let friend = dataSource[indexPath.row]
                
        // Setup eventCell
        friendCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        friendCell.profileImage.sd_setImage(with: URL(string: friend.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        friendCell.name.text = friend.username
        friendCell.addButton.isHidden = true

        return friendCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let username = dataSource[indexPath.row].username
        
        if (!username.elementsEqual(AuthService().getCurrentUser())) {
            performSegue(withIdentifier: "profilSegue", sender: username)
        }
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profilSegue" {
            if let destination = segue.destination as? PublicProfileViewController,
                let selectedUsername = sender as? String {
                destination.username = selectedUsername
            }
        }
    }
}

// Helper functions
extension GoingViewController {
    
    func sortTable() {
        goingUsersTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            self.dataSource.append(GoingCell(username: user.username!, imageURL: user.profileImageURL!))
        }
    }
    
    func setUpInfoBanner() {
        infoBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: self.searchField.frame.origin.y + 60), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        infoBanner.setTitle("No one is going", for: .normal)
            
        infoBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        infoBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        infoBanner.layer.shadowOpacity = 0.5
        infoBanner.clipsToBounds       = true
        infoBanner.layer.masksToBounds = false
                
        infoBanner.isHidden = false
        
        self.view.addSubview(infoBanner)
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
}

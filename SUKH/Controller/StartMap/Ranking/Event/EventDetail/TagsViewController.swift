//
//  TagsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.01.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit


protocol TagsVCToEventDetailVC {
    func setTags(tags: [String])
}

struct CustomEventTag {
    var title: String
}

class TagsViewController: UIViewController {
    
    var TagsVCToEventDetailVCDelegate: TagsVCToEventDetailVC?
    var eventTags = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register to listen for a notification center
        NotificationCenter.default.addObserver(self, selector: #selector(setUpTags(notification:)), name: NSNotification.Name.init("setUpTags"), object: nil)
    }
    
    func setTags(eventTags: [String]) {
        self.eventTags = eventTags
    }
    
    @objc func setUpTags(notification: NSNotification) {
                
        let screenWidth  = Int (UIScreen.main.fixedCoordinateSpace.bounds.width)
        
        // TODO: Max 8 tags
        var xValueOfButton: Int = 0
        var yValueOfButton: Int = 10
        
        var lengthOfCurrentRow: Int = 0
        var widthOfPreviousEventTag: Int = 0
        for (index, eventTag) in eventTags.enumerated(){
            let titleLength = eventTag.count
            // Min button width of 80
            let widthOfCurrentEventTag = titleLength*12 > 80 ? titleLength*12 : 80
            
            lengthOfCurrentRow = lengthOfCurrentRow + widthOfCurrentEventTag
            if (index != 0) {
                if (lengthOfCurrentRow > screenWidth-20) {
                    xValueOfButton = 0
                    yValueOfButton = yValueOfButton + 40
                    lengthOfCurrentRow = widthOfCurrentEventTag + 5
                } else {
                    lengthOfCurrentRow = lengthOfCurrentRow + 5
                    xValueOfButton = xValueOfButton + widthOfPreviousEventTag + 5
                }
            }
            widthOfPreviousEventTag = widthOfCurrentEventTag
            
            createEventTag(xValue: xValueOfButton, yValue: yValueOfButton, width: widthOfCurrentEventTag, height: 30, eventTagName: eventTag)
            
        }
    }
    
    func createEventTag(xValue: Int, yValue: Int, width: Int, height: Int, eventTagName: String) {
        let eventTagLabel = UILabel(frame: CGRect(x: xValue, y: yValue, width: width, height: height))
    
        let backgroundImages: [UIImage] = [#imageLiteral(resourceName: "FilterButtonBackgroundOne"), #imageLiteral(resourceName: "FilterButtonBackgroundTwo"), #imageLiteral(resourceName: "FilterButtonBackgroundThree"), #imageLiteral(resourceName: "FilterButtonBackgroundFour"), #imageLiteral(resourceName: "FilterButtonBackgroundFive"), #imageLiteral(resourceName: "SUCKITButtonBackgroundImage")]
        // Create random number between 0 and 5
        let number = Int.random(in: 0 ..< 6)
        eventTagLabel.backgroundColor = UIColor(patternImage: backgroundImages[number])
            
        eventTagLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
        eventTagLabel.textColor = UIColor.white
        eventTagLabel.text = eventTagName
        eventTagLabel.textAlignment = .center
    
        eventTagLabel.layer.cornerRadius   = 15
        // this 'activates' radius even when te backgroundimage is displayed
        eventTagLabel.clipsToBounds = true
            
        self.view.addSubview(eventTagLabel)
    }
}

//
//  EventDetailViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 18.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import Reachability

class EventDetailViewController: UIViewController, UIGestureRecognizerDelegate, MKMapViewDelegate {

    // Outlets
    @IBOutlet weak var customNavigationBarView: UIView!
    @IBOutlet weak var popViewControllerButton: UIButton!
    @IBOutlet weak var editEventButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var eventViewHeight: NSLayoutConstraint!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventOwner: UILabel!
    @IBOutlet weak var eventLocationMap: MKMapView!
    @IBOutlet weak var eventLocationName: UIButton!
    @IBOutlet weak var isPrivate: UILabel!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var goingUsers: UILabel!
    @IBOutlet weak var iGoButton: AddButton!
    @IBOutlet weak var invitedPeople: UILabel!
    @IBOutlet weak var goingImage: UIImageView!
    
    @IBOutlet weak var startsIn: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var plusCredit: UIImageView!
    @IBOutlet weak var plusCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var plusCreditWidth: NSLayoutConstraint!
    @IBOutlet weak var plusCreditY: NSLayoutConstraint!
    
    // Service
    let authService = AuthService()
    let mongoQueryService = MongoQueryService()
    let reportMongoService = ReportsMongoService()
    let locationService = LocationService()
    let classics = Classics()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    
    // Variables
    var containerViewController: TagsViewController?
    var eventID: String = ""
    var eventTags: [String] = [String]()
    var eventLocation = CLLocation()
    var locationName = ""
    var currentUser = ""
    var going = false
    var activityIndicator = UIActivityIndicatorView()
    var loadImageActivityIndicator = UIActivityIndicatorView()
    var eventIsOver: Bool = false
    var event = Event()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        currentUser = authService.getCurrentUser()
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        connectionService.setReachabilityNotifier(forViewController: self)
        navigationController?.setNavigationBarHidden(true, animated: true)
        getEvent()

    }
    
    func setUpView() {
        self.eventView.backgroundColor = UIColor(named: "SukhBackgroundColor")
        self.eventLocationName.sizeToFit()
        plusCredit.layer.zPosition = 101
        // TODO: set height dynamically
        //self.eventViewHeight.constant = 23
        //self.view.layoutIfNeeded()
    }
    
    func setUpLabels() {
        eventName.textColor = UIColor(named: "SukhTextColor")
        eventOwner.textColor = UIColor(named: "SukhTextColor")
        eventType.textColor = UIColor(named: "SukhTextColor")
    }
    
    func checkIfUserIsAllowedToSeeThisEvent(event: Event) {
        if (event.isPublic == true || currentUser == event.createdBy) {
            view.isUserInteractionEnabled = true
        } else {
            if (event.invitedUsers!.contains(currentUser) || event.goingUsers!.contains(currentUser)) {
                view.isUserInteractionEnabled = true
            } else {
                view.isUserInteractionEnabled = false
                let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = view.bounds
                blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                view.addSubview(blurEffectView)
                customNavigationBarView.addSubview(blurEffectView)
                setPrivateEventAlert()
            }
        }
    }
    
    func setUpGoingButtons(event: Event) {
        goingImage.tintColor = UIColor.white
        if (event.goingUsers!.contains(authService.getCurrentUser())) {
            going = true
            goingImage.image = classics.setSystemImageOrDefault(systemName: "checkmark")
            iGoButton.setTitle("Going", for: .normal)
            self.plusCreditY.constant = -1000
            self.plusCreditWidth.constant = 100
            self.plusCreditHeight.constant = 100
            self.view.layoutIfNeeded()
            self.plusCredit.isHidden = true
        } else {
            going = false
            goingImage.image = classics.setSystemImageOrDefault(systemName: "plus")
            iGoButton.setTitle("Go", for: .normal)
            self.plusCredit.isHidden = true
        }
    }
    
    func getEvent() {
        setLoading(true)
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getEvent(eventID: eventID, completion: { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    
                    self.event = Decoder().decodeSingleEvent(eventData: data)
                                    
                    let endDate = self.classics.dateFromISO8601Date(self.event.endTime!)
                    if (endDate < Date()) {
                        self.eventIsOver = true
                    } else {
                        self.eventIsOver = false
                    }
                    self.setUpEventView(event: self.event)
                    self.eventLocation = CLLocation(latitude: self.event.latitude!, longitude: self.event.longitude!)
                    
                    self.locationService.reverseGeocode(location: self.eventLocation) { (locationString) in
                        self.locationName = locationString
                        self.eventLocationName.setTitle(locationString, for: .normal)
                    }

                    // Setup UI
                    self.checkIfUserIsAllowedToSeeThisEvent(event: self.event)
                    self.setUpView()
                    self.setUpGoingButtons(event: self.event)
                    self.setUpNavigationBar(event: self.event)
                    self.setUpMapView()
                    self.setLoading(false)
                } else {
                    NSLog("Error loading Event: \(statusCode!)")
                    self.setLoading(false)
                }
            })
        }
    }
    
    func setUpNavigationBar(event: Event) {
        UIUtils().setSukhNavigationShadow(forView: customNavigationBarView.self)
        customNavigationBarView.layer.zPosition = 100
        self.navigationItem.title = "Event"
        return
    }
    
    func setUpMapView() {
        //eventLocationMap.isUserInteractionEnabled = false
        eventLocationMap.isZoomEnabled = false
        eventLocationMap.isScrollEnabled = false
        eventLocationMap.mapType = .mutedStandard
        let region = MKCoordinateRegion( center: eventLocation.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
        eventLocationMap.setRegion(eventLocationMap.regionThatFits(region), animated: false)
        
        let event = MKPointAnnotation()
        event.coordinate = eventLocation.coordinate
        eventLocationMap.addAnnotation(event)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnMap))
        eventLocationMap.addGestureRecognizer(tap)
    }
    
    @objc func tapOnMap() {
        // Unwind to StartMapViewController
        self.performSegue(withIdentifier: "goToMap", sender: self)

        // call method in StartMapViewController
        let eventInfo = ["location": CLLocationCoordinate2D(latitude: event.latitude!, longitude: event.longitude!), "eventID": event.eventID as Any, "name": event.name as Any, "type": EventType(rawValue: event.type!) as Any] as [String : Any]
        NotificationCenter.default.post(name: .didReceiveEventLocationData, object: self, userInfo: eventInfo)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "event"
        var view: MKMarkerAnnotationView
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        
        view.glyphTintColor = UIColor(named: "Party")
        
        return view
    }
    
    func setUpEventView(event: Event) {
        eventName.text = event.name
        if (event.createdBy != authService.getCurrentUser()) {
            eventOwner.text = "By " + event.createdBy!
        } else {
            eventOwner.text = "By You"
        }
        eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventImage.sd_setImage(with: URL(string: event.eventImageURL!), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        eventLocationName.setTitle(locationName, for: .normal)
        isPrivate.text = (event.isPublic! ? "Public" : "Private")
        eventType.text = event.type
        startsIn.attributedText = classics.timeToStart(ISO8601DateStringStart: event.startTime!, ISO8601DateStringEnd: event.endTime!, size: 18)
        startTime.text = classics.stringFromISO8601Date(event.startTime!)
        endTime.text = classics.stringFromISO8601Date(event.endTime!)
        eventTags = event.tags!
        // Going People TextField
        goingUsers.text = String(event.goingUsers!.count) + " going"
        let tapOnGoingPeople = UITapGestureRecognizer(target: self,action: #selector(handleTapOnGoingPeopleTextField(_:)))
        tapOnGoingPeople.numberOfTapsRequired = 1
        goingUsers.isUserInteractionEnabled = true
        goingUsers.addGestureRecognizer(tapOnGoingPeople)
        
        // Invited TextField
        let invited: String = String(event.invitedUsers!.count)
        invitedPeople.text = "\(invited) invited"
        let tapOnInvitedPeople = UITapGestureRecognizer(target: self,action: #selector(handleTapOnInvitedPeopleTextField(_:)))
        tapOnInvitedPeople.numberOfTapsRequired = 1
        invitedPeople.isUserInteractionEnabled = true
        invitedPeople.addGestureRecognizer(tapOnInvitedPeople)
        
        // Set eventTags in TagsViewController
        containerViewController?.setTags(eventTags: eventTags)
        // Notify TagsViewController that eventTags were passed -> start with setUpTags()
        NotificationCenter.default.post(name: NSNotification.Name.init("setUpTags"), object: nil)
    
    }
    
    @objc func handleTapOnGoingPeopleTextField(_ sender: UITapGestureRecognizer) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:"GoingViewController") as? GoingViewController {
            vc.modalPresentationStyle = .popover
            vc.eventID = eventID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func iGoButton(_ sender: Any) {
        if (eventIsOver) {
            basicAlerts.setEventIsOverBanner(forViewController: self)
        } else {
            if (going) {
                mongoQueryService.deleteUserFromEventGoingUsers(eventID: eventID, username: authService.getCurrentUser()) { (response) in
                    let statusCode = response.response?.statusCode
                    if (statusCode == 200) {
                        NSLog("Successfully removed user from event with status code: \(statusCode!)")
                        self.minusCreditAnimation()
                    } else {
                        NSLog("Failed removing user from event with status code: \(statusCode!)")
                    }
                }
            } else {
                mongoQueryService.updateEventGoingUsers(eventID: eventID, username: authService.getCurrentUser()) { (response) in
                    let statusCode = response.response?.statusCode
                    if (statusCode == 200) {
                        NSLog("Successfully added user to event with status code: \(statusCode!)")
                        self.plusCreditAnimation()
                    } else {
                        NSLog("Failed adding user to event with status code: \(statusCode!)")
                    }
                }
            }
        }
    }
    
    func plusCreditAnimation() {
        goingImage.image = classics.setSystemImageOrDefault(systemName: "checkmark")
        iGoButton.setTitle("Going", for: .normal)
        plusCredit.isHidden = false
        UIView.animate(withDuration: 1.5, animations: {
            let realOrigin = self.view.convert(self.plusCredit.frame.origin, to: self.view)
            let topSpace = self.view.frame.minY - (self.plusCredit.frame.height + realOrigin.y)
            
            self.plusCreditY.constant = topSpace - 50
            self.plusCreditWidth.constant = 100
            self.plusCreditHeight.constant = 100
            self.view.layoutIfNeeded()
        } , completion: {(finished:Bool) in
            self.getEvent()
        })
    }
    
    func minusCreditAnimation() {
        goingImage.image = classics.setSystemImageOrDefault(systemName: "plus")
        iGoButton.setTitle("Go", for: .normal)
        plusCredit.isHidden = false
        UIView.animate(withDuration: 1.5, animations: {
            self.plusCreditY.constant = 0
            self.plusCreditWidth.constant = 10
            self.plusCreditHeight.constant = 10
            self.view.layoutIfNeeded()
        } , completion: {(finished:Bool) in
            self.plusCredit.isHidden = true
            self.getEvent()
        })
    }
    
    @objc func handleTapOnInvitedPeopleTextField(_ sender: UITapGestureRecognizer) {
        /*if let vc = self.storyboard?.instantiateViewController(withIdentifier:"GoingViewController") as? GoingViewController {
            vc.modalPresentationStyle = .popover
            self.present(vc, animated: true, completion: nil)
        }*/
    }
    
    @IBAction func popViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToLocation(_ sender: Any) {
        let regionDistance:CLLocationDistance = 1000
        let coordinates = eventLocation.coordinate
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        let eventNameWithAddress = eventName.text! + " - " + locationName
        mapItem.name = eventNameWithAddress
        mapItem.openInMaps(launchOptions: options)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tagsContainerSegue" {
            containerViewController = segue.destination as? TagsViewController
            containerViewController!.TagsVCToEventDetailVCDelegate = self as? TagsVCToEventDetailVC
        }
        
        if segue.identifier == "eventEditSegue" {
            if let destination = segue.destination as? EditEventViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        
        eventLocationMap.isHidden = loading
        scrollView.isUserInteractionEnabled = !loading
        editButtonItem.isEnabled = !loading
    }
    
    func setPrivateEventAlert() {
        let userNotExistingAlert = UIAlertController(title: "Private Event", message: "This event is private", preferredStyle: .alert)
            
        userNotExistingAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(userNotExistingAlert, animated: true)
    }
    
}

// Reachability
extension EventDetailViewController {
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            getEvent()
        case .cellular:
            getEvent()
        case .none:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not reachable")
        case .unavailable:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not available")
        }
    }
}

// Report Event
extension EventDetailViewController {
    
    @IBAction func eventActions(_ sender: UIButton) {
        let alert = UIAlertController(title: "Event", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Report Event", style: .destructive, handler: { _ in
            self.setReportEventAlert()
        }))

        if (authService.getCurrentUser() == event.createdBy) {
            alert.addAction(UIAlertAction(title: "Delete Event", style: .destructive, handler: { _ in
                self.deleteEvent()
            }))
            alert.addAction(UIAlertAction(title: "Edit Event", style: .default, handler: { _ in
                self.editEvent()
            }))
        }

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func editEvent() {
        performSegue(withIdentifier: "eventEditSegue", sender: eventID)
    }
    
    func setReportEventAlert() {
        let reportUserAlert = UIAlertController(title: "Report Event", message: "Do you want to report this Event?", preferredStyle: .alert)
            
        reportUserAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            // Report event
            let ISO8601DateString = ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime])
            
            let newReport = Report(reportId: UUID().uuidString,
                                   type: ReportType.Event.rawValue,
                                   subject: self.event.eventID,
                                   createdBy: self.currentUser,
                                   createdAt: ISO8601DateString)
            
            self.reportMongoService.postReport(report: newReport) { (statusCode) in
                if (statusCode == 201) {
                    self.basicAlerts.setReportEventAlert(forViewController: self)
                    NSLog("Successfully reported event '" + self.eventID + "' with status code: \(statusCode)")
                } else {
                    NSLog("Error reporting event '" + self.eventID + "' with status code: \(statusCode)")
                }
            }
        }))
        reportUserAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(reportUserAlert, animated: true)
    }
    
    func deleteEvent() {
        let deleteEventAlert = UIAlertController(title: "Delete Event", message: "Do you really want to delete this Event? All data will be lost.", preferredStyle: .alert)
            
        deleteEventAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            self.setLoading(true)
            self.mongoQueryService.deleteEvent(id: self.eventID) { (statusCode) in
                if (statusCode == 200) {
                    self.basicAlerts.setDeleteEventAlert(forViewController: self)
                    self.setLoading(false)
                    
                    // Pop to StartMapViewController
                    let navigationController = self.navigationController
                    if (navigationController != nil) {
                        // Called from pushed VC
                        navigationController?.popToViewController(ofClass: StartMapViewController.self)
                    } else {
                        // Called from popover VC
                        // TODO: This will only work if there are two VC underlying
                        NotificationCenter.default.post(name: .setAlpha, object: self)
                        NotificationCenter.default.post(name: .reloadMap, object: self)
                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                    }
                } else {
                    NSLog("Error deleting event '\(self.eventID)' - \(statusCode)")
                    self.setLoading(false)
                }
            }
        }))
        
        deleteEventAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        deleteEventAlert.view.accessibilityIdentifier = "deleteEvent"

        self.present(deleteEventAlert, animated: true)
    }
}

extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}

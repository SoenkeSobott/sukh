//
//  PageRootViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 07.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class PageRootViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    // Variables
    var eventID = ""
    var firstView: UIScrollView?
    var swipeBackPanGestureRecognizer: UIPanGestureRecognizer?

    lazy var viewControllerList: [UIViewController] = {
       let sb = UIStoryboard(name: "EventDetail", bundle: nil)
        
        let vc1 = sb.instantiateViewController(withIdentifier: "EventDetailViewController") as! EventDetailViewController
        vc1.eventID = eventID
        
        let vc2 = sb.instantiateViewController(withIdentifier: "EventFeedViewController") as! EventFeedViewController
        vc2.eventID = eventID
        
        return [vc1, vc2]

    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.dataSource = self
        
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        
        firstView = view.subviews.filter{ $0 is UIScrollView }.first as? UIScrollView

        if let scrollView = firstView,
           let popGestureRecognizer = self.navigationController?.interactivePopGestureRecognizer,
           let targets = popGestureRecognizer.value(forKey: "targets") as? NSMutableArray {
            let panGestureRecognizer = UIPanGestureRecognizer()
            panGestureRecognizer.setValue(targets, forKey: "targets")
            panGestureRecognizer.delegate = self
            scrollView.addGestureRecognizer(panGestureRecognizer)
            swipeBackPanGestureRecognizer = panGestureRecognizer
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = viewControllerList.firstIndex(of: viewController) else { return nil  }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return nil }
        
        guard viewControllerList.count > previousIndex else { return nil }
        
        return viewControllerList[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = viewControllerList.firstIndex(of: viewController) else { return nil  }
        
        let nextIndex = viewControllerIndex + 1
        
        guard viewControllerList.count != nextIndex else { return nil }

        guard viewControllerList.count > nextIndex else { return nil }
        
        return viewControllerList[nextIndex]
    }
}

extension PageRootViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let panRecognizer = gestureRecognizer as? UIPanGestureRecognizer,
            panRecognizer == swipeBackPanGestureRecognizer else {
            return true
        }

        guard let currentViewController = self.viewControllers?.first,
            pageViewController(self, viewControllerBefore: currentViewController) == nil else {
            return false
        }

        guard let gestureView = panRecognizer.view else {
            return true
        }

        let velocity = (panRecognizer.velocity(in: gestureView)).x
        return velocity > 0
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == swipeBackPanGestureRecognizer && otherGestureRecognizer == firstView?.panGestureRecognizer {
            return true
        } else {
            return false
        }
    }
}

//
//  SearchViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 30.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class SearchViewController: UIViewController {

    // Outlets
    @IBOutlet weak var popViewControllerButton: UIButton!
    @IBOutlet weak var eventsButton: UIButton!
    @IBOutlet weak var eventsIndicator: UIImageView!
    @IBOutlet weak var usersButton: UIButton!
    @IBOutlet weak var usersIndicator: UIImageView!
    @IBOutlet weak var placesButton: UIButton!
    @IBOutlet weak var placesIndicator: UIImageView!
    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var usersView: UIView!
    @IBOutlet weak var placesView: UIView!
    
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    
    // Variables
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        searchUsers(self)
       
    }
    
    @IBAction func popViewControllerButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchEvents(_ sender: Any) {
        usersButton.tintColor = UIColor.lightGray
        usersIndicator.isHidden = true
        placesButton.tintColor = UIColor.lightGray
        placesIndicator.isHidden = true
        
        eventsButton.tintColor = UIColor(named: "SukhTextColor")
        eventsIndicator.isHidden = false
        
        eventsView.alpha = 1
        let childs = self.children
        let searchEventsVC = childs[0] as! SearchEventsViewController
        searchEventsVC.eventsSearchBar.becomeFirstResponder() // Show keyboard
        usersView.alpha = 0
        placesView.alpha = 0
    }
    
    @IBAction func searchUsers(_ sender: Any) {
        eventsButton.tintColor = UIColor.lightGray
        eventsIndicator.isHidden = true
        placesButton.tintColor = UIColor.lightGray
        placesIndicator.isHidden = true
        
        usersButton.tintColor = UIColor(named: "SukhTextColor")
        usersIndicator.isHidden = false
        
        eventsView.alpha = 0
        usersView.alpha = 1
        let childs = self.children
        let searchUsersVC = childs[1] as! SearchUsersViewController
        searchUsersVC.usersSearchBar.becomeFirstResponder() // Show keyboard
        placesView.alpha = 0
    }
    
    @IBAction func searchPlaces(_ sender: Any) {
        eventsButton.tintColor = UIColor.lightGray
        eventsIndicator.isHidden = true
        usersButton.tintColor = UIColor.lightGray
        usersIndicator.isHidden = true
        
        placesButton.tintColor = UIColor(named: "SukhTextColor")
        placesIndicator.isHidden = false
        
        eventsView.alpha = 0
        usersView.alpha = 0
        placesView.alpha = 1
        let childs = self.children
        let placesUsersVC = childs[2] as! SearchPlacesViewController
        placesUsersVC.placesSearchBar.becomeFirstResponder() // Show keyboard
    }
    
    
}

extension UIView {
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}


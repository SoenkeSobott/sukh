//
//  SearchUsersViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 01.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

enum FollowerStatus {
    case Following
    case NotFollowing
}

struct UserCell {
    let username: String
    let imageURL: String
    let followerStatus: FollowerStatus
}

class SearchUsersViewController: UIViewController {

    // Outlets
    @IBOutlet weak var usersSearchBar: UISearchBar!
    @IBOutlet weak var usersTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    let authService = AuthService()
    
    // Variables
    var dataSource: [UserCell] = []
    var activityIndicator = UIActivityIndicatorView()
    var currentUser = ""
    var infoBanner = DecentStyleButton()
    var isSearching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getActivityIndicator(view: usersTableView.self)
        
        if (authService.getRoleOfCurrentUser() != .Guest) {
            currentUser = authService.getCurrentUser()
        }
        
        setUpView()
        setUpSearchBar()
        setUpUserTableView()
        setUpInfoBanner()
    }
    
    func setUpView() {
        view.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func setUpSearchBar() {
        usersSearchBar.delegate = self
        usersSearchBar.returnKeyType = UIReturnKeyType.done
    }
    
    func setUpUserTableView() {
        usersTableView.dataSource = self
        usersTableView.delegate = self
    }
    
    func getUsersBy(searchTerm: String) {
        setLoading(true)
        
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getUsersBySearchTerm(username: authService.getCurrentUser(), searchTerm: searchTerm) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!

                    let users = Decoder().decodeUsers(data: data)
                    
                    self.fillDataSource(withUsers: users)
                    self.sortTable()
                    self.setLoading(false)
                } else {
                    NSLog("Error occured while loading users: \(statusCode)")
                    self.setLoading(false)
                }
            }
        }
    }
}

// Search bar
extension SearchUsersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            dataSource = []
            sortTable()
            infoBanner.isHidden = false
        } else {
            isSearching = true
            infoBanner.isHidden = true
            getUsersBy(searchTerm: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        usersSearchBar.resignFirstResponder()
    }
}

// Table view
extension SearchUsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let userCell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserTableViewCell else {
            return UITableViewCell()
        }
                
        // reset image
        userCell.profileImage.image = nil
            
        let user = dataSource[indexPath.row]
        
        if (user.username.elementsEqual(currentUser) || authService.getRoleOfCurrentUser() == .Guest) {
            userCell.actionButton.isHidden = true
        } else {
            userCell.actionButton.isHidden = false
            if (user.followerStatus == FollowerStatus.Following) {
                userCell.actionButton.isUserInteractionEnabled = true
                userCell.actionButton.setTitle("Following", for: .normal)
                userCell.actionButton.tag = indexPath.row
                userCell.actionButton.addTarget(self, action: #selector(unfollow), for: UIControl.Event.touchUpInside)
            } else {
                userCell.actionButton.isUserInteractionEnabled = true
                userCell.actionButton.setTitle("Follow", for: .normal)
                userCell.actionButton.tag = indexPath.row
                userCell.actionButton.addTarget(self, action: #selector(follow), for: UIControl.Event.touchUpInside)
            }
        }
        
        // Setup eventCell
        userCell.name.text = user.username
        
        userCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userCell.profileImage.sd_setImage(with: URL(string: user.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        return userCell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        usersSearchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let username = dataSource[indexPath.row].username
        
        if (authService.getRoleOfCurrentUser() != .Guest) {
            if (!username.elementsEqual(currentUser)) {
                performSegue(withIdentifier: "profilSegue", sender: username)
            }
        } else {
            setSignUpAlert()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profilSegue" {
            if let destination = segue.destination as? PublicProfileViewController,
                let selectedUsername = sender as? String {
                destination.username = selectedUsername
            }
        }
    }
}

// Helper functions
extension SearchUsersViewController {
    
    @objc func follow(sender: UIButton) {
        sender.isUserInteractionEnabled = false
        let username = dataSource[sender.tag].username
        let activityIndicator = Spinner().getImageUploadActivityIndicator(view: sender.imageView!)
        activityIndicator.startAnimating()
        
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.addFollower(user: username, requestingUser: currentUser) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully followed user '\(username)' with status code: \(statusCode)")
                    sender.removeTarget(self, action: #selector(self.follow), for: UIControl.Event.touchUpInside)
                        
                    // Add new target
                    sender.setTitle("Following", for: .normal)
                    sender.addTarget(self, action: #selector(self.unfollow), for: UIControl.Event.touchUpInside)
                    
                } else {
                    NSLog("Error following user '\(username)' with status code: \(statusCode)")
                }
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                sender.isUserInteractionEnabled = true
            }
        }
    }
       
    @objc func unfollow(sender: UIButton) {
        sender.isUserInteractionEnabled = false
        let username = dataSource[sender.tag].username
        let activityIndicator = Spinner().getImageUploadActivityIndicator(view: sender.imageView!)
        activityIndicator.startAnimating()
           
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.removeFollower(user: username, removeUser: currentUser) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully unfollowed user '\(username)' with status code: \(statusCode)")
                    sender.removeTarget(self, action: #selector(self.unfollow), for: UIControl.Event.touchUpInside)
                        
                    // Add new target
                    sender.setTitle("Follow", for: .normal)
                    sender.addTarget(self, action: #selector(self.follow), for: UIControl.Event.touchUpInside)
                } else {
                    NSLog("Error unfollowing user '\(username)' with status code: \(statusCode)")
                }
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                sender.isUserInteractionEnabled = true
            }
        }
    }

    
    func sortTable() {
        usersTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            var followerStatus = FollowerStatus.NotFollowing
            if (user.followers?.contains(currentUser) ?? false) { // TODO: do this other way around check if i follow him
                followerStatus = FollowerStatus.Following
            }
            self.dataSource.append(UserCell(username: user.username!, imageURL: user.profileImageURL!, followerStatus: followerStatus))
        }
    }
    
    func setUpInfoBanner() {
        infoBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: self.usersTableView.frame.origin.y + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        infoBanner.setTitle("Find interesting people", for: .normal)
            
        infoBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        infoBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        infoBanner.layer.shadowOpacity = 0.5
        infoBanner.clipsToBounds       = true
        infoBanner.layer.masksToBounds = false
                
        infoBanner.isHidden = false
        
        self.view.addSubview(infoBanner)
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
    func setSignUpAlert() {
        let signUpAlert = UIAlertController(title: "You want more?", message: "Sign in to get the full experience", preferredStyle: .alert)
            
        signUpAlert.addAction(UIAlertAction(title: "Sign in", style: .default, handler: { (alert: UIAlertAction!) in
            // Go to login
            self.authService.removeUserAndPasswordAndRole()
            self.authService.resetUserDefaults()
            self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
        }))
        
        signUpAlert.addAction(UIAlertAction(title: "Later", style: .cancel, handler: nil))

        self.present(signUpAlert, animated: true)
    }
}

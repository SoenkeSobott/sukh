//
//  SearchPlacesViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 01.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit

struct PlaceCell {
    let name: String
    let placeDescription: String
    let location: MKPlacemark
}

class SearchPlacesViewController: UIViewController {

    // Outlets
    @IBOutlet weak var placesSearchBar: UISearchBar!
    @IBOutlet weak var placesTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    let locationService = LocationService()
    let authService = AuthService()
    
    // Variables
    var dataSource: [PlaceCell] = []
    var activityIndicator = UIActivityIndicatorView()
    var infoBanner = DecentStyleButton()
    var isSearching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getActivityIndicator(view: placesTableView.self)
        setUpView()
        setUpEventsTableView()
        setUpInfoBanner()
        
        placesSearchBar.delegate = self
        placesSearchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
    func setUpView() {
        view.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func setUpEventsTableView() {
        placesTableView.dataSource = self
        placesTableView.delegate = self
    }
    
    func getPlacesBy(searchTerm: String) {
        setLoading(true)
        
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchTerm
        if #available(iOS 13.0, *) {
            searchRequest.pointOfInterestFilter = MKPointOfInterestFilter(including: [.university, .atm, .bank, .beach, .brewery, .fitnessCenter, .hospital, .hotel, .nightlife, .park, .parking, .publicTransport, .restaurant])
        }
        let search = MKLocalSearch(request: searchRequest)
        dataSource = []
        search.start { response, error in
            guard let response = response else {
                NSLog("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }

            for item in response.mapItems {
                let place = PlaceCell(name: item.name!, placeDescription: item.description, location: item.placemark)
                self.dataSource.append(place)
            }
            self.setLoading(false)
            self.sortTable()
        }


    }
}

// Search bar
extension SearchPlacesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            dataSource = []
            sortTable()
            infoBanner.isHidden = false
            placesTableView.isHidden = true
        } else {
            isSearching = true
            infoBanner.isHidden = true
            placesTableView.isHidden = false
            getPlacesBy(searchTerm: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        placesSearchBar.resignFirstResponder()
    }
}

// Table view
extension SearchPlacesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let placeCell = tableView.dequeueReusableCell(withIdentifier: "PlacesTableViewCell", for: indexPath) as? PlacesTableViewCell else {
            return UITableViewCell()
        }
            
        let place = dataSource[indexPath.row]
        
        // Setup placeCell
        
        placeCell.name.text = place.name
        
        // Calculate distance to user location
        let latitude = place.location.coordinate.latitude
        let longitude = place.location.coordinate.longitude
        locationService.reverseGeocodeCityCountry(location: CLLocation(latitude: latitude, longitude: longitude)) { (city) in
            placeCell.locationName.text = city
        }
        
        //placeCell.placesAction.tag = indexPath.row
        placeCell.placesAction.isUserInteractionEnabled = false
        //placeCell.placesAction.addTarget(self, action: #selector(showOnMap), for: UIControl.Event.touchUpInside)
                        
        return placeCell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        placesSearchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showOnMap(row: indexPath.row)

        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailsSegue" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
}

// Helper functions
extension SearchPlacesViewController {
    
    @objc func showOnMap(row: Int) {
        let name = dataSource[row].name
        let location = dataSource[row].location
                
        // Pop ViewController
        self.navigationController?.popViewController(animated: true)
        
        // call method in StartMapViewController
        let eventInfo = ["location": CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), "name": name] as [String : Any]
        NotificationCenter.default.post(name: .didReceiveLocationData, object: self, userInfo: eventInfo)
        
    }
    
    func sortTable() {
        placesTableView.reloadData()
    }
    
    func setUpInfoBanner() {
        infoBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: self.placesTableView.frame.origin.y + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        infoBanner.setTitle("Find places to explore", for: .normal)
            
        infoBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        infoBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        infoBanner.layer.shadowOpacity = 0.5
        infoBanner.clipsToBounds       = true
        infoBanner.layer.masksToBounds = false
                
        infoBanner.isHidden = false
        
        self.view.addSubview(infoBanner)
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
}

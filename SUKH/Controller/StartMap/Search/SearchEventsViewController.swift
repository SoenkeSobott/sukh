//
//  SearchEventsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 01.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation

struct SearchEventCell {
    let eventID: String
    let imageURL: String
    let name: String
    let distance: Double
    let latitude: Double
    let longitude: Double
    let type: EventType
}

class SearchEventsViewController: UIViewController {

    // Outlets
    @IBOutlet weak var eventsSearchBar: UISearchBar!
    @IBOutlet weak var eventsTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    let locationService = LocationService()
    let authService = AuthService()
    
    // Variables
    var dataSource: [SearchEventCell] = []
    var activityIndicator = UIActivityIndicatorView()
    var infoBanner = DecentStyleButton()
    var isSearching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getActivityIndicator(view: eventsTableView.self)
        setUpView()
        setUpEventsTableView()
        setUpInfoBanner()
        
        eventsSearchBar.delegate = self
        eventsSearchBar.returnKeyType = UIReturnKeyType.done
    }
    
    func setUpView() {
        view.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func setUpEventsTableView() {
        eventsTableView.dataSource = self
        eventsTableView.delegate = self
    }
    
    func getEventsBy(searchTerm: String) {
        setLoading(true)
        
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getEventsBySearchTerm(requestingUser: authService.getCurrentUser(), searchTerm: searchTerm) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!

                    let events = Decoder().decodeEvents(eventData: data)
                    
                    self.fillDataSource(withEvents: events)
                    self.sortTable()
                    self.setLoading(false)
                } else {
                    NSLog("Error loading users: \(statusCode!)")
                    self.setLoading(false)
                }
            }
        }
    }
}

// Search bar
extension SearchEventsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            dataSource = []
            sortTable()
            infoBanner.isHidden = false
        } else {
            isSearching = true
            infoBanner.isHidden = true
            getEventsBy(searchTerm: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        eventsSearchBar.resignFirstResponder()
    }
}

// Table view
extension SearchEventsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as? EventsTableViewCell else {
            return UITableViewCell()
        }
                
        // reset image
        eventCell.eventsImage.image = nil
            
        let event = dataSource[indexPath.row]
        
        // Setup eventCell
        eventCell.eventsImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventCell.eventsImage.sd_setImage(with: URL(string: event.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        eventCell.eventsName.text = event.name
        eventCell.eventsDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
        
        eventCell.eventAction.tag = indexPath.row
        eventCell.eventAction.addTarget(self, action: #selector(showOnMap), for: UIControl.Event.touchUpInside)
        
        return eventCell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        eventsSearchBar.resignFirstResponder()
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "eventDetailsSegue", sender: dataSource[indexPath.row].eventID)

        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailsSegue" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
}

// Helper functions
extension SearchEventsViewController {
    
    @objc func showOnMap(sender: UIButton) {
        let eventID = dataSource[sender.tag].eventID
        let latitude = dataSource[sender.tag].latitude
        let longitude = dataSource[sender.tag].longitude
        let name = dataSource[sender.tag].name
        let type = dataSource[sender.tag].type
        
        // Pop ViewController
        self.navigationController?.popViewController(animated: true)
        
        // call method in StartMapViewController
        let eventInfo = ["location": CLLocationCoordinate2D(latitude: latitude, longitude: longitude), "eventID": eventID, "name": name, "type": type] as [String : Any]
        NotificationCenter.default.post(name: .didReceiveEventLocationData, object: self, userInfo: eventInfo)
        
    }
    
    func sortTable() {
        eventsTableView.reloadData()
    }
    
    func fillDataSource(withEvents: [Event]) {
        dataSource = []
        for event in withEvents {
            self.dataSource.append(SearchEventCell(eventID: event.eventID!,
                                             imageURL: event.eventImageURL!,
                                             name: event.name!,
                                             distance: classics.calculateDistanceInMeters(latitude: event.latitude!, longitude: event.longitude!),
                                             latitude: event.latitude!,
                                             longitude: event.longitude!,
                                             type: EventType(rawValue: event.type!)!))
        }
    }
    
    func setUpInfoBanner() {
        infoBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: self.eventsTableView.frame.origin.y + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        infoBanner.setTitle("Find the best events", for: .normal)
            
        infoBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        infoBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        infoBanner.layer.shadowOpacity = 0.5
        infoBanner.clipsToBounds       = true
        infoBanner.layer.masksToBounds = false
                
        infoBanner.isHidden = false
        
        self.view.addSubview(infoBanner)
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
}

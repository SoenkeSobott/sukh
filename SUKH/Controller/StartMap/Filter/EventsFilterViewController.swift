//
//  EventsFilterViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 13.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

struct EventFilterCell {
    let filterID: String
    let filterName: String
    let filter: EventFilter
    let info: String
    let rank: Int
}

class EventsFilterViewController: UIViewController {

    // Outlets
    @IBOutlet weak var filterTableView: UITableView!
    
    // Servics
    let locationService = LocationService()
    
    // Variables
    var upcoming: Bool = true
    var dataSource: [EventFilterCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpFilterTableView()
        setUpFilters()
        sortTable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.post(name: NSNotification.Name.init("setFiltersRanking"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name.init("setFiltersMap"), object: nil)
    }
    
    func setUpFilterTableView() {
        filterTableView.dataSource = self
        filterTableView.delegate = self
    }
    
    func setUpFilters() {
        let upcomingFilter = EventFilter(id: "01",
                                         name: "Upcoming",
                                         from: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                         to: nil,
                                         tags: [],
                                         types: [])
        
        let previousFilter = EventFilter(id: "02",
                                         name: "Previous",
                                         from: nil,
                                         to: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                         tags: [],
                                         types: [])
        
        
        
        let endOfWeek = Calendar.current.date(byAdding: .day, value: +1, to: Date().endOfWeek!)!
        let thisWeekFilter = EventFilter(id: "03",
                                         name: "This Week",
                                         from: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                         to: ISO8601DateFormatter.string(from: endOfWeek, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                         tags: [],
                                         types: [])
        
        // Calculate weekend
        let now = Date()
        var weekendInterval = DateInterval()
        if Calendar.current.isDateInWeekend(now) {
            weekendInterval = Calendar.current.nextWeekend(startingAfter: Calendar.current.date(byAdding: .day, value: -7, to: now)!)!
        } else {
            weekendInterval = Calendar.current.nextWeekend(startingAfter: now)!
        }
        let startDate = Calendar.current.date(byAdding: .day, value: -1, to: weekendInterval.start)! // Weekend starts on friday instead of saturday
        let endDate = weekendInterval.end
        let thisWeekendFilter = EventFilter(id: "04",
                                            name: "This Weekend",
                                            from: ISO8601DateFormatter.string(from: startDate, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                            to: ISO8601DateFormatter.string(from: endDate, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                            tags: [],
                                            types: [])
        
        // Next Weekend
        // Last Week
        // Last Weekend
        // Altime best parties
        
        let upcoming = EventFilterCell(filterID: upcomingFilter.id!, filterName: upcomingFilter.name!, filter: upcomingFilter, info: "All upcoming events are presented", rank: 5)
        let previous = EventFilterCell(filterID: previousFilter.id!, filterName: previousFilter.name!, filter: previousFilter, info: "All previous events are presented", rank: 6)
        let thisWeek = EventFilterCell(filterID: thisWeekFilter.id!, filterName: thisWeekFilter.name!, filter: thisWeekFilter, info: "All Events till next week", rank: 7)
        let thisWeekend = EventFilterCell(filterID: thisWeekendFilter.id!, filterName: thisWeekendFilter.name!, filter: thisWeekendFilter, info: "All Events for this weekend", rank: 8)

        dataSource.append(upcoming)
        dataSource.append(previous)
        dataSource.append(thisWeek)
        dataSource.append(thisWeekend)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// table view
extension EventsFilterViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let filterCell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as? FilterTableViewCell else {
            return UITableViewCell()
        }
                
        let filter = dataSource[indexPath.row]
                
        // Setup filterCell
        filterCell.name.text = filter.filterName
        
        // Check if selected
        if (locationService.getMapFilter().id == filter.filterID) {
            filterCell.filterView.layer.borderWidth = 2
            filterCell.filterView.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
            filterCell.filterView.layer.shadowOpacity = 0.5
        }

        return filterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filter = dataSource[indexPath.row].filter
                
        locationService.setMapFilter(eventFilter: filter)
        self.dismissView(self)
    }
    
    // Functions to reorder
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = dataSource[sourceIndexPath.row]
        dataSource.remove(at: sourceIndexPath.row)
        dataSource.insert(item, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    func sortTable() {
        dataSource.sort(by: {$0.rank > $1.rank})
        filterTableView.reloadData()
    }
}

// Date extension
extension Date {
    var startOfWeek: Date? {
        let calendar = Calendar(identifier: .gregorian)
        guard let sunday = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return calendar.date(byAdding: .day, value: 1, to: sunday)
    }

    var endOfWeek: Date? {
        let calendar = Calendar(identifier: .gregorian)
        guard let sunday = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return calendar.date(byAdding: .day, value: 7, to: sunday)
    }
}

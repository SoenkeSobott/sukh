//
//  AddFilterViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 04.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class AddFilterViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var typeTextField: InputTextField!
    @IBOutlet weak var typePicker: UIPickerView!
    @IBOutlet weak var timeTextField: InputTextField!
    @IBOutlet weak var timePicker: UIPickerView!
    
    // Services
    
    // Variables
    var eventTypePickerVisible = false
    var eventTypePickerSelectedRow = 1
    let eventTypes = ["Party", "Hangout", "Productive", "All"]
    var timePickerVisible = false
    var timePickerSelectedRow = 1
    let times = ["Previous", "Upcoming", "All Time","This Weekend", "This Week", "This Month"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpTypePicker()
        setUpTimePicker()
        scrollView.delegate = self
        scrollView.keyboardDismissMode = .onDrag
        
        // standardcfilter in app additional in user acount in database
        
        // Your Recommendations filter -> based on last visited events. Goal: Go in to new city and instantly find the prefect night for you
    }
    
    func setUpTypePicker() {
        // TextField
        let tap = UITapGestureRecognizer(target: self,action: #selector(handleTaponTextField(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        typeTextField.isUserInteractionEnabled = true
        typeTextField.addGestureRecognizer(tap)
        typeTextField.placeholder = "Event type"
        
        // Picker
        typePicker.tag = 1
        typePicker.isHidden = true
        typePicker.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setUpTimePicker() {
        // TextField
        let tap = UITapGestureRecognizer(target: self,action: #selector(handleTaponTimeTextField(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        timeTextField.isUserInteractionEnabled = true
        timeTextField.addGestureRecognizer(tap)
        timeTextField.placeholder = "Time"
        
        // Picker
        timePicker.tag = 2
        timePicker.isHidden = true
        timePicker.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBAction func saveFilter(_ sender: Any) {
        
        // TODO: Save filter
        self.dismiss(animated: true, completion: nil)
    }
}

// EventType picker
extension AddFilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView.tag == 1) {
            return eventTypes[row]
        } else {
            return times[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1) {
            return eventTypes.count
        } else {
            return times.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 1) {
            eventTypePickerSelectedRow = row
            typeTextField.text = eventTypes[row]
        } else {
            timePickerSelectedRow = row
            timeTextField.text = times[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    @objc func handleTaponTextField(_ sender: UITapGestureRecognizer) {
        if (eventTypePickerVisible) {
            hidePickerView()
        } else {
            showPickerView()
        }
    }
    
    func showPickerView() {
        eventTypePickerVisible = true
        typePicker.selectRow(eventTypePickerSelectedRow, inComponent: 0, animated: false)
        typeTextField.text = eventTypes[eventTypePickerSelectedRow]
                
        if let constraint = (typePicker.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 90.0
            typePicker.layoutIfNeeded()
            typePicker.isHidden = false
        }
    }
    
    func hidePickerView() {
        eventTypePickerVisible = false
        
        if let constraint = (typePicker.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 0.0
           UIView.animate(withDuration: 0.3, animations: {
                 self.view.layoutIfNeeded()
            })
            typePicker.isHidden = true
        }
    }
    
    // time picker
    @objc func handleTaponTimeTextField(_ sender: UITapGestureRecognizer) {
        if (timePickerVisible) {
            hideTimePickerView()
        } else {
            showTimePickerView()
        }
    }
    
    func showTimePickerView() {
        timePickerVisible = true
        timePicker.selectRow(timePickerSelectedRow, inComponent: 0, animated: false)
        timeTextField.text = times[timePickerSelectedRow]
                
        if let constraint = (timePicker.constraints.filter{$0.identifier == "timePickerHeight"}.first) {
            constraint.constant = 90.0
            timePicker.layoutIfNeeded()
            timePicker.isHidden = false
        }
    }
    
    func hideTimePickerView() {
        timePickerVisible = false
        
        if let constraint = (timePicker.constraints.filter{$0.identifier == "timePickerHeight"}.first) {
            constraint.constant = 0.0
           UIView.animate(withDuration: 0.3, animations: {
                 self.view.layoutIfNeeded()
            })
            timePicker.isHidden = true
        }
    }
}

extension AddFilterViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hidePickerView()
        hideTimePickerView()
    }
}

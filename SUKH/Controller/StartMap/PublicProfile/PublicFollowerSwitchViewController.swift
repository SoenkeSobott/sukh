//
//  PublicFollowerSwitchViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 31.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class PublicFollowerSwitchViewController: UIViewController {

    // Outlets
    @IBOutlet weak var followerButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followerIndicator: UIImageView!
    @IBOutlet weak var followingIndicator: UIImageView!
    @IBOutlet weak var followerView: UIView!
    @IBOutlet weak var followingView: UIView!
    
    // Services
    
    // Variables
    var clickOnFollowers: Bool = true
    var username = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        if (clickOnFollowers) {
            showFollowers(self)
        } else {
            showFollowing(self)
        }
    }
    
    @IBAction func popViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showFollowers(_ sender: Any) {
        followingButton.tintColor = UIColor.lightGray
        followingIndicator.isHidden = true
        
        followerButton.tintColor = UIColor(named: "SukhTextColor")
        followerIndicator.isHidden = false
        
        followerView.alpha = 1
        followingView.alpha = 0
        let children = self.children
        let followingVC = children[0] as! PublicFollowingViewController
        followingVC.followingSearchBar.resignFirstResponder() // Hide keyboard
        followingVC.followingSearchBar.text = nil
        followingVC.isSearching = false
        followingVC.followingTableView.reloadData()
    }
    
    @IBAction func showFollowing(_ sender: Any) {
        followerButton.tintColor = UIColor.lightGray
        followerIndicator.isHidden = true
        
        followingButton.tintColor = UIColor(named: "SukhTextColor")
        followingIndicator.isHidden = false
        
        followerView.alpha = 0
        let children = self.children
        let followerVC = children[1] as! PublicFollowerViewController
        followerVC.followerSearchBar.resignFirstResponder() // Hide keyboard
        followerVC.followerSearchBar.text = nil
        followerVC.isSearching = false
        followerVC.followerTableView.reloadData()
        followingView.alpha = 1
    }

    

}

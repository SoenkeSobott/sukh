//
//  PublicFollowingViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 01.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class PublicFollowingViewController: UIViewController {

    // Outlets
    @IBOutlet weak var followingSearchBar: UISearchBar!
    @IBOutlet weak var followingTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let connectionService = ConnectionService()
    
    // Variables
    var activityIndicator = UIActivityIndicatorView()
    var noFollowingBanner = DecentStyleButton()
    var dataSource: [FollowerCell] = []
    var filteredDataSource: [FollowerCell] = []
    var isSearching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getTableViewActivityIndicator(view: followingTableView)

        followingSearchBar.delegate = self
        followingSearchBar.returnKeyType = UIReturnKeyType.done
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let parentVC = self.parent as! PublicFollowerSwitchViewController
                getFollowing(username: parentVC.username)

    }
    
    @objc func getFollowing(username: String) {
        self.setLoading(true)
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getFollowingOfUser(username: username) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    
                    let users = Decoder().decodeUsers(data: data)
                    
                    self.fillDataSource(withUsers: users)
                    self.sortTable()
                    self.setLoading(false)
                } else {
                    print("Failed loading following", statusCode!)
                    self.setLoading(false)
                }
            }
        }
    }
}

// Search Bar
extension PublicFollowingViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            view.endEditing(true)
            followingTableView.reloadData()
        } else {
            isSearching = true
            filteredDataSource = dataSource.filter( { $0.username.range(of: searchText, options: .caseInsensitive) != nil } )
            followingTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        followingSearchBar.resignFirstResponder()
    }
}

// table view
extension PublicFollowingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isSearching) {
            return filteredDataSource.count
        }
        
        return dataSource.count

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let followerCell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
            return UITableViewCell()
        }
        
        // reset image
        followerCell.profileImage.image = nil

        var follower = FollowerCell(username: "", imageURL: "", followerStatus: FollowerStatus.NotFollowing)
        if (isSearching) {
            follower = filteredDataSource[indexPath.row]
        } else {
            follower = dataSource[indexPath.row]
        }
        
        // Setup eventCell
        followerCell.name.text = follower.username
        followerCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        followerCell.profileImage.sd_setImage(with: URL(string: follower.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        if (follower.username == authService.getCurrentUser()) {
            followerCell.isUserInteractionEnabled = false
            followerCell.followerButton.setTitle("You", for: .normal)
        } else {
            followerCell.isUserInteractionEnabled = true
            followerCell.followerButton.isHidden = true
        }
        
        return followerCell
    }
        
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        followingSearchBar.resignFirstResponder()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = UIColor.gray
        }
        
        performSegue(withIdentifier: "profilSegue", sender: dataSource[indexPath.row].username)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profilSegue" {
            if let destination = segue.destination as? PublicProfileViewController,
                let selectedUsername = sender as? String {
                destination.username = selectedUsername
            }
        }
    }
}

// Helper functions
extension PublicFollowingViewController {
    
    func sortTable() {
        dataSource.sort(by: {$0.username < $1.username})
        followingTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            self.dataSource.append(FollowerCell(username: user.username!, imageURL: user.profileImageURL!, followerStatus: FollowerStatus.Following))
        }
        
        if (dataSource.count == 0) {
            followingTableView.isHidden = true
            setUpNoFollowingBanner(isHidden: false)
        } else {
            noFollowingBanner.removeFromSuperview()
            followingTableView.isHidden = false
        }
    }
    
    func setUpNoFollowingBanner(isHidden: Bool) {
        noFollowingBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: followingTableView.frame.origin.y + 40), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noFollowingBanner.setTitle("No one following", for: .normal)
        
        noFollowingBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noFollowingBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noFollowingBanner.layer.shadowOpacity = 0.5
        noFollowingBanner.clipsToBounds       = true
        noFollowingBanner.layer.masksToBounds = false
                                
        noFollowingBanner.isHidden = isHidden
        self.view.addSubview(noFollowingBanner)
    }

    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        followingTableView.isUserInteractionEnabled = !loading
    }
}



//
//  OpenProfileViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 22.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage
import Reachability
import CoreLocation

struct TimelineCell {
    let eventID: String
    let name: String
    let imageURL: String
    var city: CLLocation
    let time: String
    let startDate: Date
    let type: EventType
    let credit: (Int, CreditScore)
}

class PublicProfileViewController: UIViewController {

    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var customNavigationBar: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var timeLineTableView: UITableView!
    @IBOutlet weak var timelineTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var userCredit: UIImageView!
    @IBOutlet weak var userCreditLabel: UILabel!
    @IBOutlet weak var userCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var userCreditWidth: NSLayoutConstraint!
    @IBOutlet weak var totalCredit: UILabel!
    @IBOutlet weak var followButton: AddButton!
    @IBOutlet weak var followerIndicator: UIImageView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let spinner = Spinner()
    let classics = Classics()
    let basicAlerts = BasicAlerts()
    let locationService = LocationService()
    let authService = AuthService()
    
    // Variables
    var username = ""
    var activityIndicator = UIActivityIndicatorView()
    var tableViewActivityIndicator = UIActivityIndicatorView()
    var dataSource: [TimelineCell] = []
    var noTimelineBanner = DecentStyleButton()
    let pulsatingLayer = CAShapeLayer()
    var creditStartValue = 0.0
    var creditEndValue = 0.0
    let animationStartDate = Date()
    let creditAnimationDuration: TimeInterval = 1.0
    var alreadyFollow: Bool = false
    var userCreditValue = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        self.activityIndicator = self.spinner.getActivityIndicator(view: self.view)
        self.tableViewActivityIndicator = self.spinner.getTableViewActivityIndicator(view: timeLineTableView.self)
        setUpCustomNavigationBar()
        setUpProfileImage()
        setUpTableView()
        getUser(username: username)
        getUserTimeline(username: username)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        connectionService.setReachabilityNotifier(forViewController: self)
        
        // Do this to animate even when swithcing between views
        if (userCreditValue > 500 && userCreditValue < 1000) {
            animatePuls()
        } else if (userCreditValue > 1000) {
            animateFastPuls() // TODO: special aniamtion when LORD status
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        userCredit.layer.removeAllAnimations()
    }
    
    func setUpCustomNavigationBar() {
        UIUtils().setSukhNavigationShadow(forView: customNavigationBar.self)
        customNavigationBar.layer.zPosition = 100
        usernameLabel.text = username
    }
    
    func setUpProfileImage() {
        profileImage.layer.borderWidth = 1.5
        profileImage.layer.borderColor = UIColor.gray.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
    }
    
    func setUpTableView() {
        UIUtils().setSukhTimelineShadow(forView: timeLineTableView)
    }
    
    func getUser(username: String) {
        if (connectionService.getConnectionStatus() != .unavailable) {
            setLoading(true)
            mongoQueryService.getUser(username: username) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    let currentUser = Decoder().decodeUser(data: data)
                    
                    self.setLoading(false)
                    self.loadProfileImage(imageURL: currentUser.profileImageURL ?? "")
                    self.setCredit(credit: currentUser.monthlyCredit!, totalCredit: currentUser.totalCredit!)
                    self.setFollowers(followers: currentUser.followers!.count, following: currentUser.following!.count)
                    let following = currentUser.followers?.contains(self.authService.getCurrentUser())
                    self.setFollowButton(following: following!)
                } else {
                    NSLog("Error retrieving user: \(statusCode!)")
                    self.setLoading(false)
                }
            }
        }
    }
    
    func getUserTimeline(username: String) {
        if (connectionService.getConnectionStatus() != .unavailable) {
            self.setLoadingTableView(true)
            mongoQueryService.getTimelineOfUser(username: username) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    let events = Decoder().decodeEvents(eventData: data)
                    
                    self.fillDataSource(withEvents: events)
                    self.sortTable()
                    self.setLoadingTableView(false)
                } else {
                    self.setLoadingTableView(false)
                    NSLog("Failed retrieving timeline \(statusCode)")
                }
            }
        }
    }
    
    func setCredit(credit: Int, totalCredit: Int) {
        let userCredit = classics.calculateCredit(userCredit: credit)
        self.userCredit.tintColor = UIColor(named: userCredit.1.rawValue)
        self.totalCredit.text = "Total: \(totalCredit)"
        
        // Save credit to animate heart in viewWillAppear()
        self.userCreditValue = credit
        
        let creditForHeightCalculation = (userCredit.0 / 10)
        UIView.animate(withDuration: creditAnimationDuration) {
            self.userCreditHeight.constant = CGFloat(creditForHeightCalculation)
            self.userCreditWidth.constant = CGFloat(creditForHeightCalculation)
            self.view.layoutIfNeeded()
        }
        
        // Animate counting credit
        creditEndValue = Double(credit)
        let displayLink = CADisplayLink(target: self, selector: #selector(countCreditUp))
        displayLink.add(to: .main, forMode: .default)
                
        // Pulsing heart when over 500
        if (credit > 500 && credit <= 1000) {
            animatePuls()
        } else if (credit > 1000) {
            animateFastPuls() // TODO: special aniamtion when LORD status changes every month
        }
    }
    
    @objc func countCreditUp() {
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if (elapsedTime > creditAnimationDuration) {
            self.userCreditLabel.text = "Credit: \(Int(creditEndValue))"
        } else {
            let percentage = elapsedTime / creditAnimationDuration
            let value = (percentage) * (creditEndValue - creditStartValue)
            self.userCreditLabel.text = "Credit: \(Int(value))"
        }
    }
    
    func setFollowers(followers: Int, following: Int) {
        followersLabel.text = "\(followers)"
        followingLabel.text = "\(following)"
    }
    
    @IBAction func creditButton(_ sender: Any) {
        print("Credit button pressed")
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToFollowers(_ sender: Any) {
        goToFollowers(clickOnFollowers: true)
    }
    
    @IBAction func goToFollowing(_ sender: Any) {
        goToFollowers(clickOnFollowers: false)
    }
    
    func goToFollowers(clickOnFollowers: Bool) {
        performSegue(withIdentifier: "followerSegue", sender: (clickOnFollowers, username))
    }
    
    func setFollowButton(following: Bool) {
        if (following) {
            alreadyFollow = true
            followButton.setTitle("Following", for: .normal)
            followerIndicator.image = classics.setSystemImageOrDefault(systemName: "checkmark")
        } else {
            alreadyFollow = false
            followButton.setTitle("Follow", for: .normal)
            followerIndicator.image = classics.setSystemImageOrDefault(systemName: "plus")
        }
    }
    
    @IBAction func followButton(_ sender: Any) {
        if (alreadyFollow) {
            unfollow(username: username)
        } else {
            follow(username: username)
        }
    }
}

// add/remove follower
extension PublicProfileViewController {
    func follow(username: String) {
        let activityIndicator = Spinner().getImageUploadActivityIndicator(view: followButton.imageView!)
        activityIndicator.startAnimating()
        
        mongoQueryService.addFollower(user: username, requestingUser: authService.getCurrentUser()) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                print("Followed: \(username)", statusCode!)
                self.setFollowButton(following: true)
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            } else {
                NSLog("Error following user '\(username)' with status code: \(statusCode!)")
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }
    
    func unfollow(username: String) {
        let activityIndicator = Spinner().getImageUploadActivityIndicator(view: followButton.imageView!)
        activityIndicator.startAnimating()
        
        mongoQueryService.removeFollower(user: username, removeUser: authService.getCurrentUser()) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                NSLog("Successfully unfollowed user '\(username)' with status code: \(statusCode!)")
                self.setFollowButton(following: false)
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            } else {
                NSLog("Error unfollowing user '\(username)' with status code: \(statusCode!)")
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }
}

// table view
extension PublicProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "timelineCell", for: indexPath) as? TimelineTableViewCell else {
            return UITableViewCell()
        }
        
        let event = dataSource[indexPath.row]
        
        // Setup eventCell
        eventCell.eventID = event.eventID
        eventCell.eventsImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventCell.eventsImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
        eventCell.eventsName.text = event.name
        
        locationService.reverseGeocodeCity(location: event.city) { (city) in
            eventCell.city.text = city
        }
        eventCell.time.setTitle(event.time, for: .normal)
        
        var eventCreditSize = event.credit.0
        if (eventCreditSize > 50) {
            eventCreditSize = 50
        }
        eventCell.creditHeight.constant = CGFloat(eventCreditSize)
        eventCell.creditWidth.constant = CGFloat(eventCreditSize)
        eventCell.credit.tintColor = UIColor(named: event.credit.1.rawValue)
        
        switch event.type {
            case .Hangout:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Hangout")
            case .Party:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Party")
            case .Productive:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Productive")
        }
        
        return eventCell
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showEventDetail", sender: dataSource[indexPath.row].eventID)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// Repoort User
extension PublicProfileViewController {
    
    @IBAction func reportUser(_ sender: Any) {
        let alert = UIAlertController(title: "User", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Block User", style: .destructive, handler: { _ in
            self.blockUser(blockUsername: self.username)
        }))

        alert.addAction(UIAlertAction(title: "Report User", style: .destructive, handler: { _ in
            self.reportUser()
        }))
        
        alert.addAction(UIAlertAction(title: "Share User", style: .default, handler: { _ in
            self.shareUser()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    func blockUser(blockUsername: String) {
        self.mongoQueryService.blockUser(username: authService.getCurrentUser(), blockUsername: blockUsername) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                self.basicAlerts.setBlockAlert(forViewController: self)
            } else {
                NSLog("Error blocking user '\(blockUsername)' with status code: \(statusCode!)")
            }
        }
    }
    
    func reportUser() {
        setReportUserAlert(username: username)
    }
    
    func shareUser() {
        basicAlerts.setFeatureComingSoonAlert(forViewController: self)
    }
    
    func setReportUserAlert(username: String) {
        let reportUserAlert = UIAlertController(title: "Report User", message: "Do you want to report \(username) ?", preferredStyle: .alert)
            
        reportUserAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            // Report user
            self.mongoQueryService.reportUser(username: username) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    self.basicAlerts.setReportAlert(forViewController: self)
                } else {
                    NSLog("Error reporting user '\(username)' with status code: \(statusCode!)")
                }
            }
        }))
        reportUserAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(reportUserAlert, animated: true)
    }
}


// Reachability
extension PublicProfileViewController {
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            getUser(username: username)
        case .cellular:
            getUser(username: username)
        case .none:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not reachable")
        case .unavailable:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not available")
        }
    }
}


// Helper functions
extension PublicProfileViewController {
    
    func sortTable() {
        dataSource = dataSource.sorted(by: {$0.startDate.timeIntervalSince1970 > $1.startDate.timeIntervalSince1970})
        timeLineTableView.reloadData()
        
    }
    
    func fillDataSource(withEvents: [Event]) {
        for event in withEvents {
            let location = CLLocation(latitude: event.latitude!, longitude: event.longitude!)

            self.dataSource.append(TimelineCell(eventID: event.eventID!,
                                                name: event.name!,
                                                imageURL: event.eventImageURL!,
                                                city: location,
                                                time: self.classics.timeToNowString(ISO8601DateStringStart: event.startTime!, ISO8601DateStringEnd: event.endTime!),
                                                startDate: self.classics.dateFromISO8601Date(event.startTime!),
                                                type: EventType(rawValue: event.type!)!,
                                                credit: self.classics.calculateCreditOfEvent(username: username, feedEntries: event.eventFeedEntries!)))
        }
        if (dataSource.count > 0) {
            // Set height of table view and view
            let height: CGFloat = CGFloat(dataSource.count * 140)
            timelineTableViewHeight.constant = height
            let realOrigin = view.convert(timeLineTableView.frame.origin, to: self.view)
            let buffer: CGFloat = 60.0
            viewHeight.constant = height + realOrigin.y + buffer
            timeLineTableView.isHidden = false
            setUpNoTimelineBanner(isHidden: true)
        } else {
            timeLineTableView.isHidden = true
            setUpNoTimelineBanner(isHidden: false)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventDetail" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
        
        if segue.identifier == "followerSegue" {
            if let destination = segue.destination as? PublicFollowerSwitchViewController,
                let clickOnFollowers = sender as? (Bool, String) {
                destination.clickOnFollowers = clickOnFollowers.0
                destination.username = clickOnFollowers.1
            }
        }
    }
    
    func loadProfileImage(imageURL: String) {
        if (!imageURL.isEmpty) {
            profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            profileImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        } else {
            profileImage.image = classics.setSystemImageOrDefault(systemName: "person.circle")
        }
    }
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
    }
    
    func setLoadingTableView(_ loading: Bool) {
        if loading {
            tableViewActivityIndicator.startAnimating();
        } else {
            tableViewActivityIndicator.stopAnimating();
        }
        tableViewActivityIndicator.isUserInteractionEnabled = !loading
    }
    
    func setUpNoTimelineBanner(isHidden: Bool) {
        noTimelineBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: (self.timeLineTableView.frame.minY) + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noTimelineBanner.titleLabel?.textAlignment = .center
        noTimelineBanner.titleLabel?.lineBreakMode = .byWordWrapping
        noTimelineBanner.setTitle("No timeline events", for: .normal)
                  
        noTimelineBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noTimelineBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noTimelineBanner.layer.shadowOpacity = 0.5
        noTimelineBanner.clipsToBounds       = true
        noTimelineBanner.layer.masksToBounds = false
                                
        noTimelineBanner.isHidden = isHidden
        timeLineTableView.isHidden = !isHidden
        self.scrollView.addSubview(noTimelineBanner)
    }

    private func animatePuls() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.1
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        userCredit.layer.add(animation, forKey: "pulsing")
    }
    
    private func animateFastPuls() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.1
        animation.duration = 0.25
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        userCredit.layer.add(animation, forKey: "fastPulsing")
    }
}

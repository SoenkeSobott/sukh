//
//  PublicFollowerViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 31.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class PublicFollowerViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var followerSearchBar: UISearchBar!
    @IBOutlet weak var followerTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let connectionService = ConnectionService()
    
    // Variables
    var activityIndicator = UIActivityIndicatorView()
    var noFollowersBanner = DecentStyleButton()
    var dataSource: [FollowerCell] = []
    var filteredDataSource: [FollowerCell] = []
    var isSearching = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getTableViewActivityIndicator(view: followerTableView)
                
        followerSearchBar.delegate = self
        followerSearchBar.returnKeyType = UIReturnKeyType.done

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let parentVC = self.parent as! PublicFollowerSwitchViewController
        getFollowers(username: parentVC.username)

    }
    
    @objc func getFollowers(username: String) {
        setLoading(true)
        mongoQueryService.getFollowersOfUser(username: username) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!
                
                let users = Decoder().decodeUsers(data: data)
                
                self.fillDataSource(withUsers: users)
                self.sortTable()
                self.setLoading(false)
            } else {
                print("Failed loading followers", statusCode!)
                self.setLoading(false)
            }
        }
    }
}

// Search Bar
extension PublicFollowerViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            view.endEditing(true)
            followerTableView.reloadData()
        } else {
            isSearching = true
            filteredDataSource = dataSource.filter( { $0.username.range(of: searchText, options: .caseInsensitive) != nil } )
            followerTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        followerSearchBar.resignFirstResponder()
    }
}

// table view
extension PublicFollowerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isSearching) {
            return filteredDataSource.count
        }
        
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let followerCell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
            return UITableViewCell()
        }
        
        // reset image
        followerCell.profileImage.image = nil

        var follower = FollowerCell(username: "", imageURL: "", followerStatus: FollowerStatus.NotFollowing)
        if (isSearching) {
            follower = filteredDataSource[indexPath.row]
        } else {
            follower = dataSource[indexPath.row]
        }
        
        // Setup eventCell
        followerCell.name.text = follower.username
        followerCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        followerCell.profileImage.sd_setImage(with: URL(string: follower.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        if (follower.username == authService.getCurrentUser()) {
            followerCell.isUserInteractionEnabled = false
            followerCell.followerButton.setTitle("You", for: .normal)
        } else {
            followerCell.isUserInteractionEnabled = true
            followerCell.followerButton.isHidden = true
        }
        
        return followerCell
    }
        
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        followerSearchBar.resignFirstResponder()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = UIColor.gray
        }
        
        performSegue(withIdentifier: "profilSegue", sender: dataSource[indexPath.row].username)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profilSegue" {
            if let destination = segue.destination as? PublicProfileViewController,
                let selectedUsername = sender as? String {
                destination.username = selectedUsername
            }
        }
    }

    @objc func followUser(sender: UIButton) {
        let username = dataSource[sender.tag].username
        
        setLoading(true)
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.addFollower(user: username, requestingUser: authService.getCurrentUser()) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully followed user '\(username)' with status code: \(statusCode!)")
                    self.getFollowers(username: username)
                    self.setLoading(false)
                } else {
                    NSLog("Error following user '\(username)' with status code: \(statusCode!)")
                    self.getFollowers(username: username)
                    self.setLoading(false)
                }
            }
        }
    }
}


// Helper functions
extension PublicFollowerViewController {
    
    func sortTable() {
        dataSource.sort(by: {$0.username < $1.username})
        followerTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            self.dataSource.append(FollowerCell(username: user.username!, imageURL: user.profileImageURL!, followerStatus: (user.followers!.contains(authService.getCurrentUser()) ? FollowerStatus.Following : FollowerStatus.NotFollowing)))
        }
        
        if (dataSource.count == 0) {
            followerTableView.isHidden = true
            setUpNoFollowersBanner(isHidden: false)
        } else {
            noFollowersBanner.removeFromSuperview()
            followerTableView.isHidden = false
        }
    }
    
    func setUpNoFollowersBanner(isHidden: Bool) {
        noFollowersBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: followerTableView.frame.origin.y + 40), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noFollowersBanner.setTitle("No followers", for: .normal)
        
        noFollowersBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noFollowersBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noFollowersBanner.layer.shadowOpacity = 0.5
        noFollowersBanner.clipsToBounds       = true
        noFollowersBanner.layer.masksToBounds = false
                                
        noFollowersBanner.isHidden = isHidden
        self.view.addSubview(noFollowersBanner)
    }

    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        followerTableView.isUserInteractionEnabled = !loading
    }
}


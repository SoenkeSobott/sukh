//
//  FollowingViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 31.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class FollowingViewController: UIViewController {

    // Outlets
    @IBOutlet weak var followingSearchBar: UISearchBar!
    @IBOutlet weak var followingTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let connectionService = ConnectionService()
    
    // Variables
    var activityIndicator = UIActivityIndicatorView()
    var noFollowingBanner = DecentStyleButton()
    var dataSource: [FollowerCell] = []
    var filteredDataSource: [FollowerCell] = []
    var isSearching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        activityIndicator = Spinner().getTableViewActivityIndicator(view: followingTableView)

        NotificationCenter.default.addObserver(self, selector: #selector(getFollowing), name: NSNotification.Name.init("getFollowing"), object: nil)

        getFollowing()
        followingSearchBar.delegate = self
        followingSearchBar.returnKeyType = UIReturnKeyType.done
    }
    
    @objc func getFollowing() {
        self.setLoading(true)
        mongoQueryService.getFollowingOfUser(username: authService.getCurrentUser()) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!
                
                let users = Decoder().decodeUsers(data: data)
                
                self.fillDataSource(withUsers: users)
                self.sortTable()
                self.setLoading(false)
            } else {
                print("Failed loading following", statusCode!)
                self.setLoading(false)
            }
        }
    }
}

// Search Bar
extension FollowingViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        if (searchBar.text == nil || searchBar.text == "") {
            isSearching = false
            view.endEditing(true)
            followingTableView.reloadData()
        } else {
            isSearching = true
            filteredDataSource = dataSource.filter( { $0.username.range(of: searchText, options: .caseInsensitive) != nil } )
            followingTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        followingSearchBar.resignFirstResponder()
    }
}

// table view
extension FollowingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isSearching) {
            return filteredDataSource.count
        }
        return dataSource.count

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let friendCell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
            return UITableViewCell()
        }
        
        // reset image
        friendCell.profileImage.image = nil

        var follower = FollowerCell(username: "", imageURL: "", followerStatus: FollowerStatus.NotFollowing)
        if (isSearching) {
            follower = filteredDataSource[indexPath.row]
        } else {
            follower = dataSource[indexPath.row]
        }
        
        // Setup eventCell
        friendCell.name.text = follower.username
        friendCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        friendCell.profileImage.sd_setImage(with: URL(string: follower.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        friendCell.followerButton.setTitle("Unfollow", for: .normal)
        friendCell.followerButton.tag = indexPath.row
        friendCell.followerButton.addTarget(self, action: #selector(unfollow), for: UIControl.Event.touchUpInside)

        
        // Sets the clicked color of cell to lightgray
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray
        friendCell.selectedBackgroundView = backgroundView
        
        return friendCell
    }
        
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        followingSearchBar.resignFirstResponder()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = UIColor.gray
        }
        
        performSegue(withIdentifier: "profilSegue", sender: dataSource[indexPath.row].username)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profilSegue" {
            if let destination = segue.destination as? PublicProfileViewController,
                let selectedUsername = sender as? String {
                destination.username = selectedUsername
            }
        }
    }

    @objc func unfollow(sender: UIButton) {
        let username = dataSource[sender.tag].username
        
        setLoading(true)
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.removeFollower(user: username, removeUser: authService.getCurrentUser()) { (response) in
                    let statusCode = response.response?.statusCode
                    if (statusCode == 200) {
                        print("Unfollow \(username)", statusCode!)
                        sender.removeTarget(self, action: #selector(self.unfollow), for: UIControl.Event.touchUpInside)
                        NotificationCenter.default.post(name: NSNotification.Name.init("getFollowers"), object: nil)
                        self.getFollowing()
                        self.setLoading(false)
                    } else {
                        print("Failed unfollowing \(username) ", statusCode!)
                        sender.removeTarget(self, action: #selector(self.unfollow(sender:)), for: UIControl.Event.touchUpInside)
                        NotificationCenter.default.post(name: NSNotification.Name.init("getFollowers"), object: nil)
                        self.getFollowing()
                        self.setLoading(false)
                       }
                   }
        }
    }
}

// Helper functions
extension FollowingViewController {
    
    func sortTable() {
        dataSource.sort(by: {$0.username < $1.username})
        followingTableView.reloadData()
    }
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            self.dataSource.append(FollowerCell(username: user.username!, imageURL: user.profileImageURL!, followerStatus: FollowerStatus.Following))
        }
        
        if (dataSource.count == 0) {
            followingTableView.isHidden = true
            setUpNoFollowingBanner(isHidden: false)
        } else {
            noFollowingBanner.removeFromSuperview()
            followingTableView.isHidden = false
        }
    }
    
    func setUpNoFollowingBanner(isHidden: Bool) {
        noFollowingBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: followingTableView.frame.origin.y + 40), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noFollowingBanner.setTitle("Find people to follow", for: .normal)
        
        noFollowingBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noFollowingBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noFollowingBanner.layer.shadowOpacity = 0.5
        noFollowingBanner.clipsToBounds       = true
        noFollowingBanner.layer.masksToBounds = false
                                
        noFollowingBanner.isHidden = isHidden
        self.view.addSubview(noFollowingBanner)
    }

    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        followingTableView.isUserInteractionEnabled = !loading
    }
}


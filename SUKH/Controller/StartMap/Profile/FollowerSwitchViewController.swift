//
//  FollowerSwitchViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 31.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class FollowerSwitchViewController: UIViewController {

    // Outlets
    @IBOutlet weak var customNavigationBar: UIView!
    @IBOutlet weak var followerButton: UIButton!
    @IBOutlet weak var followerIndicator: UIImageView!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followingIndicator: UIImageView!
    @IBOutlet weak var followerView: UIView!
    @IBOutlet weak var followingView: UIView!
    
    // Services
    
    // Variables
    var clickOnFollowers: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpNavigationBar()
        if (clickOnFollowers) {
            showFollowers(self)
        } else {
            showFollowing(self)
        }
    }
    

    func setUpNavigationBar() {
        UIUtils().setSukhNavigationShadow(forView: customNavigationBar.self)
        customNavigationBar.layer.zPosition  = 1000
    }
    
    @IBAction func popViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showFollowers(_ sender: Any) {
        followingButton.tintColor = UIColor.lightGray
        followingIndicator.isHidden = true
        
        followerButton.tintColor = UIColor(named: "SukhTextColor")
        followerIndicator.isHidden = false
        
        followerView.alpha = 1
        followingView.alpha = 0
        let children = self.children
        let followingVC = children[1] as! FollowingViewController
        followingVC.followingSearchBar.resignFirstResponder() // Hide keyboard
        followingVC.followingSearchBar.text = nil
        followingVC.isSearching = false
        followingVC.followingTableView.reloadData()
    }
    
    @IBAction func showFollowing(_ sender: Any) {
        followerButton.tintColor = UIColor.lightGray
        followerIndicator.isHidden = true
        
        followingButton.tintColor = UIColor(named: "SukhTextColor")
        followingIndicator.isHidden = false
        
        followerView.alpha = 0
        let children = self.children
        let followerVC = children[0] as! FollowerViewController
        followerVC.followerSearchBar.resignFirstResponder() // Hide keyboard
        followerVC.followerSearchBar.text = nil
        followerVC.isSearching = false
        followerVC.followerTableView.reloadData()
        followingView.alpha = 1
    }
    
    
}

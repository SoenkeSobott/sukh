//
//  MyInvitationsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 04.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage

struct EventCell {
    let eventID: String
    let imageURL: String
    let name: String
    let distance: Double
}

class MyInvitationsViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var invitationsTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let locationService = LocationService()
    let classics = Classics()
    
    // Variables
    var dataSource: [EventCell] = []
    var noInvitationsBanner = DecentStyleButton()
    var activityIndicator = UIActivityIndicatorView()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
        setUpNavigationBar()
        setUpTableView()
        getEventInvitations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Invitations"
    }
    
    func setUpTableView() {
        invitationsTableView.dataSource = self
        invitationsTableView.delegate = self
    }
    
    func getEventInvitations(){
        self.dataSource = []

        mongoQueryService.getInvitationsOfUser(username: AuthService().getCurrentUser()) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                NSLog("Successfully loaded invitations of user with status code: ", statusCode!)
                let data: Data = response.data!

                let events = Decoder().decodeEvents(eventData: data)
                
                self.fillDataSource(withEvents: events)
                self.sortTable()
            } else {
                NSLog("Failed loading invitations of user with status code: ", statusCode!)
            }
        }
    }
    
    @objc func acceptEvent(sender: UIButton) {
        self.setLoading(true)
        let cell = invitationsTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! EventInvitationTableViewCell
        cell.rejectButton.isHidden = true
        
        let eventID = dataSource[sender.tag].eventID
        
        mongoQueryService.acceptOrRejectInvitation(username: AuthService().getCurrentUser(), eventID: eventID, accept: true) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                NSLog("Successfully accepted event \(eventID) with status code: ", statusCode!)
                sender.removeTarget(self, action: #selector(self.acceptEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.removeTarget(self, action: #selector(self.rejectEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.isUserInteractionEnabled = true
                self.getEventInvitations()
                self.setLoading(false)
            } else {
                NSLog("Failed accepting event \(eventID) with status code: ", statusCode!)
                sender.removeTarget(self, action: #selector(self.acceptEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.removeTarget(self, action: #selector(self.rejectEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.isUserInteractionEnabled = true
                self.getEventInvitations()
                self.setLoading(false)
            }
        }
    }
    
    @objc func rejectEvent(sender: UIButton) {
        self.setLoading(true)
        let cell = invitationsTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! EventInvitationTableViewCell
        cell.acceptButton.isHidden = true
        
        let eventID = dataSource[sender.tag].eventID
        
        mongoQueryService.acceptOrRejectInvitation(username: AuthService().getCurrentUser(), eventID: eventID, accept: false) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                NSLog("Successfully rejected event \(eventID) with status code: ", statusCode!)
                sender.removeTarget(self, action: #selector(self.acceptEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.removeTarget(self, action: #selector(self.rejectEvent(sender:)), for: UIControl.Event.touchUpInside)
                self.getEventInvitations()
                self.setLoading(false)
            } else {
                NSLog("Failed rejecting event \(eventID) with status code: ", statusCode!)
                sender.removeTarget(self, action: #selector(self.acceptEvent(sender:)), for: UIControl.Event.touchUpInside)
                sender.removeTarget(self, action: #selector(self.rejectEvent(sender:)), for: UIControl.Event.touchUpInside)
                self.getEventInvitations()
                self.setLoading(false)
            }
        }
    }
}

// Table view
extension MyInvitationsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "EventInvitationTableViewCell", for: indexPath) as? EventInvitationTableViewCell else {
            return UITableViewCell()
        }
        
        // reset image
        eventCell.eventImage.image = nil
        
        let event = dataSource[indexPath.row]
        
        // Setup eventCell
        eventCell.name.text = event.name
        eventCell.distance.text = classics.transformDistanceToStringDistance(distance: event.distance)
        eventCell.eventImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventCell.eventImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
        eventCell.acceptButton.tag = indexPath.row
        eventCell.acceptButton.isHidden = false
        eventCell.acceptButton.addTarget(self, action: #selector(acceptEvent), for: UIControl.Event.touchUpInside)
        eventCell.rejectButton.tag = indexPath.row
        eventCell.rejectButton.addTarget(self, action: #selector(rejectEvent), for: UIControl.Event.touchUpInside)
        eventCell.rejectButton.isHidden = false
        
        return eventCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "eventDetailsSeque", sender: dataSource[indexPath.row].eventID)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Helper functions
extension MyInvitationsViewController {
    
    func fillDataSource(withEvents: [Event]) {
        dataSource = []
        for event in withEvents {
            self.dataSource.append(EventCell(eventID: event.eventID!, imageURL: event.eventImageURL!, name: event.name!, distance: classics.calculateDistanceInMeters(latitude: event.latitude!, longitude: event.longitude!)))
        }
        
        // Set no invitations banner if necessary
        if (dataSource.count == 0) {
            setUpNoInvitationsBanner(isHidden: false)
        } else {
            setUpNoInvitationsBanner(isHidden: true)
        }
    }
    
    func sortTable() {
        _ = dataSource.sorted() { $0.distance > $1.distance }
        invitationsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailsSeque" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    func setUpNoInvitationsBanner(isHidden: Bool) {
        noInvitationsBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: (navigationController?.navigationBar.frame.maxY)! + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noInvitationsBanner.setTitle("No invitations", for: .normal)
        
        noInvitationsBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noInvitationsBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noInvitationsBanner.layer.shadowOpacity = 0.5
        noInvitationsBanner.clipsToBounds       = true
        noInvitationsBanner.layer.masksToBounds = false
                                
        noInvitationsBanner.isHidden = isHidden
        invitationsTableView.isHidden = !isHidden
        self.view.addSubview(noInvitationsBanner)
    }
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
    }
}

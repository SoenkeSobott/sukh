//
//  ProfileViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.11.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import Reachability

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var timelineTableView: UITableView!
    @IBOutlet weak var timelineTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var isVerifiedImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var userCredit: UIImageView!
    @IBOutlet weak var userCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var userCreditWidth: NSLayoutConstraint!
    @IBOutlet weak var userCreditLabel: UILabel!
    @IBOutlet weak var totalCredit: UILabel!
    @IBOutlet weak var gettingSukhSinceLabel: UILabel!
    @IBOutlet weak var invitedEventsNotificationIndicator: UIImageView!
    
    // Services
    let authService = AuthService()
    let mongoQueryService = MongoQueryService()
    let classics = Classics()
    let spinner = Spinner()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let locationService = LocationService()
    
    // Variables
    var currentUser = User(username: nil, name: nil, mail: nil, password: nil, createdAt: nil, role: nil, profileImage: nil, profileImageURL: nil, followers: nil, following: nil)
    var activityIndicator = UIActivityIndicatorView()
    var tableViewActivityIndicator = UIActivityIndicatorView()
    var profileImagePath: String = ""
    var noTimelineBanner = DecentStyleButton()
    var dataSource: [TimelineCell] = []
    let pulsatingLayer = CAShapeLayer()
    var creditStartValue = 0.0
    var creditEndValue = 0.0
    let animationStartDate = Date()
    let creditAnimationDuration: TimeInterval = 1.0
    var userCreditValue = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        self.setUpProfileImage()
        self.activityIndicator = self.spinner.getImageUploadActivityIndicator(view: userCredit.self)
        self.tableViewActivityIndicator = self.spinner.getTableViewActivityIndicator(view: timelineTableView.self)

        // Get currentUser
        // Info: getUser() is called in reachability at the bottom. Reason: else double call of getUser()
        self.getNotifications()
        self.setUpNavigationBar()
        self.setUpLabels()
        self.setUpTableView()
        self.setUpVerifiedImage()
        self.setUpGettingSukhSinceLabel()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Do this to animate even when swithcing between views
        if (userCreditValue > 500 && userCreditValue < 1000) {
            animatePuls()
        } else if (userCreditValue > 1000) {
            animateFastPuls() // TODO: special aniamtion when LORD status
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        userCredit.layer.removeAllAnimations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //getNotifications()
        connectionService.setReachabilityNotifier(forViewController: self)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setUpTableView() {
        UIUtils().setSukhTimelineShadow(forView: timelineTableView)
    }
    
    func getUser() {
        if (connectionService.getConnectionStatus() != .unavailable) {
            setLoading(true)
            mongoQueryService.getUser(username: authService.getCurrentUser()) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully requested user with status code: \(statusCode)")
                    let data: Data = response.data!
                    self.currentUser = Decoder().decodeUser(data: data)
        
                    self.setLoading(false)
                    self.loadProfileImage()
                    self.setCredit(credit: self.currentUser.monthlyCredit!, totalCredit: self.currentUser.totalCredit!)
                    self.setFollowers(followers: self.currentUser.followers!.count, following: self.currentUser.following!.count)
                    self.getUserTimeline(username: self.currentUser.username!)
                } else {
                    self.setLoading(false)
                    NSLog("Failed requesting user with status code: \(statusCode)")
                }
            }
        }
    }
    
    func getUserTimeline(username: String) {
        if (connectionService.getConnectionStatus() != .unavailable) {
            self.setLoadingTableView(true)
            mongoQueryService.getTimelineOfUser(username: username) { (response) in
                let statusCode = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully requested user timeline with status code: \(statusCode)")
                    let data: Data = response.data!
                    let events = Decoder().decodeEvents(eventData: data)
                    
                    self.fillDataSource(withEvents: events)
                    self.sortTable()
                    self.setLoadingTableView(false)
                } else {
                    self.setLoadingTableView(false)
                    NSLog("Failed requesting user timeline with status code: \(statusCode)")
                }
            }
        }
    }
    
    func setCredit(credit: Int, totalCredit: Int) {
        let userCredit = classics.calculateCredit(userCredit: credit)
        self.userCredit.tintColor = UIColor(named: userCredit.1.rawValue)
        self.totalCredit.text = "Total: \(totalCredit)"

        // Save credit to animate heart in viewDidAppear()
        self.userCreditValue = credit
        let creditForHeightCalculation = (userCredit.0 / 10)
        UIView.animate(withDuration: creditAnimationDuration) {
            self.userCreditHeight.constant = CGFloat(creditForHeightCalculation)
            self.userCreditWidth.constant = CGFloat(creditForHeightCalculation)
            self.view.layoutIfNeeded()
        }
        
        // Animate counting credit
        creditEndValue = Double(credit)
        let displayLink = CADisplayLink(target: self, selector: #selector(countCreditUp))
        displayLink.add(to: .main, forMode: .default)
                
        // Pulsing heart when over 500
        if (credit > 500 && credit < 1000) {
            animatePuls()
        } else if (credit > 1000) {
            animateFastPuls() // TODO: special aniamtion when LORD status
        }
    }
    
    @objc func countCreditUp() {
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if (elapsedTime > creditAnimationDuration) {
            self.userCreditLabel.text = "Credit: \(Int(creditEndValue))"
        } else {
            let percentage = elapsedTime / creditAnimationDuration
            let value = (percentage) * (creditEndValue - creditStartValue)
            self.userCreditLabel.text = "Credit: \(Int(value))"
        }
    }
    
    func setFollowers(followers: Int, following: Int) {
        followersLabel.text = "\(followers)"
        followingLabel.text = "\(following)"
    }
    
    func setUpLabels() {
        usernameLabel.text = UserDefaults.standard.string(forKey: "username")
    }
    
    func setUpGettingSukhSinceLabel() {
        gettingSukhSinceLabel.text = "Getting SUKH since 2020"
    }
    
    func setUpNavigationBar() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        UIUtils().setSukhStandardNavigationShadow(forNavigationController: navigationController!)
    }

    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func goToFollowersButton(_ sender: Any) {
        goToFollowers(clickOnFollowers: true)
    }
    
    @IBAction func goToFollowing(_ sender: Any) {
        goToFollowers(clickOnFollowers: false)
    }
    
    func goToFollowers(clickOnFollowers: Bool) {
        performSegue(withIdentifier: "followerSegue", sender: clickOnFollowers)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "followerSegue" {
            if let destination = segue.destination as? FollowerSwitchViewController,
                let clickOnFollowers = sender as? Bool {
                destination.clickOnFollowers = clickOnFollowers
            }
        }
        
        if segue.identifier == "eventDetail" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    @IBAction func goToInvitedEvents(_ sender: Any) {
        if (connectionService.getConnectionStatus() != .unavailable) {
            navigationController?.setNavigationBarHidden(false, animated: false)
            
            let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyInvitationsViewController") as? MyInvitationsViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            basicAlerts.setConnectionAlert(forViewController: self)
        }
    }
    
    func setUpProfileImage() {
        profileImage.layer.borderWidth = 1.5
        profileImage.layer.borderColor = UIColor.gray.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
    }
    
    func loadProfileImage() {
        let imageURL = currentUser.profileImageURL
        
        if (imageURL != nil) {
            profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            profileImage.sd_setImage(with: URL(string: imageURL!), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
            
        } else {
            profileImage.image = classics.setSystemImageOrDefault(systemName: "person.circle")
        }
    }

    @IBAction func uploadProfileImage(_ sender: Any) {
        setLoading(true)
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        
        self.present(image, animated: true) {
            self.setLoading(false)
            // After it is complete
        }
        
    }
    
    func setUpVerifiedImage() {
        let role = authService.getRoleOfCurrentUser()
        
        switch role {
        case .UnverifiedUser:
            NSLog("Role: unverified")
        case .VerifiedUser:
            NSLog("Role: verified")
            isVerifiedImage.image = classics.setSystemImageOrDefault(systemName: "checkmark.circle")
            isVerifiedImage.tintColor = UIColor.green
        default:
            NSLog("Role: No Role")
            // TODO: kick user
        }
    }
    
}

// profile image
extension ProfileViewController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            setLoading(true)
            
            let imageURL = ImageService().uploadImage(image: image)
            
            let profileImageData: Data = image.jpegData(compressionQuality: 1)! as Data
                        
            currentUser.profileImage = profileImageData
            
            mongoQueryService.updateUser(user: currentUser) { (statusCode) in
                if (statusCode == 200) {
                    NSLog("Successfully updated user with status code: \(statusCode)")
                    self.profileImage.sd_setImage(with: URL(string: self.currentUser.profileImageURL!), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
                    self.profileImage.image = image
                    self.setLoading(false)
                } else {
                    self.setLoading(false)
                    NSLog("Failed updating user with status code: \(statusCode)")
                }
            }
        } else {
            NSLog("Error uploading media")
            self.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

// table view
extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "timelineCell", for: indexPath) as? TimelineTableViewCell else {
            return UITableViewCell()
        }
        
        let event = dataSource[indexPath.row]
        
        // Setup eventCell
        eventCell.eventID = event.eventID
        eventCell.eventsImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventCell.eventsImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
        eventCell.eventsName.text = event.name
        
        locationService.reverseGeocodeCity(location: event.city) { (city) in
            eventCell.city.text = city
        }
        eventCell.time.setTitle(event.time, for: .normal)
        
        var eventCreditSize = event.credit.0
        if (eventCreditSize > 50) {
            eventCreditSize = 50
        }
        eventCell.creditHeight.constant = CGFloat(eventCreditSize)
        eventCell.creditWidth.constant = CGFloat(eventCreditSize)
        eventCell.credit.tintColor = UIColor(named: event.credit.1.rawValue)
        
        switch event.type {
            case .Hangout:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Hangout")
            case .Party:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Party")
            case .Productive:
                eventCell.timelineDot.backgroundColor = UIColor(named: "Productive")
        }
        
        return eventCell
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "eventDetail", sender: dataSource[indexPath.row].eventID)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}

// Reachability
extension ProfileViewController {
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            getUser()
        case .cellular:
            getUser()
        case .none:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not reachable")
        case .unavailable:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not available")
        }
    }
}

// Helper functions
extension ProfileViewController {
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
        backButton.isUserInteractionEnabled = true
    }
    
    func setLoadingTableView(_ loading: Bool) {
        if loading {
            tableViewActivityIndicator.startAnimating();
        } else {
            tableViewActivityIndicator.stopAnimating();
        }
        tableViewActivityIndicator.isUserInteractionEnabled = !loading
    }
    
    func fillDataSource(withEvents: [Event]) {
        for event in withEvents {
            let location = CLLocation(latitude: event.latitude!, longitude: event.longitude!)

            self.dataSource.append(TimelineCell(eventID: event.eventID!,
                                                name: event.name!,
                                                imageURL: event.eventImageURL!,
                                                city: location,
                                                time: self.classics.timeToNowString(ISO8601DateStringStart: event.startTime!, ISO8601DateStringEnd: event.endTime!),
                                                startDate: self.classics.dateFromISO8601Date(event.startTime!),
                                                type: EventType(rawValue: event.type!)!,
                                                credit: self.classics.calculateCreditOfEvent(username: authService.getCurrentUser(), feedEntries: event.eventFeedEntries!)))
        }
        if (dataSource.count > 0) {
            // Set height of table view and view
            let height: CGFloat = CGFloat(dataSource.count * 140)
            timelineTableViewHeight.constant = height
            let realOrigin = view.convert(timelineTableView.frame.origin, to: self.view)
            let buffer: CGFloat = 60.0
            viewHeight.constant = height + realOrigin.y + buffer
            timelineTableView.isHidden = false
            setUpNoTimelineBanner(isHidden: true)
        } else {
            timelineTableView.isHidden = true
            setUpNoTimelineBanner(isHidden: false)
        }
    }
    
    func sortTable() {
        dataSource = dataSource.sorted(by: {$0.startDate.timeIntervalSince1970 > $1.startDate.timeIntervalSince1970})
        timelineTableView.reloadData()
    }
    
    func setUpNoTimelineBanner(isHidden: Bool) {
        noTimelineBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: (self.timelineTableView.frame.minY) + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noTimelineBanner.titleLabel?.textAlignment = .center
        noTimelineBanner.titleLabel?.lineBreakMode = .byWordWrapping
        noTimelineBanner.setTitle("No timeline events", for: .normal)
                  
        noTimelineBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noTimelineBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noTimelineBanner.layer.shadowOpacity = 0.5
        noTimelineBanner.clipsToBounds       = true
        noTimelineBanner.layer.masksToBounds = false
                                
        noTimelineBanner.isHidden = isHidden
        timelineTableView.isHidden = !isHidden
        self.scrollView.addSubview(noTimelineBanner)
    }
    
    private func animatePuls() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.1
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        userCredit.layer.add(animation, forKey: "pulsing")
    }
    
    private func animateFastPuls() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.1
        animation.duration = 0.25
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        userCredit.layer.add(animation, forKey: "fastPulsing")
    }
    
    func getNotifications() {
        let username = authService.getCurrentUser()
        
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getNotificationsOfUser(username: username) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    let notificationsOfUser = Decoder().decodeSukhNotification(data: data)
                    
                    let eventInvitations = notificationsOfUser.eventInvitations!
                    //let friendRequests = notificationsOfUser.friendRequests!
                    if (eventInvitations > 0) {
                        self.invitedEventsNotificationIndicator.tintColor = UIColor.red
                    } else {
                        self.invitedEventsNotificationIndicator.tintColor = UIColor.clear

                    }
                } else {
                    NSLog("Error retrieving sukhNotifications: \(statusCode!)")
                }
            }
        }
    }
    
}

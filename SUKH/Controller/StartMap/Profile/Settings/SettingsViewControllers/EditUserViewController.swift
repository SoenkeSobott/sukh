//
//  EditUserViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class EditUserViewController: UIViewController {

    // Outlets
    @IBOutlet weak var deleteUserButton: CustomButton!
    
    //Service
    let mongoQueryService = MongoQueryService()
    let spinner = Spinner()
    let authService = AuthService()
    
    //Variables
    var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        activityIndicator = spinner.getActivityIndicator(view: view.self)
    }
    

    
    @IBAction func deleteUserButton(_ sender: Any) {
        setDeleteUserAlert()
    }
    
    func setDeleteUserAlert() {
        let userNotExistingAlert = UIAlertController(title: "Delete Account", message: "Do you really want to delete this Account? All data will be lost", preferredStyle: .alert)
            
        userNotExistingAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        userNotExistingAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            // Delete
            self.setLoading(true)
            self.mongoQueryService.deleteUser(username: self.authService.getCurrentUser()) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    // Delete all data from device
                    self.authService.removeUserAndPasswordAndRole()
                    self.authService.resetUserDefaults()
                    self.authService.resetSDWebImageCache()
                    self.setLoading(false)
                    self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
                } else {
                    let alert = UIAlertController(title: "Not able to delete user", message: "No entry found", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    self.setLoading(false)
                }
            }
        }))
        
        self.present(userNotExistingAlert, animated: true)
    }
    
}

// Helper function
extension EditUserViewController {
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
    }
    
}

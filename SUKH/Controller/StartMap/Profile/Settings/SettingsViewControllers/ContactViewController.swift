//
//  ContactViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 28.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {
    
    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var messageSubject: InputTextField!
    @IBOutlet weak var messageSubjectPickerView: UIPickerView!
    @IBOutlet weak var messageSubjectPickerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionBody: UITextView!
    
    // Services
    let basicAlerts = BasicAlerts()
    
    // Variables
    let subjects = ["Report", "Bug", "Improvement"]
    var messageSubjectPickerViewVisible = false
    let hidePickersDuration = 0.3
    var messageSubjectPickerSelectedRow = 1

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        descriptionBody.delegate = self
        scrollView.delegate = self
        scrollView.keyboardDismissMode = .onDrag
        setUpNavigationBar()
        setUpEventTypePicker()
        setUpDescription()
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Contact"
    }

    func setUpEventTypePicker() {
        // TextField
        let tap = UITapGestureRecognizer(target: self,action: #selector(handleTaponTextField(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        messageSubject.isUserInteractionEnabled = true
        messageSubject.addGestureRecognizer(tap)
        messageSubject.placeholder = "Subject"
        
        // Picker
        messageSubjectPickerView.isHidden = true
        messageSubjectPickerView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setUpDescription() {
        UIUtils().setSukhShadow(forView: descriptionBody)
        
    }
     
    @IBAction func sendEmail(_ sender: Any) {
        let subject = subjects[messageSubjectPickerSelectedRow]
        let messageBody = descriptionBody.text!
        
        let toRecipients = ["sukhforhelp@gmail.com"]
        
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(subject)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipients)
        
        self.present(mc, animated: true, completion: nil)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
            case MFMailComposeResult.cancelled.rawValue:
                NSLog("Mail cancelled")
            case MFMailComposeResult.failed.rawValue:
                NSLog("Mail failed")
            case MFMailComposeResult.sent.rawValue:
                NSLog("Mail sent")
                descriptionBody.text = ""
                basicAlerts.setFeedbackBanner(forViewController: self)
            default:
                break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

// Subject picker
extension ContactViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjects[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjects.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        messageSubjectPickerSelectedRow = row
        messageSubject.text = subjects[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    @objc func handleTaponTextField(_ sender: UITapGestureRecognizer) {
         if (messageSubjectPickerViewVisible) {
             hidePickerView()
         } else {
             showPickerView()
         }
     }
    
    func showPickerView() {
        messageSubjectPickerViewVisible = true
        messageSubjectPickerView.selectRow(messageSubjectPickerSelectedRow, inComponent: 0, animated: false)
        messageSubject.text = subjects[messageSubjectPickerSelectedRow]
      
                
        if let constraint = (messageSubjectPickerView.constraints.filter{$0.identifier == "messageSubjectTypePickerHeight"}.first) {
            constraint.constant = 90.0
            messageSubjectPickerView.layoutIfNeeded()
            messageSubjectPickerView.isHidden = false
        }
        
    }
    
    func hidePickerView() {
        messageSubjectPickerViewVisible = false
        
        if let constraint = (messageSubjectPickerView.constraints.filter{$0.identifier == "messageSubjectTypePickerHeight"}.first) {
            constraint.constant = 0.0
            UIView.animate(withDuration: hidePickersDuration, animations: {
                 self.view.layoutIfNeeded()
            })
            messageSubjectPickerView.isHidden = true
        }
        
    }
}

extension ContactViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard text.rangeOfCharacter(from: CharacterSet.newlines) == nil else {
            textView.resignFirstResponder()
            return textView.text.count + (text.count - range.length) <= 250
        }
        
        return textView.text.count + (text.count - range.length) <= 250
    }
}

extension ContactViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hidePickerView()
    }
}

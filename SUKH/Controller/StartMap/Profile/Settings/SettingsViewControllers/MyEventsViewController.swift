//
//  MyEventsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage

struct MyEventCell {
    let name: String
    let imageURL: String
    let distance: Double
    let eventID: String
}

class MyEventsViewController: UIViewController {

    // Outlets
    @IBOutlet weak var myEventsTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let classics = Classics()
    let spinner = Spinner()
    
    // Variables
    var dataSource: [MyEventCell] = []
    var username = String()
    var noEventsBanner = DecentStyleButton()
    var activityIndicator = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        activityIndicator = spinner.getActivityIndicator(view: view.self)
        setUpMyEventsTableView()
        setUpNavigationBar()
        username = authService.getCurrentUser()
        refreshMyEventsTableView(self)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "My Events"
        navigationController?.navigationBar.backgroundColor = UIColor.white
    }
    
    func setUpMyEventsTableView() {
        myEventsTableView.dataSource = self
        myEventsTableView.delegate = self
        myEventsTableView.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func refreshMyEventsTableView(_ sender: Any){
        self.dataSource = []

        mongoQueryService.getAllEventsOfUser(username: username) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!

                let events = Decoder().decodeEvents(eventData: data)
                
                self.fillDataSource(withEvents: events)
                self.sortTable()
            } else {
                print("Error loading events: ", statusCode!)
            }
        }
    }
}


// my events table view
extension MyEventsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "myEventCell", for: indexPath) as? EventsTableViewCell else {
            return UITableViewCell()
        }
        
        let event = dataSource[indexPath.row]
        
        // Setup eventCell
        eventCell.eventsName.text = event.name
        eventCell.eventsImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        eventCell.eventsImage.sd_setImage(with: URL(string: event.imageURL), completed: nil)
        eventCell.eventsDistance.text = classics.transformDistanceToStringDistance(distance: event.distance)
        eventCell.eventID = event.eventID
        // Sets the clicked color of cell to lightgray
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray
        eventCell.selectedBackgroundView = backgroundView
        
        return eventCell
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "eventDetailsSeque", sender: dataSource[indexPath.row].eventID)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Helper functions
extension MyEventsViewController {
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        myEventsTableView.isUserInteractionEnabled = !loading
    }
    
    func sortTable() {
        _ = dataSource.sorted() { $0.distance > $1.distance }
        myEventsTableView.reloadData()
    }
    
    func fillDataSource(withEvents: [Event]) {
        for event in withEvents {
            self.dataSource.append(MyEventCell(name: event.name!, imageURL: event.eventImageURL!, distance: classics.calculateDistanceInMeters(latitude: event.latitude!, longitude: event.longitude!), eventID: event.eventID!))
        }
        if (dataSource.count > 0) {
            setUpNoEventsBanner(isHidden: true)
        } else {
            setUpNoEventsBanner(isHidden: false)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailsSeque" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    
    func setUpNoEventsBanner(isHidden: Bool) {
        noEventsBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: (navigationController?.navigationBar.frame.maxY)! + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noEventsBanner.titleLabel?.textAlignment = .center
        noEventsBanner.titleLabel?.lineBreakMode = .byWordWrapping
        noEventsBanner.setTitle("No events\nPress the + Icon on the Map\n to create new Events", for: .normal)
                  
        noEventsBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noEventsBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noEventsBanner.layer.shadowOpacity = 0.5
        noEventsBanner.clipsToBounds       = true
        noEventsBanner.layer.masksToBounds = false
                                
        noEventsBanner.isHidden = isHidden
        myEventsTableView.isHidden = !isHidden
        self.view.addSubview(noEventsBanner)
    }
    
}


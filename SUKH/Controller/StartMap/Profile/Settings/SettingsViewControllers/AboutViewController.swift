//
//  AboutViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 22.11.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var AboutLabel: UILabel!
    @IBOutlet weak var sukhLabel: UILabel!
    @IBOutlet weak var sukhDefinition: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        // Text and Logo
        let boldText = "SUKH"
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)]
        let attributedString = NSMutableAttributedString(string:boldText, attributes:attrs)

        let normalText = " (सुख) noun."
        let normalString = NSMutableAttributedString(string:normalText)

        attributedString.append(normalString)
        
        sukhLabel.attributedText = attributedString
        
        sukhDefinition.text = "1. Ease, 2. tranquility, 3. easy circumstances, 4. content\n - or simply happines "
        sukhDefinition.font = UIFont(name: "Baskerville", size: 12)
        
        
        // Setup UI
        setUpNavigationBar()
        setUpAboutLabel()
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "About"
    }
    
    func setUpAboutLabel() {
        self.AboutLabel.text = "SUKH ,1.0.0 (1) \n\nSUKH 2019 Sönke Sobott.\n All rights reserved."
    }
    
}

//
//  LegalNoticeViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 19.11.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class LegalNoticeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpNavigationBar()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar() {
           self.navigationItem.title = "Legal Notice"
    }
    
    // Privacy and Terms
    @IBAction func privacy(_ sender: Any) {
        if let url = URL(string: "https://lively-ground-0e73a6503.azurestaticapps.net/privacy") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func terms(_ sender: Any) {
        if let url = URL(string: "https://lively-ground-0e73a6503.azurestaticapps.net/terms") {
            UIApplication.shared.open(url)
        }
    }
    
    // Used Libraries
    @IBAction func Alamofire(_ sender: Any) {
        if let url = URL(string: "https://github.com/Alamofire/Alamofire") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func NotificationBanner(_ sender: Any) {
        if let url = URL(string: "https://github.com/Daltron/NotificationBanner") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func cocoapodkeys(_ sender: Any) {
        if let url = URL(string: "https://github.com/orta/cocoapods-keys") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func sdWebImage(_ sender: Any) {
        if let url = URL(string: "https://github.com/SDWebImage/SDWebImage") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func reachabilitySwift(_ sender: Any) {
        if let url = URL(string: "https://github.com/ashleymills/Reachability.swift") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func swiftKeychainWrapper(_ sender: Any) {
        if let url = URL(string: "https://github.com/jrendel/SwiftKeychainWrapper") {
            UIApplication.shared.open(url)
        }
    }
    
   
   // Code inspirations
    @IBAction func spaceBeforeTextField(_ sender: Any) {
        if let url = URL(string: "https://stackoverflow.com/questions/25367502/create-space-at-the-beginning-of-a-uitextfield") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func popPageViewController(_ sender: Any) {
        if let url = URL(string: "https://stackoverflow.com/questions/48147754/how-to-pop-uipageviewcontroller-when-swipe-back-from-first-page-or-0-index") {
            UIApplication.shared.open(url)
        }
    }
    
    // Image sources
    @IBAction func samiLay(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/sly_flm_pht/?hl=de") {
            UIApplication.shared.open(url)
        }
    }
        
    @IBAction func dannyHowe(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/@dannyhowe") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func mattyAdame(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/@omgitsmattyvee") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func pimMyten(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/@pimmyten") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func admaWhitlock(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/@adam_whitlock") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func alasdairElmes(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/@alelmes") {
            UIApplication.shared.open(url)
        }
    }
    
    
}

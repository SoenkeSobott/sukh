//
//  BlockedUsersViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 19.12.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

struct BlockedUserCell {
    let username: String
}

class BlockedUsersViewController: UIViewController {

    // Outlets
    @IBOutlet weak var blockedUsersTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let classics = Classics()
    let spinner = Spinner()
    let decoder = Decoder()
    
    // Variables
    var dataSource: [BlockedUserCell] = []
    var noBlockedUsersBanner = DecentStyleButton()
    var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        setUpNavigationBar()
        activityIndicator = spinner.getActivityIndicator(view: view.self)
        setUpBlockedUsersTableView()
        setUpNavigationBar()
        refreshBlockedUsersTableView(self)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Blocked Users"
        navigationController?.navigationBar.backgroundColor = UIColor.white
    }
    
    func setUpBlockedUsersTableView() {
        blockedUsersTableView.dataSource = self
        blockedUsersTableView.delegate = self
        blockedUsersTableView.backgroundColor = UIColor(named: "SukhBackgroundColor")
    }
    
    func refreshBlockedUsersTableView(_ sender: Any){
        self.dataSource = []
        let username = authService.getCurrentUser()
        mongoQueryService.getBlockedUsers(username: username) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                let data: Data = response.data!
                let blockedUsers = self.decoder.decodeBlockedUsers(data: data)
                self.fillDataSource(withBlockedUsers: blockedUsers)
                self.sortTable()
            } else {
                NSLog("Error loading blocked users for '\(username)' - status code: \(String(describing: statusCode))")
            }
        }
    }
}

// Blocked users table view
extension BlockedUsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let blockedUserCell = tableView.dequeueReusableCell(withIdentifier: "BlockedUserTableViewCell", for: indexPath) as? BlockedUserTableViewCell else {
            return UITableViewCell()
        }
        
        let blockedUser = dataSource[indexPath.row]
        
        // Setup eventCell
        blockedUserCell.username.text = blockedUser.username
        
        // Sets the clicked color of cell to lightgray
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray
        blockedUserCell.selectedBackgroundView = backgroundView
        
        return blockedUserCell
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let username = dataSource[indexPath.row].username
        let unblockUserAlert = UIAlertController(title: "Unblock \(username)", message: "", preferredStyle: .alert)
            
        unblockUserAlert.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: { (alert: UIAlertAction!) in
            self.unblockUser(unblockUsername: username)
        }))
        
        unblockUserAlert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))

        self.present(unblockUserAlert, animated: true)
        
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Helper functions
extension BlockedUsersViewController {
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        blockedUsersTableView.isUserInteractionEnabled = !loading
    }
    
    func sortTable() {
        _ = dataSource.sorted() { $0.username > $1.username }
        blockedUsersTableView.reloadData()
    }
    
    func fillDataSource(withBlockedUsers: [String]) {
        for blockedUser in withBlockedUsers {
            self.dataSource.append(BlockedUserCell(username: blockedUser))
        }
        
        if (dataSource.count > 0) {
            setUpNoBlockedUsersBanner(isHidden: true)
        } else {
            setUpNoBlockedUsersBanner(isHidden: false)
        }
    }
    
    func unblockUser(unblockUsername: String) {
        mongoQueryService.unblockUser(username: authService.getCurrentUser(), unblockUsername: unblockUsername) { (response) in
            let statusCode = response.response?.statusCode
            if (statusCode == 200) {
                NSLog("Successfully unblocked user '\(unblockUsername)' - status code: \(String(describing: statusCode))")
                self.refreshBlockedUsersTableView(self)
            } else {
                NSLog("Error unblocking user '\(unblockUsername)' - status code: \(String(describing: statusCode))")
            }
        }
    }
    
    func setUpNoBlockedUsersBanner(isHidden: Bool) {
        noBlockedUsersBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: (navigationController?.navigationBar.frame.maxY)! + 10), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noBlockedUsersBanner.titleLabel?.textAlignment = .center
        noBlockedUsersBanner.titleLabel?.lineBreakMode = .byWordWrapping
        noBlockedUsersBanner.setTitle("No blocked users", for: .normal)
                  
        noBlockedUsersBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noBlockedUsersBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noBlockedUsersBanner.layer.shadowOpacity = 0.5
        noBlockedUsersBanner.clipsToBounds       = true
        noBlockedUsersBanner.layer.masksToBounds = false
                                
        noBlockedUsersBanner.isHidden = isHidden
        blockedUsersTableView.isHidden = !isHidden
        self.view.addSubview(noBlockedUsersBanner)
    }
}

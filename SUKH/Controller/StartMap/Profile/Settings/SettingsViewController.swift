//
//  SettingsViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 14.11.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

struct CustomizableSettingsCell {
    let name: String
    let identity: String
}

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var SettingsTableView: UITableView!
    
    var dataSource: [CustomizableSettingsCell] = []
    
    let authService = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        SettingsTableView.dataSource = self
        SettingsTableView.delegate = self
        
        // Setup UI
        setUpNavigationBar()
        setUpSettingsTableView()
        
        // Fill SettingsTableView
        loadDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadDataSource(){
        dataSource.append(CustomizableSettingsCell(name: "My Events", identity: "MyEventsViewController"))
        dataSource.append(CustomizableSettingsCell(name: "Legal Notice", identity: "ID_Legal_Notice"))
        dataSource.append(CustomizableSettingsCell(name: "Contact", identity: "ContactViewController"))
        dataSource.append(CustomizableSettingsCell(name: "About", identity: "ID_About"))
        dataSource.append(CustomizableSettingsCell(name: "Edit User", identity: "EditUserViewController"))
        dataSource.append(CustomizableSettingsCell(name: "Blocked users", identity: "BlockedUsersViewController"))
        dataSource.append(CustomizableSettingsCell(name: "Logout", identity: "ID_Logout"))
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Settings"
    }
    
    func setUpSettingsTableView() {
        self.SettingsTableView.backgroundColor = UIColor(named: "SukhBackgroundColor")
        self.SettingsTableView.rowHeight = 44.0
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let settingsCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SettingsTableViewCell else {
            return UITableViewCell()
        }
        
        let setting = dataSource[indexPath.row]
        
        // Setup settingsCell
        settingsCell.settingsLabel.text = setting.name

        // Sets the clicked color of cell to lightgray
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray
        settingsCell.selectedBackgroundView = backgroundView
                
        return settingsCell
    }
    
    // From UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewControllerName = dataSource[indexPath.row].identity
        
        // Check if logout
        if (viewControllerName != "ID_Logout") {
            let viewController = storyboard?.instantiateViewController(withIdentifier: viewControllerName)
            self.navigationController?.pushViewController(viewController!, animated: true)
        } else {
            // handle logout
            askIfUserWantsToLogOutIfYesPerformLogout()
        }
        // deselect row, so that cell doesn't stays clicked
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Logout extension
extension SettingsViewController {
        
    func askIfUserWantsToLogOutIfYesPerformLogout() {
        let userLogoutAlert = UIAlertController(title: "Do you want to log out", message: "", preferredStyle: .alert)
            
        userLogoutAlert.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: { (alert: UIAlertAction!) in
            self.logoutUser()
        }))
        
        userLogoutAlert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))

        self.present(userLogoutAlert, animated: true)
    }
    
    func logoutUser() {
        // Delete all users from device
        
        authService.removeUserAndPasswordAndRole()
        self.authService.resetUserDefaults()
        self.authService.resetSDWebImageCache()

        self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
    }
}

//
//  StartMapViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 11.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import MapKit
import Reachability

enum EventType : String {
    case Party
    case Hangout
    case Productive
}

enum Filter : String {
    case Upcoming
    case Previous
}

var initialCenterOfMap = MKMapPoint()

class StartMapViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var eventListButton: UIButton!
    @IBOutlet weak var goToUserLocation: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var addEvent: UIButton!
    @IBOutlet weak var diveInButton: UIButton!
    
    @IBOutlet weak var discoverLabel: UILabel!
    @IBOutlet weak var diveIn: UILabel!
    
    @IBOutlet weak var saveEventLocationButton: GoToLocationButton!
    
    @IBOutlet weak var dismissAddEventButton: UIButton!
    @IBOutlet weak var currentFilterLabel: UILabel!
    @IBOutlet weak var notificationIndicator: UIImageView!
    
    // Services
    let locationService = LocationService()
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let classics = Classics()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    
    // Variables
    var dataSource: [EventAnnotation] = []
    var dataSourcePrevious: [EventAnnotation] = []
    var heightOfMap = MAXFLOAT
    var addEventLocationAnnotation = MKPointAnnotation()
    var tapForNewEventGestureRecognizer = UIGestureRecognizer()
    var activityIndicator = UIActivityIndicatorView()
    var addEventLocation = CLLocation()
    var userLocation = CLLocation()
    var requestingData = false
    var loadUpcomingOnlyOnce = false
    var loadPreviousOnlyOnce = false
    var needsResize = false
    var selectedEventAnnotationFromDiveInView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Go to user position before all
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
                
        // Setup
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveEventLocationData(_:)), name: .didReceiveEventLocationData, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveLocationData(_:)), name: .didReceiveLocationData, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setAlpha(_:)), name: .setAlpha, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(filterChanged), name: NSNotification.Name.init("setFiltersMap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillAppear(_:)), name: .reloadMap, object: nil)
        initialCenterOfMap = MKMapPoint(x: Double(mapView.center.x), y: Double(mapView.center.y))
        activityIndicator = Spinner().getMapActivityIndicator(view: view.self)
        
        locationService.startUpdatingLocation()
        locationService.setCenterOfMap(location: locationService.getUserLocation())
        
        setUpFilterLabel()
        setUpNavigationBar()
        setUpMapView()
        setUpButtons()
        setUpLabels()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (authService.getRoleOfCurrentUser() != .Guest) {
            getNotifications()
        }
        connectionService.setReachabilityNotifier(forViewController: self)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // This reloads the events and sets the filter heading
        currentFilterLabel.text = locationService.getMapFilter().name
        getAllEventsAndAddAnnotationsToMap(location: locationService.getCenterOfMap())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        
        // Deselect all map annotations
        for annotation in mapView.selectedAnnotations {
            mapView.deselectAnnotation(annotation, animated: true)
        }
    }
        
    func setUpNavigationBar() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func setUpFilterLabel() {
        currentFilterLabel.accessibilityIdentifier = "filter"
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnFilter))
        currentFilterLabel.isUserInteractionEnabled = true
        currentFilterLabel.addGestureRecognizer(tap)
    }
    
    func setUpMapView() {
        mapView.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.mapType = .mutedStandard
        if #available(iOS 13.0, *) {
            mapView.pointOfInterestFilter = MKPointOfInterestFilter(including: [.university, .atm, .bank, .beach, .brewery, .fitnessCenter, .hospital, .hotel, .nightlife, .park, .parking, .publicTransport, .restaurant])
        }
        mapView.isPitchEnabled = false
        mapView.isRotateEnabled = false
    }
    
    func setUpButtons() {
        profileButton.accessibilityIdentifier = "profile"
        dismissAddEventButton.isHidden = true
        saveEventLocationButton.isHidden = true
        saveEventLocationButton.setTitle("Tap on the Map to select a Location", for: .normal)
    }
    
    func setUpLabels() {
        discoverLabel.font = UIFont.boldSystemFont(ofSize: 14)
        diveIn.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    @objc func tapOnFilter() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:"EventsFilterViewController") as? EventsFilterViewController {
            vc.modalPresentationStyle = .popover
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func filterChanged() {
        removeAllAnnotationsAndClearDataSource()
        currentFilterLabel.text = locationService.getMapFilter().name
        getAllEventsAndAddAnnotationsToMap(location: locationService.getCenterOfMap())
    }
    
    @IBAction func goToProfile(_ sender: Any) {
        if (authService.getRoleOfCurrentUser() != .Guest) {
            if let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
                let nav = UINavigationController.init(rootViewController: vc)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        } else {
            setSignUpAlert()
        }
    }
    
    @IBAction func goToEventList(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EventsRankingViewController") as? EventsRankingViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func goToUserLocation(_ sender: Any) {
        removeAllAnnotationsAndClearDataSource()
        
        let location = locationService.getUserLocation()
        getAllEventsAndAddAnnotationsToMap(location: location)
        
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
    }
    
    @IBAction func search(_ sender: UIButton) {
        if (authService.getRoleOfCurrentUser() != .Guest) {
            if let vc =  UIStoryboard.init(name: "Friends", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            setSignUpAlert()
        }
    }
    
    @IBAction func diveIn(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        if let vc =  UIStoryboard.init(name: "DiveIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "DiveInPageRootViewController") as? DiveInPageRootViewController {
            view.alpha = 0.5
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = .clear
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func diveInToSpecificEvent(eventID: String) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        if let vc =  UIStoryboard.init(name: "DiveIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "DiveInPageRootViewController") as? DiveInPageRootViewController {
            view.alpha = 0.5
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = .clear
            vc.passedEventID = eventID
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}

// Extension for map related functions
extension StartMapViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? EventAnnotation else {
          return nil
        }
        
        let identifier = annotation.eventID
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
            view.displayPriority = .required
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.displayPriority = .required
        }
        switch annotation.type {
        case .Party:
            view.markerTintColor = UIColor(named: "Party")
        case .Hangout:
            view.markerTintColor = UIColor(named: "Hangout")
        case .Productive:
            view.markerTintColor = UIColor(named: "Productive")
        }
        
        if annotation.isOver! {
            //view.alpha = 0.5
        }
        
        view.annotation = annotation
        view.canShowCallout = true
        
        if #available(iOS 11.0, *) {
            view.clusteringIdentifier = "event"
        } else {
           // Fallback on earlier versions
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // At this point we don't want to display selected annotations on the map.
        // Reason is that we can't click on selected events to open them in the
        // dive in view. This would lead to unwanted UX.
        mapView.deselectAnnotation(view.annotation, animated: false)
        if let clustered = view.annotation as? MKClusterAnnotation {
            mapView.showAnnotations(clustered.memberAnnotations, animated: true)
        }
        if (!self.selectedEventAnnotationFromDiveInView) {
            if let eventAnnotation = view.annotation as? EventAnnotation {
                if (authService.getRoleOfCurrentUser() != .Guest) {
                    diveInToSpecificEvent(eventID: eventAnnotation.eventID)
                } else {
                    setSignUpAlert()
                }
            }
        } else {
            // don't open dive in
            self.selectedEventAnnotationFromDiveInView = false
        }
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        checkIfMapNeedsToBeUpdated()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailFromMapSeque" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
        if segue.identifier == "passLocationToEventSegue" {
            if let destination = segue.destination as? AddEventViewController,
                let eventLocation = sender as? CLLocation {
                destination.eventLocation = eventLocation
            }
        }
    }
    
    func checkIfMapNeedsToBeUpdated() {
        // Get map visible rect to get all four edges of the map
        let mapVisibleRect = self.mapView.visibleMapRect
        
        // Create points for all map edges
        let neMapPoint = MKMapPoint(x: mapVisibleRect.maxX, y: mapVisibleRect.origin.y)
        let seMapPoint = MKMapPoint(x: mapVisibleRect.maxX, y: mapVisibleRect.maxY)
        let swMapPoint = MKMapPoint(x: mapVisibleRect.origin.x, y: mapVisibleRect.maxY)
        let nwMapPoint = MKMapPoint(x: mapVisibleRect.minX, y: mapVisibleRect.origin.y)

        // Create point for new center of map
        let newCenterOfMapLocation = MKMapPoint(x: mapVisibleRect.midX, y: mapVisibleRect.midY)
               
        // Get screen height in meters -> eqiuvalent make radius of api call
        let newHeightOfMap = swMapPoint.distance(to: nwMapPoint)
        
        if (newHeightOfMap > 1000000) {
            return // On first zoom skip the call, because to high TODO: find better way
        }
        
        // When a edge of the screen view 'touches' the circle of the previous radius the map data gets updated from mongo
        let neToInitialCenterDistance = neMapPoint.distance(to: initialCenterOfMap)
        let seToInitialCenterDistance = seMapPoint.distance(to: initialCenterOfMap)
        let swToInitialCenterDistance = swMapPoint.distance(to: initialCenterOfMap)
        let nwToInitialCenterDistance = nwMapPoint.distance(to: initialCenterOfMap)
        
        // Bool to check if user big zooms in
        let bigZoomIn = (Float(newHeightOfMap) < heightOfMap * 0.4)

        let radius: Double = Double(locationService.getSearchRadius())
        if (bigZoomIn || neToInitialCenterDistance > radius || seToInitialCenterDistance > radius || swToInitialCenterDistance > radius || nwToInitialCenterDistance > radius) {
            // Print out for better understanding and adjustments
            //print("Big change: \(radius) Radius")
            //print("Big zoomin: \(bigZoomIn)")

            // Update values for new calculation
            heightOfMap = Float(newHeightOfMap)
            initialCenterOfMap = newCenterOfMapLocation
            locationService.setCenterOfMap(location: CLLocation(latitude: newCenterOfMapLocation.coordinate.latitude, longitude: newCenterOfMapLocation.coordinate.longitude))
            locationService.setSearchRadius(radius: Int(newHeightOfMap) * 5)
            
            // Get updated events with center and radius
            getAllEventsAndAddAnnotationsToMap(location: locationService.getCenterOfMap())
        }
    }
}

// Extension
extension StartMapViewController {
    
    func getAllEventsAndAddAnnotationsToMap(location: CLLocation){
        if (!requestingData) {
            setLoading(true)
            if (connectionService.getConnectionStatus() != .unavailable) {
                mongoQueryService.getEventsFiltered(filter: locationService.getMapFilter()) { (response) in
                    let statusCode: Int = response.response!.statusCode
                    if (statusCode == 200) {
                        NSLog("Successfully requested events with status code: \(statusCode)")
                        let data: Data = response.data!
                        let events = Decoder().decodeEvents(eventData: data)
                        self.setLoading(false)
                        self.updateDataSource(withEvents: events)
                    } else {
                        NSLog("Failed requesting events with status code: \(statusCode)")
                        self.setLoading(false)
                    }
                }
            } else {
                NSLog("Failed requesting events: no connection")
                setLoading(false)
            }
        }
    }
    
    func sortDataSource() {
        _ = dataSource.sorted() { $0.distance! > $1.distance! }
    }
    
}

// Go to locations from search view controller
extension StartMapViewController {
    @objc func onDidReceiveEventLocationData(_ notification:Notification) {
        if let data = notification.userInfo as? [String: Any]
        {
            let location: CLLocationCoordinate2D = data["location"] as! CLLocationCoordinate2D
            let eventID: String = data["eventID"] as! String
            let name: String = data["name"] as! String
            let type: EventType = data["type"] as! EventType
                        
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: 600, longitudinalMeters: 600)
            mapView.setRegion(viewRegion, animated: false)
            
            let annotation = EventAnnotation(eventID: eventID,
                                            title: name,
                                            type: type,
                                            information: type.rawValue,
                                            coordinate: location,
                                            distance: 10000,
                                            isOver: false)
            
            if (!dataSource.contains(where: { (eventAnnotation) -> Bool in
                if (eventAnnotation.eventID == eventID) {
                    self.selectedEventAnnotationFromDiveInView = true
                    self.mapView.selectAnnotation(eventAnnotation, animated: false)
                    return true
                } else {
                    return false
                }
            })) {
                // Only add annotation if it isn't already on the map
                self.mapView.addAnnotation(annotation)
                self.selectedEventAnnotationFromDiveInView = true
                self.mapView.selectAnnotation(annotation, animated: false)
            }
        }
    }
    
    @objc func onDidReceiveLocationData(_ notification:Notification) {
        if let data = notification.userInfo as? [String: Any]
        {
            let location: CLLocationCoordinate2D = data["location"] as! CLLocationCoordinate2D
            let name: String = data["name"] as! String
                        
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: 600, longitudinalMeters: 600)
            mapView.setRegion(viewRegion, animated: false)
            
            let annotation = MKPointAnnotation()
            annotation.title = name
            annotation.coordinate = location
            
            removeAllAnnotationsAndClearDataSource()
            self.mapView.addAnnotation(annotation)
        }
    }
}

// Add event
extension StartMapViewController: UIGestureRecognizerDelegate {
        
    @IBAction func addEvent(_ sender: Any) {
        if (authService.getRoleOfCurrentUser() != .Guest) {
            
            profileButton.isHidden = true
            goToUserLocation.isHidden = true
            searchButton.isHidden = true
            eventListButton.isHidden = true
            addEvent.isHidden = true
            diveInButton.isHidden = true
            saveEventLocationButton.isHidden = false
            saveEventLocationButton.isEnabled = false
            dismissAddEventButton.isHidden = false
            
            diveIn.isHidden = true
            discoverLabel.isHidden = true
            
            // Filter
            currentFilterLabel.isHidden = true
            
            // NotificationIndicator
            notificationIndicator.isHidden = true
            
            setUpMapViewForAddEvent()
        } else {
            setSignUpAlert()
        }
    }
    
    func setUpMapViewForAddEvent() {
        // Setup gestureRecognizer
        tapForNewEventGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapForNewEventGestureRecognizer.delegate = self
        mapView.addGestureRecognizer(tapForNewEventGestureRecognizer)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {

        // Remove previous eventLocationAnnotation from mapView
        mapView.removeAnnotation(addEventLocationAnnotation)
        
        // Get coordinate of point on mapView
        let location = sender.location(in: mapView)
        let eventCoordinates = mapView.convert(location,toCoordinateFrom: mapView)
        
        // set addEventLocation
        addEventLocation = CLLocation(latitude: eventCoordinates.latitude, longitude: eventCoordinates.longitude)
                
        // Center mapView to tapped point
        mapView.setCenter(eventCoordinates, animated: true)
        
        // Make saveLocationButton visible
        saveEventLocationButton.setTitle("Save location", for: .normal)
        saveEventLocationButton.isEnabled = true

        // Add annotation:
        addEventLocationAnnotation.coordinate = eventCoordinates
        mapView.addAnnotation(addEventLocationAnnotation)
    }
    
    @IBAction func safeEventLocationAction(_ sender: Any) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        performSegue(withIdentifier: "passLocationToEventSegue", sender: addEventLocation)
        cleanUpButtonsAndLabels()
        cleanUpView()
    }
    
    @IBAction func dismissAddEventAction(_ sender: Any) {
        cleanUpButtonsAndLabels()
        cleanUpView()
    }
    
    func cleanUpButtonsAndLabels() {
        profileButton.isHidden = false
        eventListButton.isHidden = false
        goToUserLocation.isHidden = false
        addEvent.isHidden = false
        diveInButton.isHidden = false
        saveEventLocationButton.isHidden = true
        saveEventLocationButton.isEnabled = false
        saveEventLocationButton.setTitle("Tap on the Map to select a Location", for: .normal)
        dismissAddEventButton.isHidden = true
        
        diveIn.isHidden = false
        discoverLabel.isHidden = false
        searchButton.isHidden = false
        
        // Filter
        currentFilterLabel.isHidden = false
        
        // NotificationIndicator
        notificationIndicator.isHidden = false
    }
    
    func cleanUpView() {
        // Remove previous eventLocationAnnotation from mapView
        mapView.removeAnnotation(addEventLocationAnnotation)
        mapView.removeGestureRecognizer(tapForNewEventGestureRecognizer)
    }
}

// Helper functions
extension StartMapViewController {
    
    func removeAllAnnotationsAndClearDataSource() {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        self.dataSource = []
    }
    
    func updateDataSource(withEvents: [Event]) {
        // Remove old (events that are not in dataSource) eventAnnotations from map
        for eventAnnotation in dataSource {
            if (!withEvents.contains(where: { (event) -> Bool in
                if (event.eventID == eventAnnotation.eventID) {
                    return true
                } else {
                    return false
                }
            })) {
               mapView.removeAnnotation(eventAnnotation)
               let index = dataSource.firstIndex(of: eventAnnotation)!
               dataSource.remove(at: index)
            }
        }
        
        if (withEvents.count > 0) {
            for event in withEvents {
                // Check if already in dataSource
                var eventAnnotationIsAlreadyOnMap = false
                for annotation in mapView.annotations {
                    if let eventAnnotation = annotation as? EventAnnotation {
                        if (eventAnnotation.eventID == event.eventID) {
                            eventAnnotationIsAlreadyOnMap = true
                            break
                        }
                    }
                }

                // Add to dataSource and map if neccessary
                if (!eventAnnotationIsAlreadyOnMap) {
                    let eventAnnotation = EventAnnotation(eventID: event.eventID,
                                                        title: event.name,
                                                        type: EventType(rawValue: event.type!)!,
                                                        information: event.type,
                                                        coordinate: CLLocationCoordinate2D(latitude: event.latitude!, longitude: event.longitude!),
                                                        distance: classics.calculateDistanceInMeters(latitude: event.latitude!, longitude: event.longitude!),
                                                        isOver: classics.isOver(end: event.endTime!))
                    
                    self.dataSource.append(eventAnnotation)
                    self.mapView.addAnnotation(eventAnnotation)
                }
            }
            
            // Resize map to nearby events if neccessary
            if (!needsResize) {
                needsResize = true
                resize()
            }
        } else {
            // TODO: handle no events in radius
            // load more ? tell user? etc.
        }
    }
    
    func resize() {
        // Resize to nearby annotations
        // TODO: do this when go to user location is pressed
        let sorted = dataSource.sorted(by: { $0.distance! < $1.distance! })
        let minDistance: Double = 500
        let location = CLLocationCoordinate2D(latitude: locationService.getCenterOfMap().coordinate.latitude, longitude: locationService.getUserLocation().coordinate.longitude)
        
        if (sorted.count > 3) {
            var meters = sorted[3].distance!
            if (meters < minDistance) {
                meters = minDistance
            }
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: meters, longitudinalMeters: meters)
            mapView.setRegion(viewRegion, animated: false)
        } else if (sorted.count > 2) {
            var meters = sorted[2].distance! * 2
            if (meters < minDistance) {
                meters = minDistance
            }
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: meters, longitudinalMeters: meters)
            mapView.setRegion(viewRegion, animated: false)
        } else if (sorted.count > 1) {
            var meters = sorted[1].distance! * 2
            if (meters < minDistance) {
                meters = minDistance
            }
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: meters, longitudinalMeters: meters)
            mapView.setRegion(viewRegion, animated: false)
        } else {
            var meters = sorted[0].distance! * 2
            if (meters < minDistance) {
                meters = minDistance
            }
            let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: meters, longitudinalMeters: meters)
            mapView.setRegion(viewRegion, animated: false)
        }
    }
    
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        requestingData = loading
    }
    
    func getNotifications() {
        let username = authService.getCurrentUser()
        
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.getNotificationsOfUser(username: username) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    let notificationsOfUser = Decoder().decodeSukhNotification(data: data)
                    
                    if (notificationsOfUser.eventInvitations! > 0 || notificationsOfUser.newFollowers! > 0) {
                        self.notificationIndicator.tintColor = UIColor.red
                    } else {
                        self.notificationIndicator.tintColor = UIColor.clear
                    }
                } else {
                    NSLog("Error retrieving sukhNotifications: \(statusCode!)")
                }
            }
        }
    }
    
    func setSignUpAlert() {
        let signUpAlert = UIAlertController(title: "You want more?", message: "Sign in to get the full experience", preferredStyle: .alert)
            
        signUpAlert.addAction(UIAlertAction(title: "Sign in", style: .default, handler: { (alert: UIAlertAction!) in
            // Go to login
            self.authService.removeUserAndPasswordAndRole()
            self.authService.resetUserDefaults()
            self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
        }))
        
        signUpAlert.addAction(UIAlertAction(title: "Later", style: .cancel, handler: nil))
        signUpAlert.view.accessibilityIdentifier = "guest"
        self.present(signUpAlert, animated: true)
    }
    
    // Set alpha back to 1 when returning from dive in view
    @objc func setAlpha(_ notification:Notification) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1
        })
    }
}

// Reachability
extension StartMapViewController {
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            getAllEventsAndAddAnnotationsToMap(location: locationService.getUserLocation())
        case .cellular:
            getAllEventsAndAddAnnotationsToMap(location: locationService.getUserLocation())
        case .none:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not reachable")
        case .unavailable:
            basicAlerts.setConnectionAlert(forViewController: self)
            NSLog("Not available")
        }
    }
}

class AnnotationTapGesture: UITapGestureRecognizer {
    var eventID = String()
}

extension Notification.Name {
    static let didReceiveEventLocationData = Notification.Name("didReceiveEventLocationData")
    static let didReceiveLocationData = Notification.Name("didReceiveLocationData")
    static let setAlpha = Notification.Name("setAlpha")
    static let reloadMap = Notification.Name("reloadMap")
}


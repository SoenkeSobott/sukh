//
//  DiveInNoDataViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 11.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInNoDataViewController: UIViewController {

    // Outlets
    @IBOutlet weak var noDataFoundView: DiveInImageView!
    @IBOutlet weak var heading: DiveInHeading!
    @IBOutlet weak var tip: DiveInInfo!
    
    // Services
    
    // Variables
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpBackground()
        setUpLabels()
        
    }
    
    func setUpBackground() {
        if !UIAccessibility.isReduceTransparencyEnabled {
            noDataFoundView.backgroundColor = .clear

            let blurEffect = UIBlurEffect(style: .light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = noDataFoundView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            noDataFoundView.addSubview(blurEffectView)
        } else {
            noDataFoundView.backgroundColor = UIColor(named: "SukhBackgroundColor")
        }
    }
    
    func setUpLabels() {
        heading.numberOfLines = 0
        heading.text = "Uff\n -\n Nothing going on"
        heading.textAlignment = .center
        
        tip.numberOfLines = 0
        tip.text = "Search other locations or\n Press the + button on the map to create a new event"
        tip.textAlignment = .center
    }
}


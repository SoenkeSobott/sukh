//
//  DiveInPageRootViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

enum Direction {
    case Right
    case Left
}

class DiveInPageRootViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let locationService = LocationService()
    let classics = Classics()
    
    // Variables
    var events:[Event] = []
    var currentIndex = 0
    var passedEventID: String? = nil
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        getEvents()
        view.backgroundColor = UIColor.clear
        self.dataSource = self
        self.delegate = self
        connectionService.setReachabilityWithoutNotifier()
        
        // Initial Screen
        let sb = UIStoryboard(name: "DiveIn", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DiveInLoadingViewController")
        vc.view.backgroundColor = .clear
        self.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dragToDismiss(_ :)))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func dragToDismiss(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        
        if (translation.y > 0) {
            view.frame.origin.y = translation.y
            
            if gesture.state == .ended {
                let velocity = gesture.velocity(in: view)
                
                if (velocity.y >= 1150) {
                    NotificationCenter.default.post(name: .setAlpha, object: self)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.view.frame.origin = CGPoint(x: 0, y: 0)
                    })
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func getEvents(){
        if (connectionService.getConnectionStatus() != .unavailable) {
            self.events = []
            
            mongoQueryService.getEventsFiltered(filter: locationService.getMapFilter()) { (response) in
                let statusCode: Int = response.response!.statusCode
                if (statusCode == 200) {
                    NSLog("Successfully requested events with status code: \(statusCode)")
                    
                    let data: Data = response.data!
                    self.events = Decoder().decodeEvents(eventData: data)
                    
                    if (self.events.count > 0) {
                        if (self.passedEventID != nil && self.passedEventID!.count > 0) {
                            self.currentIndex = self.getIndexByEventID(eventID: self.passedEventID!, events: self.events)
                        }
                        let vc = self.viewControllerWithData(index: self.currentIndex)
                        vc.view.backgroundColor = .clear
                        self.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
                    } else {
                        // handle no data found
                        let sb = UIStoryboard(name: "DiveIn", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "DiveInNoDataViewController")
                        self.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
                    }
                } else {
                    NSLog("Failed requesting events with status code: \(statusCode)")
                }
            }
        } else {
            let sb = UIStoryboard(name: "DiveIn", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "DiveInLoadingViewController")
            self.setViewControllers([vc], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func viewControllerWithData(index: Int) -> DiveInViewController {
        let sb = UIStoryboard(name: "DiveIn", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DiveInViewController") as! DiveInViewController
        vc.event = events[index]
        
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vc = viewController as? DiveInViewController else {
            return nil
        }
        
        guard let viewControllerIndex = events.firstIndex(of: vc.event) else { return nil  }
                
        let previousIndex = viewControllerIndex - 1
        currentIndex = previousIndex + 1

        guard previousIndex >= 0 else {
            return nil
        }
        
        guard events.count > previousIndex else { return nil }
    
        return viewControllerWithData(index: previousIndex)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let vc = viewController as? DiveInViewController else {
            return nil
        }
        
        guard let viewControllerIndex = events.firstIndex(of: vc.event) else { return nil  }
        
        let nextIndex = viewControllerIndex + 1
        
        currentIndex = nextIndex - 1
        
        guard events.count != nextIndex else {
            return nil
        }

        guard events.count > nextIndex else {
            return nil
        }
        
        return viewControllerWithData(index: nextIndex)
    }
    
    func getIndexByEventID(eventID: String, events: [Event]) -> Int {
        for (index, event) in events.enumerated() {
            if (event.eventID == eventID) {
                return index
            }
        }
        NSLog("Error: Couldn't find events in dive in events for ID '\(eventID)' - returning default value.")
        return 0 // Should not happen - return default
    }
}

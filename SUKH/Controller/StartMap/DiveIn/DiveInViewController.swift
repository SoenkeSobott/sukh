//
//  DiveInViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import AVFoundation

class DiveInViewController: UIViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    // Outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: DiveInInfo!
    @IBOutlet weak var time: DiveInInfo!
    @IBOutlet weak var goingUsers: DiveInInfo!
    @IBOutlet weak var shadowView: DiveInView!
    @IBOutlet weak var media: DiveInImageView!
    @IBOutlet weak var mediaBack: UIButton!
    @IBOutlet weak var mediaForward: UIButton!
    @IBOutlet weak var votesButton: UIButton!
    @IBOutlet weak var downImage: UIImageView!
    @IBOutlet weak var downLabel: UILabel!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var upLabel: UILabel!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let reportsMongoService = ReportsMongoService()
    let classics = Classics()
    let basicAlerts = BasicAlerts()
    let authService = AuthService()
    
    // Variables
    var activityIndicator = UIActivityIndicatorView()
    var event: Event = Event()
    var mediaLinks: [String] = []
    var mediaCounter = 0
    var mediaTimer = Timer()
    
    // TODO:
    /* - display basic infos (name of media uploader?)
     * - sort date -> you want to see what going on right now (quick filters (This eveningn/ this weekend / rightnow))
     * - add media directly button
     * - Vote from here
     * - Handle last event (-> load more)
     * - what to do with private events?
     * - indicator if event is for free
     */
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        connectionService.setReachabilityWithoutNotifier()
        self.activityIndicator = Spinner().getActivityIndicator(view: view.self)
        
        getMedia()
        setUpView()
        setUpLongPress()
        setUpMediaTimer()
        setUpVotes()
        addTapToName()
    }
    
    func setUpView() {
        name.text = event.name
        location.text = classics.getDistanceStringFromCoordinates(latitude: event.latitude!, longitude: event.longitude!)
        goingUsers.text = classics.getGoingUsersString(goingUsers: event.goingUsers)
        time.attributedText = classics.timeToStart(ISO8601DateStringStart: event.startTime!, ISO8601DateStringEnd: event.endTime!, size: 14)
    }
    
    func setUpVotes() {
        // Set flipped triangle
        var flippedTriangle = classics.setSystemImageOrDefault(systemName: "triangle.fill")
        flippedTriangle = flippedTriangle.sd_flippedImage(withHorizontal: false, vertical: true)!
        if #available(iOS 13.0, *) {
            downImage.image = flippedTriangle.withTintColor(UIColor(named: "SUKHDark")!)
        } else {
            downImage.image = flippedTriangle
        }
        
        // Votes
        downLabel.text = String(classics.calculateDownVotes(votes: event.votes!))
        upLabel.text = String(classics.calculateUpVotes(votes: event.votes!))
    }
    
    @IBAction func goToLocation(_ sender: Any) {
        NotificationCenter.default.post(name: .setAlpha, object: self)
        self.dismiss(animated: true, completion: nil)
        
        // call method in StartMapViewController
        let eventInfo = ["location": CLLocationCoordinate2D(latitude: event.latitude!, longitude: event.longitude!), "eventID": event.eventID as Any, "name": event.name as Any, "type": EventType(rawValue: event.type!) as Any, "endTime": event.endTime!] as [String : Any]
        NotificationCenter.default.post(name: .didReceiveEventLocationData, object: self, userInfo: eventInfo)
    }
    
    @IBAction func eventActions(_ sender: Any) {
        let alert = UIAlertController(title: "Event", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Report Event", style: .destructive, handler: { _ in
            self.setReportEventAlert()
        }))

        alert.addAction(UIAlertAction(title: "Share Event", style: .default, handler: { _ in
            self.basicAlerts.setFeatureComingSoonAlert(forViewController: self)
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    func setReportEventAlert() {
        let reportUserAlert = UIAlertController(title: "Report Event", message: "Do you want to report this Event?", preferredStyle: .alert)
            
        reportUserAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            // Report event
            let ISO8601DateString = ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime])
            
            let eventID = self.event.eventID!
            let newReport = Report(reportId: UUID().uuidString,
                                   type: ReportType.Event.rawValue,
                                   subject: eventID,
                                   createdBy: self.authService.getCurrentUser(),
                                   createdAt: ISO8601DateString)
            
            self.reportsMongoService.postReport(report: newReport) { (statusCode) in
                if (statusCode == 201) {
                    self.basicAlerts.setReportEventAlert(forViewController: self)
                    NSLog("Successfully reported event '" + eventID + "' with status code: \(statusCode)")
                } else {
                    NSLog("Error reporting event '" + eventID + "' with status code: \(statusCode)")
                }
            }
        }))
        reportUserAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(reportUserAlert, animated: true)
    }
    
}

// Media
extension DiveInViewController {
    func setUpLongPress() {
        let longPressOne = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressOne.minimumPressDuration = 0.5
        longPressOne.delaysTouchesBegan = true
        longPressOne.delegate = self
        self.mediaBack.addGestureRecognizer(longPressOne)
    
        let longPressTwo = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressTwo.minimumPressDuration = 0.5
        longPressTwo.delaysTouchesBegan = true
        longPressTwo.delegate = self
        self.mediaForward.addGestureRecognizer(longPressTwo)
    }
    
    func setUpMediaTimer() {
        mediaTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.mediaForward(_:)), userInfo: nil, repeats: true)
    }
    
    func getMedia() {
        mediaLinks.append(event.eventImageURL!)
        for entry in event.eventFeedEntries! {
            mediaLinks.append(entry.imageURL!)
        }
        
        UIView.transition(with: self.media, duration: 0.3, options: .transitionCrossDissolve, animations: { self.media.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.media.sd_setImage(with: URL(string: self.mediaLinks[self.mediaCounter]), placeholderImage: UIImage(named: "natureOne"))
        },
        completion: nil)
    }
    
    @IBAction func mediaBack(_ sender: Any) {
        if (self.mediaCounter >= 1) {
            self.mediaCounter -= 1
        } else {
            self.mediaCounter = mediaLinks.count-1
        }
        mediaTapped()
    }
    
    @IBAction func mediaForward(_ sender: Any) {
        if (self.mediaCounter < mediaLinks.count-1) {
            self.mediaCounter += 1
        } else {
            self.mediaCounter = 0
        }
        mediaTapped()
    }
    
    func mediaTapped() {
        // Reset Time
        mediaTimer.invalidate()
        setUpMediaTimer()
        UIView.transition(with: self.media, duration: 0.3, options: .transitionCrossDissolve, animations: { self.media.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.media.sd_setImage(with: URL(string: self.mediaLinks[self.mediaCounter]), placeholderImage: UIImage(named: "natureOne"))
        },
        completion: nil)
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            mediaTimer.invalidate()
        }
        else {
            setUpMediaTimer()
        }
    }
}

// go to event on tap on name
extension DiveInViewController {
    func addTapToName() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(goToEvent))
        name.addGestureRecognizer(tap)
        name.isUserInteractionEnabled = true
    }
    
    @objc func goToEvent() {
        if (authService.getRoleOfCurrentUser() != .Guest) {
            performSegue(withIdentifier: "showEventDetail", sender: event.eventID!)
        } else {
            setSignUpAlert()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventDetail" {
            if let destination = segue.destination as? PageRootViewController,
                let selectedEventID = sender as? String {
                destination.eventID = selectedEventID
            }
        }
    }
    
    func setSignUpAlert() {
        let signUpAlert = UIAlertController(title: "You want more?", message: "Sign in to get the full experience", preferredStyle: .alert)
            
        signUpAlert.addAction(UIAlertAction(title: "Sign in", style: .default, handler: { (alert: UIAlertAction!) in
            // Go to login
            self.authService.removeUserAndPasswordAndRole()
            self.authService.resetUserDefaults()
            self.performSegue(withIdentifier: "unwindToLoginViewController", sender: self)
        }))
        
        signUpAlert.addAction(UIAlertAction(title: "Later", style: .cancel, handler: nil))
        signUpAlert.view.accessibilityIdentifier = "guest"

        self.present(signUpAlert, animated: true)
    }
}


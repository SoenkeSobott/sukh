//
//  DiveInLoadingViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 11.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInLoadingViewController: UIViewController {

    // Outlets
    @IBOutlet weak var loadingImageView: DiveInImageView!
    
    // Services
    let spinner = Spinner()
    
    // Variables
    var activityIndicator = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setUpBackground()
        activityIndicator = spinner.getImageUploadActivityIndicator(view: loadingImageView)
        activityIndicator.startAnimating()
        
    }
    
    func setUpBackground() {
        if !UIAccessibility.isReduceTransparencyEnabled {
            loadingImageView.backgroundColor = .clear

            let blurEffect = UIBlurEffect(style: .light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = loadingImageView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            loadingImageView.addSubview(blurEffectView)
        } else {
            loadingImageView.backgroundColor = UIColor(named: "SukhBackgroundColor")
        }
    }
}

//
//  InviteFollowersViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 03.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import SDWebImage

class InviteFollowersViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var popViewButton: UIButton!
    @IBOutlet weak var inviteLabel: UILabel!
    @IBOutlet weak var searchField: SearchTextField!
    @IBOutlet weak var myFriendsTableView: UITableView!
    
    // Services
    let mongoQueryService = MongoQueryService()
    let spinner = Spinner()
    let authService = AuthService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    
    // Variables
    var currentUser = ""
    var dataSource: [UserCell] = []
    var noFollowersBanner = DecentStyleButton()
    var invited = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        currentUser = authService.getCurrentUser()
        getFriends()
        setUpMyFriendsTableView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.post(name: NSNotification.Name.init("setInvitedUsers"), object: nil)
    }
    
    func setUpMyFriendsTableView() {
        myFriendsTableView.dataSource = self
        myFriendsTableView.delegate = self
    }
    
    // Get friends of user
    func getFriends() {
        if (connectionService.getConnectionStatus() != .unavailable) {
            let activityIndicator = spinner.getActivityIndicator(view: myFriendsTableView)
            activityIndicator.startAnimating()
            
            mongoQueryService.getFollowersOfUser(username: currentUser) { (response) in
                let statusCode = response.response?.statusCode
                if (statusCode == 200) {
                    let data: Data = response.data!
                    
                    let users = Decoder().decodeUsers(data: data)
                    
                    self.fillDataSource(withUsers: users)
                    self.sortFriendsTable()
                    activityIndicator.stopAnimating()
                    activityIndicator.removeFromSuperview()
                } else {
                    NSLog("Error loading followers of user \(statusCode!)")
                    activityIndicator.stopAnimating()
                    activityIndicator.removeFromSuperview()
                }
            }
        } else {
            basicAlerts.setConnectionAlert(forViewController: self)
        }
    }
}

// Table View
extension InviteFollowersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let friendCell = tableView.dequeueReusableCell(withIdentifier: "inviteFriendCell", for: indexPath) as? InviteFriendTableViewCell else {
            return UITableViewCell()
        }
        
        // reset values
        friendCell.profileImage.image = nil
        
        
        let friend = dataSource[indexPath.row]
        
        // Setup eventCell
        friendCell.name.text = friend.username
        friendCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        friendCell.profileImage.sd_setImage(with: URL(string: friend.imageURL), placeholderImage: UIImage(named: "natureOne"), options: .refreshCached)
        
        friendCell.inviteButton.tag = indexPath.row
        friendCell.inviteButton.addTarget(self, action: #selector(inviteFriend), for: UIControl.Event.touchUpInside)
        friendCell.uninviteButton.tag = indexPath.row
        friendCell.uninviteButton.addTarget(self, action: #selector(uninviteFriend), for: UIControl.Event.touchUpInside)
        if (invitations.contains(friend.username)) {
            friendCell.inviteButton.isHidden = true
            friendCell.uninviteButton.isHidden = false
        } else {
            friendCell.inviteButton.isHidden = false
            friendCell.uninviteButton.isHidden = true
        }
        
        // Sets the clicked color of cell to lightgray
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray
        friendCell.selectedBackgroundView = backgroundView
        
        return friendCell
    }
    
    @objc func inviteFriend(sender: UIButton) {
        let cell = myFriendsTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! InviteFriendTableViewCell
        cell.uninviteButton.isHidden = false
        sender.isHidden = true
        let username = dataSource[sender.tag].username
        invitations.append(username)
    }
    
    @objc func uninviteFriend(sender: UIButton) {
        let cell = myFriendsTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! InviteFriendTableViewCell
        cell.inviteButton.isHidden = false
        sender.isHidden = true
        let username = dataSource[sender.tag].username
        invitations.remove(at: invitations.firstIndex(of: username)!)
    }
           
}

// Helper functions
extension InviteFollowersViewController {
    
    func fillDataSource(withUsers: [User]) {
        dataSource = []
        for user in withUsers {
            if (!updatedGoingUsers.contains(user.username!)) {
                self.dataSource.append(UserCell(username: user.username!, imageURL: user.profileImageURL!, followerStatus: FollowerStatus.Following))
            }
        }
        
        // Set no friends banner
        if (dataSource.count == 0) {
            setUpNoFollowersBanner(isHidden: false)
        } else {
            noFollowersBanner.removeFromSuperview()
        }
    }
        
    func sortFriendsTable() {
        dataSource.sort(by: {$0.username < $1.username})
        myFriendsTableView.reloadData()
    }
    
    func setUpNoFollowersBanner(isHidden: Bool) {
        noFollowersBanner = DecentStyleButton(frame: CGRect(origin: CGPoint(x: 10, y: searchField.frame.origin.y + 40), size: CGSize(width: self.view.frame.width - 20, height: 100)))
        noFollowersBanner.setTitle("Find some followers", for: .normal)
        
        noFollowersBanner.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        noFollowersBanner.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        noFollowersBanner.layer.shadowOpacity = 0.5
        noFollowersBanner.clipsToBounds       = true
        noFollowersBanner.layer.masksToBounds = false
        
        //noFriendsBanner.addTarget(self, action: #selector(addFriends), for: .touchUpInside)
                        
        noFollowersBanner.isHidden = isHidden
        self.myFriendsTableView.addSubview(noFollowersBanner)
    }
    
}

//
//  AddEventViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 17.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

var invitations: [String] = []

class AddEventViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var eventLocationMapView: MKMapView!
    
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var addEventImageButton: UIButton!
    
    @IBOutlet weak var eventNameTextField: InputTextField!
    @IBOutlet weak var eventTypePickerView: UIPickerView!
    
    @IBOutlet weak var eventTypeLabel: smallHeading!
    @IBOutlet weak var eventTypeTextField: InputTextField!
    
    @IBOutlet weak var startDateLabel: smallHeading!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    
    @IBOutlet weak var endDateLabel: smallHeading!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    
    @IBOutlet weak var invitePeopleLabel: smallHeading!
    @IBOutlet weak var invitePeopleButton: DecentStyleButton!
    
    @IBOutlet weak var privateTextField: InputTextField!
    @IBOutlet weak var privateSwitch: UISwitch!
    
    @IBOutlet weak var eventTagsLabel: smallHeading!
    @IBOutlet weak var eventTagsView: UIView!
    
    
    // Services
    let authService = AuthService()
    let mongoQueryService = MongoQueryService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    
    // Variables
    var eventLocation = CLLocation()
    var activityIndicator = UIActivityIndicatorView()
    let imagePickerController = UIImagePickerController()
    var eventTags = [String]()
    var rowCounter = 0
    let hidePickersDuration = 0.3
    let defaultImages = [UIImage(named: "ColorParty"), UIImage(named: "DarkClub"), UIImage(named: "DecentGathering"), UIImage(named: "DJAndCrowd"), UIImage(named: "NatureOne"), UIImage(named: "RedClub")]
    let eventTypes = ["Party", "Hangout", "Productive"]
    var eventTypePickerVisible = false
    var eventTypePickerSelectedRow = 1
    var isPublic: Bool = true
    var startDateChanged = false
    var endDateChanged = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        updatedGoingUsers = []
        invitations = []
        setUpEventTypePicker()
        setUpDateLabels()
        scrollView.delegate = self
        
        // Register to listen for a notification center
        NotificationCenter.default.addObserver(self, selector: #selector(setInvitedUsers(notification:)), name: NSNotification.Name.init("setInvitedUsers"), object: nil)
       
        setUpDatePickers()
        setUpMapView()
        setUpAddEventTagButton(x: 10, y: 10)
        setUpEventImage()
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
                
        scrollView.keyboardDismissMode = .onDrag
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveEvent))
        navigationItem.rightBarButtonItem?.accessibilityIdentifier = "save"

        privateTextField.isUserInteractionEnabled = false
        privateTextField.text = "Public"
    }
    
    func setUpMapView() {
        eventLocationMapView.layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        eventLocationMapView.layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        eventLocationMapView.layer.shadowOpacity = 0.5
        eventLocationMapView.clipsToBounds       = true
        eventLocationMapView.layer.masksToBounds = false
        
        eventLocationMapView.isUserInteractionEnabled = false
        eventLocationMapView.mapType = .mutedStandard
        let region = MKCoordinateRegion( center: eventLocation.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
        eventLocationMapView.setRegion(eventLocationMapView.regionThatFits(region), animated: false)
        
        let event = MKPointAnnotation()
        event.coordinate = eventLocation.coordinate
        eventLocationMapView.addAnnotation(event)
    }
    
    func setUpEventImage() {
        let random = Int.random(in: 0..<6)
        eventImage.image = defaultImages[random]
        addEventImageButton.tintColor = UIColor.white
    }
    
    func setUpEventTypePicker() {
        // TextField
        let tap = UITapGestureRecognizer(target: self,action: #selector(handleTaponTextField(_:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        eventTypeTextField.isUserInteractionEnabled = true
        eventTypeTextField.addGestureRecognizer(tap)
        eventTypeTextField.placeholder = "Event type"
        
        // Picker
        eventTypePickerView.isHidden = true
        eventTypePickerView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setUpDateLabels() {
        eventTypeLabel.text = "Type"
        eventTypeLabel.textColor = UIColor(named: "SukhTextColor")
        startDateLabel.text = "Start"
        startDateLabel.textColor = UIColor(named: "SukhTextColor")
        endDateLabel.text = "End"
        endDateLabel.textColor = UIColor(named: "SukhTextColor")
        invitePeopleLabel.text = "Invited people"

        eventTagsLabel.text = "Tags"
        eventTagsLabel.textColor = UIColor(named: "SukhTextColor")
    }
    
    func setUpDatePickers() {
        // Start Date Picker
        startDatePicker.isHidden = false
        startDatePicker.datePickerMode = .dateAndTime
        startDatePicker.minimumDate = Date(timeIntervalSinceNow: 0)
        startDatePicker.addTarget(self, action: #selector(self.startDateChanged(datePicker:)), for: .valueChanged)
        startDatePicker.accessibilityIdentifier = "startDatePicker"

        // End Date Picker
        endDatePicker.isHidden = false
        endDatePicker.datePickerMode = .dateAndTime
        endDatePicker.minimumDate = Date(timeIntervalSinceNow: 0)
        endDatePicker.addTarget(self, action: #selector(self.endDateChanged(datePicker:)), for: .valueChanged)
        endDatePicker.accessibilityIdentifier = "endDatePicker"
    }
    
    @IBAction func inviteFriendsButton(_ sender: Any) {

    }
    
    @objc func setInvitedUsers(notification: NSNotification) {
        let friendsString = makeInvitationsString(invites: invitations)
        invitePeopleButton.setTitle(friendsString, for: .normal)
    }
    
    @IBAction func publicPrivateSwitch(_ sender: UISwitch) {
        if (sender.isOn == true) {
            isPublic = true
        } else {
            isPublic = false
        }
    }
    
    @objc func saveEvent() {
        if (connectionService.getConnectionStatus() != .unavailable) {
            setLoading(true)
            
            let inputValid = checkIfInputIsPresentAndValid().0
            let missingInput = checkIfInputIsPresentAndValid().1
            if (inputValid) {
                // Create newEvent
                let eventImageData: Data = eventImage.image!.jpegData(compressionQuality: 0.5)! as Data
                
                let newEvent = Event(eventID: UUID().uuidString,
                                        eventImage: eventImageData,
                                        eventImageURL: nil,
                                        name: self.eventNameTextField.text,
                                        type: self.eventTypeTextField.text,
                                        startTime: ISO8601DateFormatter.string(from: self.startDatePicker.date, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        endTime: ISO8601DateFormatter.string(from: self.endDatePicker.date, timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        invitedUsers: invitations,
                                        goingUsers: nil,
                                        isPublic: self.isPublic,
                                        latitude: eventLocation.coordinate.latitude,
                                        longitude: eventLocation.coordinate.longitude,
                                        votes: nil,
                                        createdAt: ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime]),
                                        createdBy: authService.getCurrentUser(),
                                        tags: self.eventTags
                )
                     
                mongoQueryService.createEvent(event: newEvent) { (statusCode) -> () in
                    if (statusCode == 201) {
                        
                        // Close AddEventViewController
                        self.setLoading(false)
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        // TODO: handle else
                        self.setAddEventErrorAlert() // TODO: correct answer in there according statusCode
                        self.setLoading(false)
                    }
                }
            } else {
                basicAlerts.setCreateEventFillInAllFieldsAlert(forViewController: self, missingInput: missingInput)
                setLoading(false)
            }
        } else {
            basicAlerts.setConnectionAlert(forViewController: self)
        }
    }
}

// EventType picker
extension AddEventViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return eventTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return eventTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eventTypePickerSelectedRow = row
        eventTypeTextField.text = eventTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    @objc func handleTaponTextField(_ sender: UITapGestureRecognizer) {
        if (eventTypePickerVisible) {
            hidePickerView()
        } else {
            showPickerView()
        }
    }
   
    
    func showPickerView() {
        eventTypePickerVisible = true
        eventTypePickerView.selectRow(eventTypePickerSelectedRow, inComponent: 0, animated: false)
        eventTypeTextField.text = eventTypes[eventTypePickerSelectedRow]
                
        if let constraint = (eventTypePickerView.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 90.0
            eventTypePickerView.layoutIfNeeded()
            eventTypePickerView.isHidden = false
        }
        
    }
    
    func hidePickerView() {
        eventTypePickerVisible = false
        
        if let constraint = (eventTypePickerView.constraints.filter{$0.identifier == "eventTypePickerHeight"}.first) {
            constraint.constant = 0.0
            UIView.animate(withDuration: hidePickersDuration, animations: {
                 self.view.layoutIfNeeded()
            })
            eventTypePickerView.isHidden = true
        }
        
    }
}

// Upload image
extension AddEventViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBAction func uploadEventImage(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Encoding profile image
            setLoading(true)
            self.eventImage.image = image
            self.setLoading(false)
        } else {
            NSLog("Error uploading event image")
            self.dismiss(animated: true, completion: nil)
        }
        
        addEventImageButton.setBackgroundImage(classics.setSystemImageOrDefault(systemName: "ellipsis.circle"), for: .normal)

        self.dismiss(animated: true, completion: nil)
    }
}

// Date pickers
extension AddEventViewController {
    
    @objc func startDateChanged(datePicker: UIDatePicker) {
        endDatePicker?.minimumDate = startDatePicker?.date
        startDateChanged = true
    }
    
    @objc func endDateChanged(datePicker: UIDatePicker) {
        endDateChanged = true
    }

}

// Event Tags View
extension AddEventViewController {
    
    func setUpAddEventTagButton(x: Int, y: Int) {
        let addEventTagButton = EventTagUIButton(frame: CGRect (x: x, y: y, width: 70, height: 30))
        let image = classics.setSystemImageOrDefault(systemName: "plus")
        addEventTagButton.setImage(image, for: .normal)
        addEventTagButton.tintColor = UIColor.white
        
        addEventTagButton.addTarget(self, action: #selector(addEventTag), for: .touchUpInside)
        addEventTagButton.tag = 666
        addEventTagButton.accessibilityIdentifier = "addTag"
        self.eventTagsView.addSubview(addEventTagButton)
    }
    
    @IBAction func addEventTag(sender: UIButton!) {
        addEventAlert()
    }
    
    func addEventAlert() {
        let addEventAlert = UIAlertController(title: "Add Tag", message: "Add some tags to describe your event", preferredStyle: .alert)

        addEventAlert.addTextField { (textField) in
            let exampleTags = ["HipHop", "Techno", "Houseparty", "Friends", "RnB", "Escalation"]
            let randomInt = Int.random(in: 0..<exampleTags.count)
            textField.placeholder = exampleTags[randomInt]
            textField.accessibilityIdentifier = "addTagTextField"
        }

        addEventAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak addEventAlert] (_) in
            let textField = addEventAlert?.textFields![0]
            if (textField?.text != "") {
                // Add to eventTags list
                self.eventTags.append("#" + (textField?.text)!)
                self.setUpEventTags()
            }
        }))

        addEventAlert.view.accessibilityIdentifier = "addTag"
        self.present(addEventAlert, animated: true, completion: nil)
    }
    
    
    func setUpEventTags() {
        let screenWidth  = Int (UIScreen.main.fixedCoordinateSpace.bounds.width)
        rowCounter = 0
                
        // TODO: Max 8 tags
        var xValueOfButton: Int = 10
        var yValueOfButton: Int = 10
        
        var lengthOfCurrentRow: Int = 0
        var widthOfPreviousEventTag: Int = 0
        for (index, eventTag) in eventTags.enumerated(){
            let titleLength = eventTag.count
            // Min button width of 80
            let widthOfCurrentEventTag = titleLength*12 > 80 ? titleLength*12 : 80
            
            lengthOfCurrentRow = lengthOfCurrentRow + widthOfCurrentEventTag
            if (index != 0) {
                if (lengthOfCurrentRow > screenWidth-20) {
                    xValueOfButton = 10
                    yValueOfButton = yValueOfButton + 40
                    lengthOfCurrentRow = widthOfCurrentEventTag + 5
                    rowCounter += 1
                } else {
                    lengthOfCurrentRow = lengthOfCurrentRow + 5
                    xValueOfButton = xValueOfButton + widthOfPreviousEventTag + 5
                }
            }
            widthOfPreviousEventTag = widthOfCurrentEventTag
            
           // Add FilterButton with inital values
            let filterButton = EventTagUIButton(frame: CGRect (x: xValueOfButton, y: yValueOfButton, width: widthOfCurrentEventTag, height: 30))
            filterButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            filterButton.setTitle(eventTag, for: .normal)

            // Add filterButton to buttonsInOneRow[]
            self.eventTagsView.addSubview(filterButton)
            
        }
        
        removeAddEventButton()
        if (eventTags.count < 10) {
            if (lengthOfCurrentRow < screenWidth - 80 ) {
                setUpAddEventTagButton(x: xValueOfButton + widthOfPreviousEventTag + 5, y: yValueOfButton)
            } else {
                setUpAddEventTagButton(x: 10 , y: yValueOfButton +  40)
            }
        }
    }
    
    @IBAction func buttonAction(sender: UIButton!) {
        let pressedButton = sender as? EventTagUIButton
        
        let alert = UIAlertController(title: "Delete Filter", message: "Do you wan't to delete this Filter Tag?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (alert: UIAlertAction!) in
            let index = self.eventTags.firstIndex(of: (pressedButton?.titleLabel!.text!)!)
            self.eventTags.remove(at: index!)
            self.deleteOldView()
            self.setUpEventTags()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}

extension AddEventViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hidePickerView()
    }
}

// Helper functions
extension AddEventViewController {
    
    func deleteOldView() {
        // TODO: delete old view before setting up the new buttons/tags
        guard let sublayers = self.eventTagsView.layer.sublayers else { return }
        for layer in sublayers {
            layer.removeFromSuperlayer()
        }
    }

    func removeAddEventButton(){
        if let viewWithTag = self.view.viewWithTag(666) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    enum Input: String {
        case EventName = "Event name"
        case EventTags = "Event tags"
        case EventType = "Event type"
        case StartDate = "Start date"
        case EndDate = "End date"
        case None = "None"
    }
    
    func checkIfInputIsPresentAndValid() -> (Bool, String) {
        // Till now only check if input is present TODO: validation
        if (self.eventNameTextField.text?.count == 0) {
            return (false, Input.EventName.rawValue)
        } else if (self.eventTypeTextField.text?.count == 0) {
            return (false, Input.EventType.rawValue)
        } else if (!self.endDateChanged) {
            return (false, Input.EndDate.rawValue)
        } else if (self.eventTags.count == 0) {
            return (false, Input.EventTags.rawValue)
        } else {
            return (true, Input.None.rawValue)
        }
    }
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        view.isUserInteractionEnabled = !loading
        navigationItem.rightBarButtonItem?.isEnabled = !loading
    }
    
    func setAddEventErrorAlert() {
        let alert = UIAlertController(title: "Can not upload Event", message: "Please check you connection and try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func makeInvitationsString(invites: [String]) -> String {
        if (invites.count == 0) {
            invitePeopleButton.setImage(classics.setSystemImageOrDefault(systemName: "plus"), for: .normal)
            return ""
        } else if (invites.count == 1) {
            invitePeopleButton.setImage(UIImage(), for: .normal)
            return invites[0]
        } else if (invites.count == 2) {
            invitePeopleButton.setImage(UIImage(), for: .normal)
            return invites[0] + ", " + invites[1]
        } else {
            invitePeopleButton.setImage(UIImage(), for: .normal)
            return invites[0] + ", " + invites[1] + " and \(invites.count - 2) more"
        }
    }
    
    func openCamera() {
        setLoading(true)
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePickerController.sourceType = UIImagePickerController.SourceType.camera
            imagePickerController.allowsEditing = true
            imagePickerController.delegate = self
            self.present(imagePickerController, animated: true, completion: nil)
            setLoading(false)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            setLoading(false)
        }
    }

    func openGallary()
    {
        setLoading(true)
        imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
        setLoading(false)
    }
}



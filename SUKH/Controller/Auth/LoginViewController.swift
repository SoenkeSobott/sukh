//
//  LoginViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 22.01.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
        
    // Outlets
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: CustomButton!
    @IBOutlet weak var signUpButton: CustomButton!
    @IBOutlet weak var justGetMeStartedButton: LoginButton!
  
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
        
    // Variables
    var activityIndicator = UIActivityIndicatorView()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        setUpTapAnywhereToDismissKeyboardGestureRecognizer()
        setUpActivityIndicator()
        setUpTextFields()
        setUpButtons()
        
    }
    
    func setUpTapAnywhereToDismissKeyboardGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
    
    func setUpActivityIndicator() {
        activityIndicator = Spinner().getActivityIndicator(view: view.self)
    }
    
    func setUpNavigationBar() {
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func setUpTextFields() {
        usernameField.delegate = self
        passwordField.delegate = self
        usernameField.clearButtonMode = .whileEditing
        passwordField.clearButtonMode = .whileEditing
    }
    
    func setUpButtons() {
        signInButton.setTitle("Login", for: .normal);
        signInButton.addTarget(self, action: #selector(logIn), for: .touchUpInside)
        
        signUpButton.setTitle("Sign Up", for: .normal);
        justGetMeStartedButton.setTitle("Please not", for: .normal)
    }
    
    @objc func logIn() {
        
        setLoading(true)
        
        guard let username = usernameField.text, username.count > 0, !username.isEmptyWithNoSpaces else {
            setInputMissingAlert()
            setLoading(false)
            return
        }
        guard let password = passwordField.text, password.count > 0, !password.isEmptyWithNoSpaces else {
            setInputMissingAlert()
            setLoading(false)
            return
        }
        
        let user = User(username: username, name: nil, mail: nil, password: password, createdAt: nil, role: nil, profileImage: nil, profileImageURL: nil, followers: nil, following: nil)
       
        
        // Get User
        if (connectionService.getConnectionStatus() != .unavailable) {
            mongoQueryService.authUser(user: user) { (response) -> () in
                if (response.response!.statusCode == 200) {
                    do {
                        // decode data from authUser response
                        let data: Data = response.data!
                        let decoder = JSONDecoder()
                        let authUser = try decoder.decode(User.self, from: data)
                                        
                        // save auth information on ios device
                        self.authService.saveUserAndPasswordAndRole(username: username, password: password, role: UserRole(rawValue: authUser.role!) ?? UserRole.Guest)
                                    
                        // unset loading and push to next vc
                        self.setLoading(false)
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StartMapViewController") as? StartMapViewController
                        self.navigationController?.pushViewController(vc!, animated: true)
                        return
                    } catch {
                        NSLog("Error occured retrieving user: " + error.localizedDescription)
                        self.setLoading(false)
                        self.passwordField.text = ""
                        self.setUserNotFoundOrPasswordIncorrectAlert(username: username)
                        return
                    }
                } else {
                    self.setLoading(false)
                    self.passwordField.text = ""
                    self.setUserNotFoundOrPasswordIncorrectAlert(username: username)
                    return
                }
            }
        } else {
            setLoading(false)
            basicAlerts.setConnectionAlert(forViewController: self)
        }
    }
    
    @IBAction func justGetMeStarted(_ sender: Any) {
        setLoading(true)
        self.authService.setGuestUser(role: .Guest)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StartMapViewController") as? StartMapViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        setLoading(false)
    }
    
    @IBAction func privacyAndTerms(_ sender: Any) {
        if let url = URL(string: "https://lively-ground-0e73a6503.azurestaticapps.net/") {
            UIApplication.shared.open(url)
        }
    }
    
    func setInputMissingAlert() {
        let alert = UIAlertController(title: "Can not sign up", message: "Please fill in all fields", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "inputMissing"
        self.present(alert, animated: true)
    }
    
    func setUserNotFoundOrPasswordIncorrectAlert(username: String) {
        let userNotExistingAlert = UIAlertController(title: "User or password incorrect", message: "The user \(username) or the provided password are incorrect", preferredStyle: .alert)
        
        // Sign up action
        let signUpAction = UIAlertAction(title: "Sign up", style: .default, handler: { (alert: UIAlertAction!) in
            let vc = UIStoryboard.init(name: "Auth", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        })
        
        userNotExistingAlert.addAction(signUpAction)
        
        userNotExistingAlert.addAction(UIAlertAction(title: "Try again", style: .cancel, handler: nil))

        userNotExistingAlert.view.accessibilityIdentifier = "usernameOrPasswordIncorrect"
        self.present(userNotExistingAlert, animated: true)
    }
   
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == usernameField {
            textField.resignFirstResponder()
            passwordField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

// Helper functions
extension LoginViewController {
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        usernameField.isEnabled = !loading
        passwordField.isEnabled = !loading
        signInButton.isEnabled = !loading
        signUpButton.isEnabled = !loading
        justGetMeStartedButton.isEnabled = !loading
    }
    
}

extension String {
    var isEmptyWithNoSpaces: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
}


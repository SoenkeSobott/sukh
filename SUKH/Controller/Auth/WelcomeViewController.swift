//
//  WelcomeViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 03.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    // Outlets
    @IBOutlet weak var sukhLabel: UILabel!
    
    // Services
    let authService = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        setUpNavigationBar()
        setUpLogo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkUser()
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        viewDidLoad()
    }
    
    func setUpNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 4.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.5
    }
    
    func setUpLogo() {
        let boldText = "SUKH"
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 30)]
        let attributedString = NSMutableAttributedString(string:boldText, attributes:attrs)
        
        sukhLabel.attributedText = attributedString

        let top = NSLayoutConstraint(item: sukhLabel as Any, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: self.view.bounds.height/2.5)
        view.addConstraint(top)
    }
    
    func checkUser() {
        if (authService.isSignedIn()) {
            self.goToStartController()
        } else  {
            sleep(1)
            let vc = UIStoryboard.init(name: "Auth", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func goToStartController() {
        sleep(1)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StartMapViewController") as? StartMapViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}

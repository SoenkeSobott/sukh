//
//  SignUpViewController.swift
//  SUKH
//
//  Created by Sönke Sobott on 03.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    // Outlets
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var goToLogin: UIButton!
    @IBOutlet weak var signUpButton: CustomButton!
        
    // Services
    let mongoQueryService = MongoQueryService()
    let authService = AuthService()
    let connectionService = ConnectionService()
    let basicAlerts = BasicAlerts()
    let classics = Classics()
    
    // Variables
    var defaultProfileImage = UIImage()
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        connectionService.setReachabilityWithoutNotifier()
        setUpTapAnywhereToDismissKeyboardGestureRecognizer()
        setUpTextFields()
        setUpActivityIndicator()
        setUpButtons()
        
    }
    
    func setUpTapAnywhereToDismissKeyboardGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
    
    func setUpTextFields() {
        usernameField.delegate = self
        passwordField.delegate = self
        usernameField.clearButtonMode = .whileEditing
        passwordField.clearButtonMode = .whileEditing
    }
    
    func setUpActivityIndicator() {
        // Configure the activity indicator.
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        // Set the layout constraints of the activity indicator.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            // The activity indicator is centred over the rest of the view.
            activityIndicator.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
    }
    
    func setUpButtons() {
        signUpButton.setTitle("Sign Up", for: .normal);
        signUpButton.addTarget(self, action: #selector(signUp), for: .touchUpInside)
    }
    
    // Sign up with the username and password
    @objc func signUp() {
        if (connectionService.getConnectionStatus() != .unavailable) {
            // set activity indicator
            setLoading(true)
            
            guard let username = usernameField.text, username.count > 3, !username.isEmptyWithNoSpaces else {
                setInputMissingAlert()
                setLoading(false)
                return
            }
            guard let password = passwordField.text, password.count > 0, !password.isEmptyWithNoSpaces else {
                setInputMissingAlert()
                setLoading(false)
                return
            }
            
            if (username.containsWhitespace) {
                setUsernameWrongCharacterAlert()
                passwordField.text = ""
                setLoading(false)
                return
            }
            
            if (username.contains("ö") || username.contains("ä") || username.contains("ü")) {
                setUsernameWrongCharacterAlert()
                passwordField.text = ""
                setLoading(false)
                return
            }
            
            // Validate username (TODO) and password
            if (password.count < 8) {
                setPasswordNotLongEnoughAlert()
                passwordField.text = ""
                setLoading(false)
                return
            }

            defaultProfileImage = classics.setSystemImageOrDefault(systemName: "person.circle")

            let defaultProfileImageData: Data = defaultProfileImage.jpegData(compressionQuality: 1)! as Data
            
            let ISO8601DateString = ISO8601DateFormatter.string(from: Date(), timeZone: .current, formatOptions: [.withFullDate, .withFullTime])
            
            let newUser = User(username: username, name: username, mail: "", password: password, createdAt: ISO8601DateString, role: UserRole.UnverifiedUser.rawValue, profileImage: defaultProfileImageData, profileImageURL: nil, followers: [""], following: [""], monthlyCredit: 0, totalCredit: 0)
            
            mongoQueryService.createUser(user: newUser) { (statusCode) -> () in
                if (statusCode == 201) {
                    
                    // save auth information on ios device
                    self.authService.saveUserAndPasswordAndRole(username: username, password: password, role: UserRole.UnverifiedUser)
                    
                    // unset loading and push to next vc
                    self.setLoading(false)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StartMapViewController") as? StartMapViewController
                    self.navigationController?.pushViewController(vc!, animated: true)
                } else if (statusCode == 409) {
                    self.setUsernameAlreadyUsedAlert()
                    self.passwordField.text = ""
                    self.setLoading(false)
                    return
                }else {
                    self.passwordField.text = ""
                    self.setLoading(false)
                    return
                }
            }
        } else {
            basicAlerts.setConnectionAlert(forViewController: self)
        }
        
    }
    
    func setInputMissingAlert() {
        let alert = UIAlertController(title: "Can not sign up", message: "Please fill in all fields and use a username with at least three characters", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "missingInput"
        self.present(alert, animated: true)
    }
    
    
    func setPasswordNotLongEnoughAlert() {
        let alert = UIAlertController(title: "Password not valid", message: "Please use a stronger password", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "invalidPassword"
        self.present(alert, animated: true)
    }
    
    func setUsernameWrongCharacterAlert() {
        let alert = UIAlertController(title: "Invalid username", message: "Please use different characters and no whitespaces", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "invalidUsername"
        self.present(alert, animated: true)
    }
    
    func setUsernameAlreadyUsedAlert() {
        let alert = UIAlertController(title: "Invalid username", message: "This username is already in use", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.view.accessibilityIdentifier = "usernameAlreadyTaken"
        self.present(alert, animated: true)
    }
    
    @IBAction func goToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         if textField == usernameField {
             textField.resignFirstResponder()
             passwordField.becomeFirstResponder()
         } else {
             textField.resignFirstResponder()
         }
         return true
    }
}

// Helper functions
extension SignUpViewController {
    
    // Turn on or off the activity indicator.
    func setLoading(_ loading: Bool) {
        if loading {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
        usernameField.isEnabled = !loading
        passwordField.isEnabled = !loading
        signUpButton.isEnabled = !loading
    }
    
}

extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}

//
//  NavigationBarExtension.swift
//  SUKH
//
//  Created by Sönke Sobott on 03.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        interactivePopGestureRecognizer?.delegate = self
    }

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

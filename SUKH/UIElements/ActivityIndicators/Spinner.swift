//
//  MainActivityIndicator.swift
//  SUKH
//
//  Created by Sönke Sobott on 17.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class Spinner {
    
    var activityIndicator: UIActivityIndicatorView?

    func getActivityIndicator(view: UIView) -> UIActivityIndicatorView {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor(named: "SukhTextColor")

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityIndicator as Any, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: view.bounds.height/2.2),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
        
        return activityIndicator
    }
    
    func getImageUploadActivityIndicator(view: UIImageView) -> UIActivityIndicatorView {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor(named: "SukhTextColor")

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
        
        return activityIndicator
    }
    
    
    func getMapActivityIndicator(view: UIView) -> UIActivityIndicatorView {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor.red

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            // The activity indicator is centred over the rest of the view.
            NSLayoutConstraint(item: activityIndicator as Any, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: guide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -10),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
        
        return activityIndicator
    }
    
    func getButtonActivityIndicator(button: UIButton) -> UIActivityIndicatorView {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor(named: "SukhTextColor")

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        button.addSubview(activityIndicator)
        
        let guide = button.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
        
        return activityIndicator
    }
    
    func getTableViewActivityIndicator(view: UIView) -> UIActivityIndicatorView {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor.gray

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityIndicator as Any, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: guide, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 5),
            activityIndicator.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
        ])
        
        return activityIndicator
    }

}

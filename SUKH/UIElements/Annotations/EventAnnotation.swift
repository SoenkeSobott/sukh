//
//  EventAnnotation.swift
//  SUKH
//
//  Created by Sönke Sobott on 12.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import MapKit

class EventAnnotation: NSObject, MKAnnotation {
   
    let eventID: String
    let title: String?
    let type: EventType
    let information: String?
    var coordinate: CLLocationCoordinate2D
    var distance: Double?
    var isOver: Bool?
    
    init(eventID: String?, title: String?, type: EventType, information: String?, coordinate: CLLocationCoordinate2D,distance: Double, isOver: Bool) {
        self.eventID = eventID!
        self.title = title
        self.type = type
        self.information = information
        self.coordinate = coordinate
        self.distance = distance
        self.isOver = isOver
        super.init()
        
    }

    var subtitle: String? {
        return information
    }

}

//
//  smallHeading.swift
//  SUKH
//
//  Created by Sönke Sobott on 13.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class smallHeading: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        setUpStyle()
    }
    
    
    func setUpStyle() {
        textColor = UIColor(named: "SukhTextColor")
        textAlignment = .left
        self.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
}

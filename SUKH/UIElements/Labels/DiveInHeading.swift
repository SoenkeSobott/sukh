//
//  DiveInHeading.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInHeading: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override func drawText(in rect: CGRect) {
        let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        super.drawText(in: rect.inset(by: padding))
        
    }
    
    func commonInit(){
        setUpStyle()
    }
    
    func setUpStyle() {
        textColor = UIColor.white
        textAlignment = .left
        self.font = UIFont.boldSystemFont(ofSize: 26)
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
    }
    
}


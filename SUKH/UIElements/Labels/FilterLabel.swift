//
//  FilterLabel.swift
//  SUKH
//
//  Created by Sönke Sobott on 24.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class FilterLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override func drawText(in rect: CGRect) {
        let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        super.drawText(in: rect.inset(by: padding))
    }
    
    func commonInit(){
        setUpStyle()
    }
    
    func setUpStyle() {
        // Text
        textColor = UIColor.white
        textAlignment = .center
        self.font = UIFont.boldSystemFont(ofSize: 25)
        
        // Shadow
        layer.shadowColor = UIColor(named: "SukhShadowColor")?.cgColor
        layer.shadowOpacity = 5
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize.zero
        
        // Size
        //layer.cornerRadius = 5
        //layer.masksToBounds = true
        numberOfLines = 1
        
        // Background
        //backgroundColor = UIColor(patternImage: UIImage(named: "FilterButtonBackgroundOne")!)
        // Make color Customizable (Every filter one backgroundimage/color)


    }
}

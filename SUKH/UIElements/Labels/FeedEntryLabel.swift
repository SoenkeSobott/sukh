//
//  FeedEntryLabel.swift
//  SUKH
//
//  Created by Sönke Sobott on 19.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class FeedEntryLabel: UILabel {

    
    override func drawText(in rect: CGRect) {
        let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        super.drawText(in: rect.inset(by: padding))
        
    }
    

}

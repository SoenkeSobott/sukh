//
//  DiveInFooter.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInFooter: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()

        if !UIAccessibility.isReduceTransparencyEnabled {
            backgroundColor = .clear

            let blurEffect = UIBlurEffect(style: .light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            addSubview(blurEffectView)
        } else {
            backgroundColor = .black
        }
        
        clipsToBounds = true
        layer.cornerRadius = 10
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

    }


}

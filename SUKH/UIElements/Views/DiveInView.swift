//
//  DiveInView.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 10
        
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.gray.cgColor
        
        clipsToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 10
        
    }

}

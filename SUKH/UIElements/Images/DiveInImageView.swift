//
//  DiveInImageView.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DiveInImageView: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()

        clipsToBounds = true
        layer.cornerRadius = 10
        
    }

}

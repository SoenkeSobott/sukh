//
//  SearchTextField.swift
//  SUKH
//
//  Created by Sönke Sobott on 02.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class SearchTextField: UITextField, UITextFieldDelegate {

   // Credit: https://stackoverflow.com/questions/25367502/create-space-at-the-beginning-of-a-uitextfield
   let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

   override open func textRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
   }

   override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
   }

   override open func editingRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
   }
   
   override init(frame: CGRect) {
       super.init(frame: frame)
       setUpTextField()
   }
   
   required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       setUpTextField()
       setShadow()
   }
   
    func setUpTextField() {
        attributedPlaceholder = NSAttributedString(string:placeholder ?? "N/A", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textColor = UIColor(named: "SukhTextColor")
        backgroundColor = UIColor(named: "SukhBackgroundColor")
    
        layer.cornerRadius = self.frame.height / 2
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.2
        clearButtonMode = .whileEditing

        delegate = self as UITextFieldDelegate
    }
   
   private func setShadow() {
       layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
       layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
       layer.shadowOpacity = 0.5
       clipsToBounds       = true
       layer.masksToBounds = false
   }

}

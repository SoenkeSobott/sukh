//
//  InputTextField.swift
//  SUKH
//
//  Created by Sönke Sobott on 13.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class InputTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpTextField()
        setShadow()
    }
    
    func setUpTextField() {
        attributedPlaceholder = NSAttributedString(string:placeholder ?? "N/A", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textColor = UIColor(named: "SukhTextColor")
        backgroundColor = UIColor(named: "SukhBackgroundColor")
        
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.2
        clearButtonMode = .whileEditing

        delegate = self as UITextFieldDelegate
    }
    
    private func setShadow() {
        layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        layer.shadowOpacity = 0.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }
    
}

extension InputTextField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
}

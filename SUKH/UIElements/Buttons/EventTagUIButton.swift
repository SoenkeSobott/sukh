//
//  EventTagUIButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 02.02.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class EventTagUIButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        setTitleColor(.white, for: .normal)
        setBackgroundImage(#imageLiteral(resourceName: "SUCKITButtonBackgroundImage"), for: UIControl.State.normal)
        titleLabel?.font     = UIFont(name: "AvenirNext-DemiBold", size: 15)
        layer.cornerRadius   = 15
        // this 'activates' cornerRadius even when the backgroundimage is displayed
        self.clipsToBounds = true
        //setShadow()
    }
    
    // TODO what is wrong with the shadow? -> check in all button classes
    private func setShadow() {
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 6.0, height: 6.0)
        layer.shadowRadius  = 20
        layer.shadowOpacity = 5.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }
    
}

//
//  GoToLocationButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 04.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class GoToLocationButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        setShadow()
        setTitleColor(.white, for: .normal)
        setBackgroundImage(#imageLiteral(resourceName: "SUCKITButtonBackgroundImage"), for: UIControl.State.normal)
        titleLabel?.font     = UIFont(name: "AvenirNext-DemiBold", size: 12)
        layer.cornerRadius   = 15
        // this 'activates' radius even when te backgroundimage is displayed
        self.clipsToBounds = true
    }
    
    private func setShadow() {
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 0.0, height: 6.0)
        layer.shadowRadius  = 8
        layer.shadowOpacity = 0.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }

}


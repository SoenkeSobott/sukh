//
//  AddButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 30.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class AddButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        setTitleColor(.white, for: .normal)
        setBackgroundImage(#imageLiteral(resourceName: "SUCKITButtonBackgroundImage"), for: UIControl.State.normal)
        titleLabel?.font     = UIFont(name: "AvenirNext-DemiBold", size: 18)
        layer.cornerRadius   = 15
        // this 'activates' cornerRadius even when the backgroundimage is displayed
        self.clipsToBounds = true
    }

}

//
//  decentStyleButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class DecentStyleButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
        setShadow()

    }
    
    func setupButton() {
        layer.borderWidth = 0.2
        layer.borderColor = UIColor.gray.cgColor

        backgroundColor = UIColor(named: "SukhBackgroundColor")
        
        setTitleColor(UIColor(named: "SukhTextColor"), for: .normal)
        titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0)
    
    }
    
    // TODO what is wrong with the shadow? -> why not work with radius?
    private func setShadow() {
        layer.shadowColor   = UIColor(named: "SukhTextColor")?.cgColor
        layer.shadowOffset  = CGSize(width: 0.5, height: 0.5)
        layer.shadowOpacity = 0.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }

}

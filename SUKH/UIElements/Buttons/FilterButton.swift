//
//  FilterButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 05.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class FilterButton: UIButton {
    
    var backgroundimages: [UIImage] = [#imageLiteral(resourceName: "FilterButtonBackgroundOne"), #imageLiteral(resourceName: "FilterButtonBackgroundTwo"), #imageLiteral(resourceName: "FilterButtonBackgroundThree"), #imageLiteral(resourceName: "FilterButtonBackgroundFour"), #imageLiteral(resourceName: "FilterButtonBackgroundFive"), #imageLiteral(resourceName: "SUCKITButtonBackgroundImage")]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        let number = Int.random(in: 0 ..< 6)
        setTitleColor(.white, for: .normal)
        // TODO: set background image so that it looks like one behind all buttons
        setBackgroundImage(backgroundimages[number], for: UIControl.State.normal)
        titleLabel?.font     = UIFont(name: "AvenirNext-DemiBold", size: 18)
        layer.cornerRadius   = 20
        // this 'activates' radius even when te backgroundimage is displayed
        self.clipsToBounds = true
    }
    
    // TODO: set shadow in all button classes
    private func setShadow() {
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 0.0, height: 6.0)
        layer.shadowRadius  = 8
        layer.shadowOpacity = 0.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }

}

//
//  LoginButton.swift
//  SUKH
//
//  Created by Sönke Sobott on 08.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class LoginButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        setTitleColor(.white, for: .normal)
        setBackgroundImage(#imageLiteral(resourceName: "SUCKITButtonBackgroundImage"), for: UIControl.State.normal)
        titleLabel?.font     = UIFont(name: "AvenirNext-DemiBold", size: 18)
        layer.cornerRadius   = 15
        // this 'activates' cornerRadius even when the backgroundimage is displayed
        self.clipsToBounds = true
        //setShadow()
    }
    
    // TODO what is wrong with the shadow? -> check in all button classes
    private func setShadow() {
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 3.0, height: 3.0)
        //layer.shadowRadius  = 10
        layer.shadowOpacity = 5.5
        layer.masksToBounds = false
    }

}

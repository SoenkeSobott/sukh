//
//  PlacesTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 01.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class PlacesTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var placesAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

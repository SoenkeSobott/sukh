//
//  BlockedUserTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 19.12.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class BlockedUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var username: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Setup        
    }
}

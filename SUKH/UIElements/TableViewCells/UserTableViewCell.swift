//
//  UserTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 31.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Setup
        setUpImage()
        setUpButton()
        
    }
    
    func setUpImage() {
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderWidth = 0.6
        profileImage.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
    }
    
    func setUpButton() {
        actionButton.layer.borderWidth = 0.4
        actionButton.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
        actionButton.layer.cornerRadius = 4
        actionButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
}


//
//  FilterTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 24.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var name: FilterLabel!
    @IBOutlet weak var filterView: FilterView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  EventsRankingPlaceOneTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 21.04.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class EventsRankingPlaceOneTableViewCell: UITableViewCell {

    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDistance: UILabel!
    @IBOutlet weak var goingUsers: UILabel!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var rankIndicator: UIImageView!
    @IBOutlet weak var eventTagsView: UIView!
    
    var eventID: String = ""
        
    var activityIndicator = UIActivityIndicatorView()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpLabels()
        setUpImage()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setUpLabels() {
        eventName.textColor = UIColor(named: "SukhTextColor")
        eventDistance.textColor = UIColor(named: "SukhTextColor")
        goingUsers.textColor = UIColor(named: "SukhTextColor")
    }
    
    func setUpImage() {
        eventImage.contentMode = .scaleAspectFill
        eventImage.layer.borderWidth = 0.5
        eventImage.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
    }
}

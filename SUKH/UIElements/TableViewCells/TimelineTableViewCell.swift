//
//  TimelineTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 28.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class TimelineTableViewCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var eventsName: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var eventsImage: UIImageView!
    @IBOutlet weak var timelineDot: UIImageView!
    @IBOutlet weak var time: EventTagUIButton!
    @IBOutlet weak var credit: UIImageView!
    @IBOutlet weak var creditHeight: NSLayoutConstraint!
    @IBOutlet weak var creditWidth: NSLayoutConstraint!
    
    // Variables
    var eventID: String = ""
                
    override func awakeFromNib() {
        super.awakeFromNib()

        // Setup
        setUp()
        setUpImage()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setUp() {
        eventsName.textColor = UIColor(named: "SukhTextColor")
    }
    
    func setUpImage() {
        eventsImage.contentMode = .scaleAspectFill
        eventsImage.layer.borderWidth = 0.5
        eventsImage.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
        
        // timelineDot
        timelineDot.layer.cornerRadius = timelineDot.bounds.height / 2
    }

}

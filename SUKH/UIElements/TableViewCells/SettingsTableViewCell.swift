//
//  SettingsTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.11.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settingsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Setup
        setUp()
    }
    
    func setUp() {
        backgroundColor = UIColor(named: "SukhBackgroundColorColor")
        settingsLabel.textColor = UIColor(named: "SukhTextColor")
    }
    
}



//
//  EventsTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 10.12.19.
//  Copyright © 2019 SUKH. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell  {
    
    @IBOutlet weak var eventsName: UILabel!
    @IBOutlet weak var eventsImage: UIImageView!
    @IBOutlet weak var eventsDistance: UILabel!
    @IBOutlet weak var eventAction: UIButton!
    
    var eventID: String = ""
                
    override func awakeFromNib() {
        super.awakeFromNib()

        // Setup
        setUp()
        setUpImage()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setUp() {
        eventsName.textColor = UIColor(named: "SukhTextColor")
        eventsDistance.textColor = UIColor(named: "SukhTextColor")
    }
    
    func setUpImage() {
        eventsImage.contentMode = .scaleAspectFill
        eventsImage.layer.borderWidth = 0.5
        eventsImage.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
    }
}

//
//  EditGoingUserTableViewCell.swift
//  SUKH
//
//  Created by Sönke Sobott on 16.05.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import UIKit

class EditGoingUserTableViewCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var removeUser: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Setup
        setUpImage()
    }
       
    func setUpImage() {
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderWidth = 0.6
        profileImage.layer.borderColor = UIColor(named: "SukhTextColor")?.cgColor
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
    }

}

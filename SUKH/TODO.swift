//
//  TODO.swift
//  SUKH
//
//  Created by Sönke Sobott on 04.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//


/* Checklist Apple Store Ready MVP (see: https://developer.apple.com/app-store/review/guidelines/)
    - A method for filtering objectionable material from being posted to the app
    - A mechanism to report offensive content and timely responses to concerns (see: ReportMongoService)
    - Report events DONE
    - Report event feed entries DONE
    - Report users DONE
   The ability to block abusive users from the service
    - Block DONE
    - Integrate to all places where check for block users is necessary:
        - Don't show in search DONE
        - unfollow user/remove from followers when block DONE
         - Don't allow search for you DONE
         - Dont allow follow of you DONE
         - Don't find events of you DONE
         - Don't allow messages/invites to you (Should work because you can only invite friends)
        - ...?
*/

// Backup passwords (in cloud)
// - Published contact information so users can easily reach you. DONE
// - People need to know how to reach you with questions and support issues. DONE
// - Customers should know what they’re getting when they download or buy your app DONE
//      - Get realtime feedback on events nearby and never miss a great night again.

// DEV and PROD systeme (Mongo, Azure, etc.)

// Add Authentication
// Transactions for DELETE/UPDATE etc.

// TODO: add tests
// - createt test db !!!!!
// -> change host env in edit scheme
// - test all vc's
// - test all classes / services

// Move Backend to app service





// ---- New features for later:

// TODO: make own image uploader style \
// TODO: add videos (short <= 10 sec)  /

// TODO: Soziale Strucktur nachbauen: organissation ind gruppen, etc.

// TODO: Tags as entities

// TODO: own storyboard for search

// TODO: Filter
// - no events found communicate to user -> banner or so
// - Filter customizable
// - Filter in database
// - Filter ranking only via user interaction -> user dont want to arrange things by themseleves -> it should work from alone

// TODO: name
// - Souk Zook Zohk sook Sook ...


// TODO verifie user sign everywhere where user name is visible

// TODO: add pagination for Profile timeline (public and normal), eventsRanking, event feed
// TODO: when click on image show image before chaging it: profile/event
// TODO: old events annotation have gray layer -> not so dominant on the map because over
// TODO: search events have special pin
// TODO: add new follower notification
// TODO: Add Credit + Get Feed back when credit increases and lowers
// TODO: location indicator (Loerrach, Köln, etc..) on top of ranking table view
// TODO: when click on own profile redirect to own profile
// TODO: check if guest user access on search and so one -> only allow to view the map
// TODO: ranking of users: there you can see your followers feed and what they do/post
// TODO: rank people with most credit (with filter: location, time, etc..)
// In call when no entry found for this user delete it from the list (can be used everywhere) -> little bit like a cascade delete
// TODO: change color of clustered annotation
// TODO: Add users to map / ask if near event (Do you go to this event? / XYuser is at event xy)
// TODO  Vote only when at event location -> What to do when event is bigger (festival?) distance not appropriate
// TODO: instead of setting a radius in the frontend just send loaction to backend and ensure to always send 10 events to frontend?
// TODO: all hashtags clcikable to hastag site with all content with same hashtag
// TODO: find solution for variables without class
// TODO: get user preferences (tags) - for customized event recommendations
// TODO: change pw
// TODO: delete old events after one week or so (only for start -> safe data)
// TODO: mongoDB garbage collector
// TODO: Tags bug (correct length zeilenumbruch)
// TODO: Easy CI CD mechanisem
// TODO: single endpoint for updating user / event not many single for every value
// TODO: sms verification
// TODO: verification sign (only shown when verified with mail / phone)
// TODO: center logic (one activityIndicator for all, all button, labels as classes,...)

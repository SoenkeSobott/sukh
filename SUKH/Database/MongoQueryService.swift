//
//  MongoQueryService.swift
//  SUKH
//
//  Created by Sönke Sobott on 12.03.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import Alamofire
import MapKit
import Keys

class MongoQueryService {

    let encoder = JSONEncoder()
    
    // Services
    let keys = SUKHKeys()
    let locationService = LocationService()

    // https variables
    let hostDEV = "query-mongo-service"
    let hostPROD = "mongo-query-service"
    let scheme = "https"
    var host = ".azurewebsites.net"
    let eventPath = "/api/Event"
    let userPath = "/api/User"
    
    let userHost = "sukh-user-service.azurewebsites.net"
    let userPathNew = "/users"
    
    // host key
    var hostKey = ""
    
    init() {
        // Change between DEV and PROD by selecting the host and host key
        host = hostDEV + host //ProcessInfo.processInfo.environment["Mongo_Host"]!
        //hostKey = keys.azureFunctionHostKeyProd
        hostKey = keys.azureFunctionHostKeyDev
    }
    
    //--------    ----    ----    ----    ----    ----    ----    --------//
    //--------    ----               EVENTS       ----    ----    --------//
    //--------    ----    ----    ----    ----    ----    ----    --------//
    func createEvent(event: Event, completion: @escaping (Int) -> ()) {
                    
        var statusCode = 500
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        let data = event.encoded()
        
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                statusCode = response.response!.statusCode
                completion(statusCode)
            }
        }
    }
    
    func addVoteToEvent(eventID: String, vote: Vote, completion: @escaping (HTTPURLResponse) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/votes"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
                
        let data = vote.encoded()
        
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                completion(response.response!)
            }
        }
    }
    
    func updateEvent(eventID: String, event: Event, completion: @escaping (Int) -> ()) {
        
        var statusCode = 500
                
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
                
            return components.url
        }
           
        let data = event.encoded()
    
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                statusCode = response.response!.statusCode
                completion(statusCode)
            }
        }
    }
    
    func updateEventGoingUsers(eventID: String, username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/goingUsers/\(username)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .put).responseJSON { response in
            completion(response)
        }
        
    }
    
    func deleteUserFromEventGoingUsers(eventID: String, username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/goingUsers/\(username)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .delete).responseJSON { response in
            completion(response)
        }
    }
    
    func getAllEventsInRadius(location: CLLocation, radius: Int, filter: Filter, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(location.coordinate.latitude)/\(location.coordinate.longitude)/\(radius)/\(filter.rawValue)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getEventsFiltered(filter: EventFilter, completion: @escaping (AFDataResponse<Any>) -> ()) {
                
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/all/filter"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        // Set dynamic values (Geo)
        let completeEventFilter = CompleteEventFilter(id: filter.id,
                                        name: filter.name,
                                        latitude: locationService.getCenterOfMap().coordinate.latitude,
                                        longitude: locationService.getCenterOfMap().coordinate.longitude,
                                        radius: locationService.getSearchRadius(),
                                        from: filter.from,
                                        to: filter.to,
                                        tags: filter.tags,
                                        types: filter.types)
        
        let data = completeEventFilter.encoded()
                
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                completion(response)
            }
        }
    }
    
    func deleteEvent(id: String, completion: @escaping (Int) -> ()) {
        
        var statusCode = 500
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(id)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
    
        let data: Data? = nil
        
        AF.request(url!,
            method: .delete,
            parameters: data,
            encoder: JSONParameterEncoder.default).response { response in
                statusCode = response.response!.statusCode
                completion(statusCode)
        }
    }
    
    
    func getAllEventsOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/events"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getGoingUsersOfEvent(eventID: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/goingUsers"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    
    func getEvent(eventID: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
                
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getEventsBySearchTerm(requestingUser: String, searchTerm: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/search/\(requestingUser)/\(searchTerm)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func addFeedEntryToEvent(eventID: String, eventFeedEntry: EventFeedEntry, completion: @escaping (HTTPURLResponse) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/eventFeed"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
                
        let data = eventFeedEntry.encoded()
        
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                completion(response.response!)
            }
        }
    }
    
    func removeFeedEntryFromEvent(eventID: String, feedEntryId: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = eventPath + "/\(eventID)/eventFeed/\(feedEntryId)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
                        
        AF.request(url!, method: .delete).responseJSON { response in
            completion(response)
        }
    }
    
    
    
    //--------    ----    ----    ----    ----    ----    ----    --------//
    //--------    ----               USERS        ----    ----    --------//
    //--------    ----    ----    ----    ----    ----    ----    --------//
    
    func createUser(user: User, completion: @escaping (Int) -> ()) {
                
        var statusCode = 500
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        let data = user.encoded()
    
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                statusCode = response.response!.statusCode
                completion(statusCode)
            }
        }
    }
    
    func deleteUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .delete).responseJSON { response in
            completion(response)
        }
    }
    
    func updateUser(user: User, completion: @escaping (Int) -> ()) {
        
        var statusCode = 500
            
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/update"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        let data = user.encoded()
        
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                statusCode = response.response!.statusCode
                completion(statusCode)
            }
        }
    }
    
    func authUser(user: User, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/auth"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        let data = user.encoded()
        
        if let data = data {
            AF.upload(data, to: url!).responseJSON { response in
                completion(response)
            }
        }
    }
    
    func getUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {

        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = userHost
            components.path = userPathNew + "/\(username)"
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            debugPrint(response)
            completion(response)
        }
    }
    
    func getUsersBySearchTerm(username: String, searchTerm: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/search/\(searchTerm)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func addFollower(user: String, requestingUser: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(user)/follower/\(requestingUser)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .put).responseJSON { response in
            completion(response)
        }
    }
    
    func removeFollower(user: String, removeUser: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(user)/follower/\(removeUser)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .delete).responseJSON { response in
            completion(response)
        }
    }
    
    func getFollowersOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {

        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/follower"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
                
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getFollowingOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/following"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
                
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getInvitationsOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
          
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/invitations"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func acceptOrRejectInvitation(username: String, eventID: String, accept: Bool, completion: @escaping (AFDataResponse<Any>) -> ()) {
             
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/invitations/\(eventID)/\(accept)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .put).responseJSON { response in
            completion(response)
        }
    }
    
    func getNotificationsOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
             
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/notifications"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func getTimelineOfUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
             
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/timeline"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
    
    func reportUser(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
             
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/report"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .put).responseJSON { response in
            completion(response)
        }
    }
    
    func blockUser(username: String, blockUsername: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/block/\(blockUsername)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .put).responseJSON { response in
            completion(response)
        }
    }
    
    func unblockUser(username: String, unblockUsername: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/block/\(unblockUsername)"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .delete).responseJSON { response in
            completion(response)
        }
    }
    
    func getBlockedUsers(username: String, completion: @escaping (AFDataResponse<Any>) -> ()) {
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = userPath + "/\(username)/block"
            components.queryItems = [
                URLQueryItem(name: "code", value: self.hostKey),
            ]
            
            return components.url
        }
        
        AF.request(url!, method: .get).responseJSON { response in
            completion(response)
        }
    }
}

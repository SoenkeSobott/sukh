//
//  ReportsMongoService.swift
//  SUKH
//
//  Created by Sönke Sobott on 05.12.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import Alamofire
import MapKit
import Keys

class ReportsMongoService {

    let encoder = JSONEncoder()
    
    // Services
    let keys = SUKHKeys()
    let locationService = LocationService()

    // https variables
    let scheme = "https"
    let host = "sukh-report-service.azurewebsites.net"
    let reportPath = "/reports"
    
    //--------    ----    ----    ----    ----    ----    ----    --------//
    //--------    ----               REPORTS      ----    ----    --------//
    //--------    ----    ----    ----    ----    ----    ----    --------//
    func postReport(report: Report, completion: @escaping (Int) -> ()) {
                    
        var statusCode = 500
        
        var url: URL? {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = reportPath
        
            return components.url
        }
                
        let parameters: Parameters = ["reportId": report.reportId!,
                                      "type": report.type!,
                                      "subject": report.subject!,
                                      "createdBy": report.createdBy!,
                                      "createdAt": report.createdAt!]

        AF.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            statusCode = response.response!.statusCode
            debugPrint(response)
            completion(statusCode)
        }
    }
}


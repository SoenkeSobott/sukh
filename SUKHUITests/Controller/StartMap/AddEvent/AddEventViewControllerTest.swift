//
//  AddEventViewControllerTest.swift
//  SUKHUITests
//
//  Created by Sönke Sobott on 20.12.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import XCTest

class AddEventViewControllerTest: XCTestCase {
    
    // Services
    let uiTestClassics = UITestClassics()
    
    // Variables
    var logout = false

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        let app = XCUIApplication()
        if (logout) {
            // Logout
            let profileButton = app.buttons["profile"]
            profileButton.tap()
            app.scrollViews.otherElements.buttons["menu"].tap()
            app.tables/*@START_MENU_TOKEN@*/.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            app.alerts["Do you want to log out"].scrollViews.otherElements.buttons["Yes"].tap()
        } else {
            // do nothing
        }
        logout = false    }

    func testAddEvent() throws {
        logout = true
        let app = XCUIApplication()
        let title = "Test Add Event"
        uiTestClassics.goToStartMapLoggedIn(app: app)
        
        let addEventButton = app.buttons["addEvent"]
        XCTAssertTrue(addEventButton.waitForExistence(timeout: 5))
        addEventButton.tap()
        
        let saveLocationButton = app.buttons["saveLocation"]
        XCTAssertTrue(saveLocationButton.exists)
        saveLocationButton.tap() // This 'mocks' the tap on map
        XCTAssertTrue(saveLocationButton.isEnabled)
        saveLocationButton.tap()
        
        let eventNameTextField = app.textFields["eventName"]
        XCTAssertTrue(eventNameTextField.exists)
        
        // Try save
        let saveButton = app.buttons["save"]
        XCTAssertTrue(saveButton.exists)
        saveButton.tap()
        
        let invalidAlert = app.alerts["invalidAlert"]
        XCTAssertTrue(invalidAlert.exists)
        
        let okButton = invalidAlert.buttons["Ok"]
        okButton.tap()
        
        // Type event name
        eventNameTextField.tap()
        eventNameTextField.typeText(title)
        
        // Try save
        saveButton.tap()
        XCTAssertTrue(invalidAlert.exists)
        okButton.tap()
        
        // Select event type
        let eventTypeTextField = app.textFields["eventType"]
        XCTAssertTrue(eventTypeTextField.exists)
        eventTypeTextField.tap()
        
        let eventTypePicker = app.pickers["eventTypePicker"]
        XCTAssertTrue(eventTypePicker.exists)
        let hangoutPicker = eventTypePicker.pickerWheels["Hangout"]
        XCTAssertTrue(hangoutPicker.exists)
        hangoutPicker.adjust(toPickerWheelValue: "Productive")
        
        // Try Save
        saveButton.tap()
        XCTAssertTrue(invalidAlert.exists)
        okButton.tap()
        
        app.swipeUp()

        // Select start date
        app.datePickers["startDatePicker"].tap()
        
        // Add date
        var result = getFutureDateAndIdentifier(daysInFuture: 2)
        app.collectionViews.buttons[result.dateString].otherElements.containing(.staticText, identifier: result.identifier).element.tap()
        
        app.tapCoordinate(at: CGPoint(x:30,y:30))
        
        saveButton.tap()
        okButton.tap()
        
        // Select end date
        app.datePickers["endDatePicker"].tap()

        // Add date
        result = getFutureDateAndIdentifier(daysInFuture: 4)
        app.collectionViews.buttons[result.dateString].otherElements.containing(.staticText, identifier: result.identifier).element.tap()
        
        app.tapCoordinate(at: CGPoint(x:30,y:30))
        
        saveButton.tap()
        okButton.tap()

        // Check public switch
        let publicSwitch = app.switches["publicSwitch"]
        XCTAssertTrue(publicSwitch.exists)
        publicSwitch.tap()
        publicSwitch.tap()
        
        // Add tags
        let addTagButton = app.buttons["addTag"]
        XCTAssertTrue(addTagButton.exists)
        addTagButton.tap()
        
        let addTagAlert = app.alerts["addTag"]
        XCTAssertTrue(addTagAlert.exists)

        let addTagTextField = app.textFields["addTagTextField"]
        XCTAssertTrue(addTagTextField.exists)
        addTagTextField.tap()
        addTagTextField.typeText("TestTag")
        
        addTagAlert.buttons["Add"].tap()
        
        XCTAssertTrue(app.buttons["#TestTag"].exists)

        // Save event
        saveButton.tap()
        
        // Check that back on map
        let eventAnnotation = app.maps.element.otherElements[title]
        XCTAssertTrue(eventAnnotation.waitForExistence(timeout: 10))
        eventAnnotation.tap()
        
        // Delete event
        let diveInTitle = app.buttons["diveInName"]
        XCTAssertTrue(diveInTitle.waitForExistence(timeout: 5))
        diveInTitle.tap()
        
        let eventActions = app.buttons["eventActions"]
        XCTAssertTrue(eventActions.waitForExistence(timeout: 3))
        eventActions.tap()
        
        let deleteEventButton = app.buttons["Delete Event"]
        XCTAssertTrue(deleteEventButton.exists)
        deleteEventButton.tap()
        
        let deleteEventAlert = app.alerts["deleteEvent"]
        XCTAssertTrue(deleteEventAlert.waitForExistence(timeout: 3))
        
        deleteEventAlert.buttons["Yes"].tap()
        
        // Check that it is deleted from Map
        XCTAssertFalse(eventAnnotation.waitForExistence(timeout: 1))
        
        // Wait for banner to disappear
        let profileButton = app.buttons["profile"]
        XCTAssertTrue(profileButton.waitForExistence(timeout: 6))
    }
    
    func getFutureDateAndIdentifier(daysInFuture: Int) -> (dateString: String, identifier: String) {
        var dateComponent = DateComponents()
        dateComponent.day = daysInFuture
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, LLLL dd"
        let dateString = formatter.string(from: futureDate)
        formatter.dateFormat = "dd"
        let identifier = formatter.string(from: futureDate)
        
        return (dateString, identifier)
    }
    
}

extension XCUIApplication {
    func tapCoordinate(at point: CGPoint) {
        let normalized = coordinate(withNormalizedOffset: .zero)
        let offset = CGVector(dx: point.x, dy: point.y)
        let coordinate = normalized.withOffset(offset)
        coordinate.tap()
    }
}

//
//  StartMapViewControllerTest.swift
//  SUKHUITests
//
//  Created by Sönke Sobott on 09.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import XCTest
import Keys

class StartMapViewControllerTest: XCTestCase {
    
    // Services
    let uiTestClassics = UITestClassics()
    let keys = SUKHKeys()
    
    // Variables
    var logout = false

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDownWithError() throws {
        let app = XCUIApplication()
        if (logout) {
            // Logout
            let profileButton = app.buttons["profile"]
            profileButton.tap()
            app.scrollViews.otherElements.buttons["menu"].tap()
            app.tables/*@START_MENU_TOKEN@*/.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            app.alerts["Do you want to log out"].scrollViews.otherElements.buttons["Yes"].tap()
        } else {
            // do nothing
        }
        logout = false
    }

    func testStartMapAsGuest() throws {
        let app = XCUIApplication()

        uiTestClassics.goToStartMapAsGuest(app: app)
        uiTestClassics.checkIfMapItemsArePresent(app: app, timeout: 1)
                
        // Go to profile as guest
        let profileButton = app.buttons["profile"]
        profileButton.tap()
        
        let alert = app.alerts["guest"]
        XCTAssertTrue(alert.exists)
        
        let signInButton = alert.buttons["Sign in"]
        XCTAssertTrue(signInButton.exists)
        signInButton.tap()
        
        let guestButton = app.buttons["guest"]
        XCTAssertTrue(guestButton.waitForExistence(timeout: 2))
        
        // Check search as guest TODO: check if all is captured by search
        uiTestClassics.goToStartMapAsGuest(app: app)
                
        let searchButton = app.buttons["search"]
        searchButton.tap()
        
        XCTAssertTrue(alert.exists)
        
        var laterButton = alert.buttons["Later"]
        XCTAssertTrue(laterButton.exists)
        laterButton.tap()
                
        // Check add event as guest
        let addEventButton = app.buttons["addEvent"]
        addEventButton.tap()
        
        XCTAssertTrue(alert.exists)
        
        laterButton = alert.buttons["Later"]
        XCTAssertTrue(laterButton.exists)
        laterButton.tap()
            
        // Check dive in as guest
        let diveInButton = app.buttons["diveIn"]
        diveInButton.tap()
        
        let votesButton = app.buttons["votesButton"]
        XCTAssertTrue(votesButton.waitForExistence(timeout: 2))
        
        let goToEventButton = app.buttons["diveInName"]
        XCTAssertTrue(goToEventButton.exists)
        
        app.swipeDown(velocity: 1500)
        
        // Check ranking as guest
        let rankingButton = app.buttons["ranking"]
        rankingButton.tap()
        
        uiTestClassics.checkIfRankingItemsArePresent(app: app)
    }
    
    func testStartMapLoggedIn() throws {
        logout = true
        let app = XCUIApplication()
        
        uiTestClassics.goToStartMapLoggedIn(app: app)
        uiTestClassics.checkIfMapItemsArePresent(app: app, timeout: 3)
        
        // Check profile logged in
        let profileButton = app.buttons["profile"]
        profileButton.tap()
        
        let backToMapButton = app.buttons["backToMap"]
        backToMapButton.tap()
        
        // Check search logged in
        let searchButton = app.buttons["search"]
        searchButton.tap()
        
        uiTestClassics.checkIfSearchItemsArePresent(app: app)
        
        let backToMapFromSearchButton = app.buttons["back"]
        backToMapFromSearchButton.tap()
        
        // Check add event logged in
        let addEventButton = app.buttons["addEvent"]
        addEventButton.tap()
        
        uiTestClassics.checkIfAddEventItemsArePresent(app: app)
        
        let cancelAddEventButton = app.buttons["cancelAddEvent"]
        cancelAddEventButton.tap()
        
        // Check ranking logged in
        let rankingButton = app.buttons["ranking"]
        rankingButton.tap()
        
        uiTestClassics.checkIfRankingItemsArePresent(app: app)
        
        let backFromRankingButton = app.buttons["back"]
        backFromRankingButton.tap()
    }
}

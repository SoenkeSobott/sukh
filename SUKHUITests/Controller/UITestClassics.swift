//
//  UITestClassics.swift
//  SUKHUITests
//
//  Created by Sönke Sobott on 08.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import Foundation
import XCTest
import Keys

class UITestClassics {
    
    let keys = SUKHKeys()
    
    func checkIfMapItemsArePresent(app: XCUIApplication, timeout: TimeInterval) {
        let profileButton = app.buttons["profile"]
        XCTAssertTrue(profileButton.waitForExistence(timeout: timeout)) // Wait till map is loaded on first item
        
        let userLocationButton = app.buttons["userLocation"]
        XCTAssertTrue(userLocationButton.exists)

        let searchButton = app.buttons["search"]
        XCTAssertTrue(searchButton.exists)

        let addEventButton = app.buttons["addEvent"]
        XCTAssertTrue(addEventButton.exists)

        let rankingButton = app.buttons["ranking"]
        XCTAssertTrue(rankingButton.exists)

        let diveInButton = app.buttons["diveIn"]
        XCTAssertTrue(diveInButton.exists)
        
        let cancelAddEventButton = app.buttons["cancelAddEvent"]
        XCTAssertFalse(cancelAddEventButton.isHittable)
        
        let saveLocationButton = app.buttons["saveLocation"]
        XCTAssertFalse(saveLocationButton.isHittable)
    }
    
    func checkIfSearchItemsArePresent(app: XCUIApplication) {
        let backButton = app.buttons["back"]
        XCTAssertTrue(backButton.exists)
        
        let searchLabel = app.staticTexts["search"]
        XCTAssertTrue(searchLabel.exists)

        let eventsButton = app.buttons["events"]
        XCTAssertTrue(eventsButton.exists)

        let usersButton = app.buttons["users"]
        XCTAssertTrue(usersButton.exists)

        let placesButton = app.buttons["places"]
        XCTAssertTrue(placesButton.exists)
    }
    
    func checkIfRankingItemsArePresent(app: XCUIApplication) {
        let backButton = app.buttons["back"]
        XCTAssertTrue(backButton.exists)
        
        let trendingLabel = app.staticTexts["trendingLabel"]
        XCTAssertTrue(trendingLabel.exists)
        
        let filterButton = app.buttons["filterButton"]
        XCTAssertTrue(filterButton.exists)
    }
    
    func checkIfAddEventItemsArePresent(app: XCUIApplication) {
        let cancelAddEventButton = app.buttons["cancelAddEvent"]
        XCTAssertTrue(cancelAddEventButton.exists)
        XCTAssertTrue(cancelAddEventButton.isHittable)

        let saveLocationButton = app.buttons["saveLocation"]
        XCTAssertTrue(saveLocationButton.exists)
        XCTAssertTrue(saveLocationButton.isHittable)
    }
    
    func goToStartMapAsGuest(app: XCUIApplication) {
        // guest button
        let guestButton = app.buttons["guest"]
        XCTAssertTrue(guestButton.exists)
        guestButton.tap()        
    }
    
    func goToStartMapLoggedIn(app: XCUIApplication) {
        let validUsername = "JohnDoe"
        let validPassword = keys.johnDoePassword
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(validUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(validPassword)
        
        // signIn button
        let signInButton = app.buttons["signIn"]
        XCTAssertTrue(signInButton.exists)
        signInButton.tap()        
    }
}

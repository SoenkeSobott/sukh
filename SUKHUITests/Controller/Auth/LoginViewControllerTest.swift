//
//  LoginViewController.swift
//  SUKHUITests
//
//  Created by Sönke Sobott on 27.06.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import XCTest
import Keys

class LoginViewControllerTest: XCTestCase {
    
    // Services
    let uiTestClassics = UITestClassics()
    let keys = SUKHKeys()
    
    // variables
    var logout = false

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        let app = XCUIApplication()
        if (logout) {
            // Logout
            let profileButton = app.buttons["profile"]
            profileButton.tap()
            app.scrollViews.otherElements.buttons["menu"].tap()
            app.tables/*@START_MENU_TOKEN@*/.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            app.alerts["Do you want to log out"].scrollViews.otherElements.buttons["Yes"].tap()
        } else {
            // do nothing
        }
        
        logout = false
        
    }

    func testLoginSuccess() throws {
        logout = true
                        
        let validUsername = "JohnDoe"
        let validPassword = keys.johnDoePassword
            
        let app = XCUIApplication()
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(validUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(validPassword)
        
        // signIn button
        let signInButton = app.buttons["signIn"]
        XCTAssertTrue(signInButton.exists)
        signInButton.tap()
        
        // Check that start map items are present
        uiTestClassics.checkIfMapItemsArePresent(app: app, timeout: 5)
    }
    
    func testLoginWrongUsername() throws {
        let invalidUsername = "invalidUsername"
        let invalidPassword = "invalidPassword"
            
        let app = XCUIApplication()
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(invalidUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(invalidPassword)
        
        // signIn button
        let signInButton = app.buttons["signIn"]
        XCTAssertTrue(signInButton.exists)
        signInButton.tap()
        
        // alert
        let alert = app.alerts["usernameOrPasswordIncorrect"]
        XCTAssertTrue(alert.waitForExistence(timeout: 3))
        
        let tryAgainButton = alert.buttons["Try again"]
        XCTAssertTrue(tryAgainButton.exists)
        tryAgainButton.tap()
    }
    
    func testLoginMissingInput() throws {
        let emptyUsername = ""
        let emptyPassword = ""
        let spacesUsername = "    "
        let spacesPassword = "     "
        
        let app = XCUIApplication()
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(emptyUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(emptyPassword)
        
        // signIn button
        let signInButton = app.buttons["signIn"]
        XCTAssertTrue(signInButton.exists)
        signInButton.tap()
        
        // alert
        let alert = app.alerts["inputMissing"]
        XCTAssertTrue(alert.waitForExistence(timeout: 3))
        
        let okButton = alert.buttons["Ok"]
        XCTAssertTrue(okButton.exists)
        okButton.tap()
        
        // with spaces
        usernameTextField.tap()
        usernameTextField.typeText(spacesUsername)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(spacesPassword)
        signInButton.tap()
        
        // alert
        XCTAssertTrue(alert.waitForExistence(timeout: 3))
        XCTAssertTrue(okButton.exists)
        okButton.tap()
    }
    
    func testGuestSuccess() throws {
        let app = XCUIApplication()

        // guest button
        let guestButton = app.buttons["guest"]
        XCTAssertTrue(guestButton.exists)
        guestButton.tap()
        
        // Check that start map items are present
        uiTestClassics.checkIfMapItemsArePresent(app: app, timeout: 1)
    }
}

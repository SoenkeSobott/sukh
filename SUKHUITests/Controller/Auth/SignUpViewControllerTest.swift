//
//  SignUpViewControllerTest.swift
//  SUKHUITests
//
//  Created by Sönke Sobott on 08.07.20.
//  Copyright © 2020 SUKH. All rights reserved.
//

import XCTest

class SignUpViewControllerTest: XCTestCase {
    
    // Services
    let uiTestClassics = UITestClassics()
    
    // Variables
    var deleteUser = false

    override func setUpWithError() throws {
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        if (deleteUser) {
            let app = XCUIApplication()
            let profileButton = app.buttons["profile"]
            profileButton.tap()
            app.scrollViews.otherElements.buttons["menu"].tap()
            app.tables/*@START_MENU_TOKEN@*/.staticTexts["Edit User"]/*[[".cells.staticTexts[\"Edit User\"]",".staticTexts[\"Edit User\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            app.buttons["Delete Account"].tap()
            app.alerts["Delete Account"].scrollViews.otherElements.buttons["Yes"].tap()
        }
    }
    
    func testSignUpSuccess() throws {
        deleteUser = true

        let newUsername = "newUser"
        let newPassword = "12345678"
        
        let app = XCUIApplication()
        goToSignUp(app: app)

        // sign up label
        let signUpLabel = app.staticTexts["signUp"]
        XCTAssertTrue(signUpLabel.exists)
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(newUsername)
        
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(newPassword)
        
        // signUp button
        let signUpButton = app.buttons["signUp"]
        XCTAssertTrue(signUpButton.exists)
        signUpButton.tap()
        
        // Check that start map items are present
        uiTestClassics.checkIfMapItemsArePresent(app: app, timeout: 3)
    }
    
    func testSignUpInvalidUsernames() throws {
        let invalidUsername = "invalid username"
        let validPassword = "validPassword"
            
        let app = XCUIApplication()
        goToSignUp(app: app)

        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(invalidUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(validPassword)
        
        // signUp button
        let signUpButton = app.buttons["signUp"]
        XCTAssertTrue(signUpButton.exists)
        signUpButton.tap()
        
        // alert
        let alert = app.alerts["invalidUsername"]
        XCTAssertTrue(alert.waitForExistence(timeout: 5))
        
        let okButton = alert.buttons["Ok"]
        XCTAssertTrue(okButton.exists)
        okButton.tap()
    }
    
    func testSignUpInvalidPasswords() throws {
        let validUsername = "newUser"
        var invalidPassword = "1234567"
            
        let app = XCUIApplication()
        goToSignUp(app: app)

        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(validUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(invalidPassword)
        
        // signUp button
        let signUpButton = app.buttons["signUp"]
        XCTAssertTrue(signUpButton.exists)
        signUpButton.tap()
        
        // alert
        let alert = app.alerts["invalidPassword"]
        XCTAssertTrue(alert.waitForExistence(timeout: 5))
        
        let okButton = alert.buttons["Ok"]
        XCTAssertTrue(okButton.exists)
        okButton.tap()
        
        // Second invalidPassword
        invalidPassword = "asd"
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(invalidPassword)
        signUpButton.tap()
            
        // alert
        XCTAssertTrue(alert.waitForExistence(timeout: 5))
        XCTAssertTrue(okButton.exists)
        okButton.tap()
    }
    
    func testSignUpUsernameAlreadTaken() throws {
        let alreadyTakenUsername = "JohnDoe"
        let validPassword = "validPassword"
            
        let app = XCUIApplication()
        goToSignUp(app: app)

        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(alreadyTakenUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(validPassword)
        
        // signUp button
        let signUpButton = app.buttons["signUp"]
        XCTAssertTrue(signUpButton.exists)
        signUpButton.tap()
        
        // alert
        let alert = app.alerts["usernameAlreadyTaken"]
        XCTAssertTrue(alert.waitForExistence(timeout: 3))
        
        let okButton = alert.buttons["Ok"]
        XCTAssertTrue(okButton.exists)
        okButton.tap()
    }
    
    func testSignUpMissingInput() throws {
        let emptyUsername = ""
        let emptyPassword = ""
        let spacesUsername = "    "
        let spacesPassword = "     "
        
        let app = XCUIApplication()
        goToSignUp(app: app)

        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        usernameTextField.tap()
        usernameTextField.typeText(emptyUsername)
    
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(emptyPassword)
        
        // signUp button
        let signUpButton = app.buttons["signUp"]
        XCTAssertTrue(signUpButton.exists)
        signUpButton.tap()
        
        // alert
        let alert = app.alerts["missingInput"]
        XCTAssertTrue(alert.waitForExistence(timeout: 5))
        
        let okButton = alert.buttons["Ok"]
        XCTAssertTrue(okButton.exists)
        okButton.tap()
        
        // with spaces
        usernameTextField.tap()
        usernameTextField.typeText(spacesUsername)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(spacesPassword)
        signUpButton.tap()
        
        // alert
        XCTAssertTrue(alert.waitForExistence(timeout: 5))
        XCTAssertTrue(okButton.exists)
        okButton.tap()
    }
    func testBreakUpSignUp() throws {
        let app = XCUIApplication()
        goToSignUp(app: app)

        // sign up label
        let signUpLabel = app.staticTexts["signUp"]
        XCTAssertTrue(signUpLabel.exists)
        
        // username textField
        let usernameTextField = app.textFields["username"]
        XCTAssertTrue(usernameTextField.exists)
        
        // password textField
        let passwordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        
        // signUp button
        let backToLoginButton = app.buttons["backToLogin"]
        XCTAssertTrue(backToLoginButton.exists)
        backToLoginButton.tap()
        
        // Check that login screen items are present
        let loginUsernameTextField = app.textFields["username"]
        XCTAssertTrue(loginUsernameTextField.exists)
        
        let loginPasswordSecureTextField = app.secureTextFields["password"]
        XCTAssertTrue(loginPasswordSecureTextField.exists)
        
        let signInButton = app.buttons["signIn"]
        XCTAssertTrue(signInButton.exists)
    }

    func goToSignUp(app: XCUIApplication) {
        let goToSignUpButton = app.buttons["signUp"]
        XCTAssertTrue(goToSignUpButton.exists)
        goToSignUpButton.tap()
    }
}
